/* 
 * File:   main.cpp
 * Author: Mehmet Akif TASOVA
 * Student Number: 111044016
 *
 * Created on October 8, 2012, 12:52 PM
 * 
 * This program will take a Turkish word from the user and 
 *  separate the word into root and suffixes.
 * 
 * This program also supports command line argument. so you can call
 *  program as "[executable_name] [word]" from command line
 *
 */

#include <iostream>

using namespace std;

// Constants for exit codes
const int EXIT_SUCCESS=0; // exited without any errors
const int EXIT_WITH_TOO_MANY_ARGS=1; // too many arguments given to program

// Constant array lengths
const int ROOT_SIZE = 11; // max length of root
const int WORD_SIZE = 21; // max length of word
const int SUFF_SIZE = 11; // max length of suffix array

// Constant function return codes 
const int SUCCESS=1;
const int FAIL=0;
const int FAILED_TO_FIND=-1; // failed to find code for getIndex function

// Miscellaneous Constants
const int ROOT_DICT_SIZE=15; // size of root dictionary string array
const char NULL_CHAR='\0'; // String terminator character


// ** SUFFIX CHECKER FUNCTIONS **
// This function checks for suffixes and return true if they found something
//  else they return false just for their profession
//  they also changes the string foundString with the found suffix

// check for Object Condition Suffixes
bool checkObjectCondition(char word[]);

// check for Static Position Suffixes
bool checkStaticPosition(char word[]);

// check for Ownership Suffixes
bool checkOwnership(char word[]);

// check for Movement Towards Suffixes
bool checkMoveTowards(char word[]);

//check for Movement Away Suffixes
bool checkMoveAway(char word[]);

// check for Without Condition (lacking) Suffixes
bool checkLacking(char word[]);

// check for Plural Suffixes
bool checkPlurals(char word[]);

// ** END OF SUFFIX CHECKER FUNCTIONS **

void strReverse(char str[]);
//  Takes a string reference then reverses the referenced string

int strlen(const char str[]);
// counts total number of elements of a string wihtout null character
// Precondition: given char array must contain a null characker as known as
//  string-terminator

int replaceAll(char toFind, char toRepalce, 
               char str1[], char str2[], char str3[]);
//  Finds char toFind in all strings, and replaces it with char toReplace
//  Returns total number of changes made

int replace(char toFind, char toRepalce, char str[]);
//  Finds char toFind in string, and replaces it with char toReplace
//  Returns total number of changes made

void append (char str[], char toAppend);
// Appends given char to given string
// Precondition: string must contain a null character at end

void strcpy (const char source[], char dest[]);
// copies source string to destination string
// Precondition1: source string must contain a null char
// Precondition2: destination string's allocated area must be equal or greater
//  than source string

int getIndex(const char str[], char toFind);
// Find given character toFind in string and return its index
//  returns index of first occurance
//  return -1 if cannot found character in string
// Precondition: string must contain a null character

void fillWithNull(char str[], int size);
// fills given char array with nulls

char numberToChar(const int number);
// converts given number to character

// Let the fun begin

int strcmp(const char str1[], const char str2[]);
// Compares given strings
// If 1st string longer than 2nd returns a positive number
//  If 2nd string longer than 1st returns a negative number
//  Else returns 0
// It assumes both strings lenghts equal to shortest string
// Precondition: both strings must contain a null(\0) character


int main(int argc, char** argv) {
    
    // a roots list to make things a little easier 
    char *rootDict[ROOT_DICT_SIZE]={"abone", "ada", "adres", "araba", "afis", 
                                 "cumba", "macera", "mahmuz", "seciye", "seher",
                                 "sentetik", "seyyah", "icinde", "disinda", 
                                 "sihhi"};  
    
    char theWord[WORD_SIZE]; // the word which taken from user
    
    if(argc > 2){
        cout << "Too many arguments given\n"
             << "Exactly " << (argc-1) << " of them given\n"
             << "Program will shut down with exit code 1...\n";
        return EXIT_WITH_TOO_MANY_ARGS;
    } else if (argc == 2){
        cout << "Your word is: " << argv[1] << endl; //show word to user
        strcpy(argv[1], theWord);
    } else { 
  // argc can't be lower than 1 because executable name is also an argument 
        cout << "Enter the word:";
        cin >> theWord;
    }
    
    bool found=false; // a bool value for passed flag
    char root[ROOT_SIZE]; // string to store root of word
    int i; // loop counter
    
    //fillWithNull(root, ROOT_SIZE);
    
    for(i=0;i<ROOT_DICT_SIZE;++i){
        if(strcmp(theWord, rootDict[i]) == 0){
            if(theWord[0] == rootDict[i][0]){
                found = true;
                strcpy(rootDict[i], root);
                break;
            }            
        } else {
            continue;
        }
    }
    
    size_t pos; // the position where to divide string
    char suffix[SUFF_SIZE]; // string to store pure suffixes
    char lostWord[WORD_SIZE];
    char foundSuffix[SUFF_SIZE];
    
    if (found){
        pos = strlen(root); // find the start point of suffixes
        strcpy(&theWord[pos], suffix); // strnig of pure suffixes
        suffix[SUFF_SIZE-1] = NULL_CHAR;
        
        cout << root << " : root" << endl;
        
        strReverse(suffix);
        
        // Find suffix and print them to screen
        checkOwnership(suffix);
        checkObjectCondition(suffix);
        checkStaticPosition(suffix);
        checkMoveTowards(suffix);
        checkMoveAway(suffix);
        checkPlurals(suffix);
        checkLacking(suffix);
        
        cout << "Suffixes which not included in Suffix Dictionaries\n";
        cout << suffix << " : rest" << endl;
    }else{
        cout << "The Word \"" << theWord 
             << "\" has no root in Root Dictionary\n"
             << "Program will try to parse the word.\n"
             << "But word might be parsed wrong.\n\n";
        strcpy(theWord, lostWord);
        strReverse(lostWord);
        
        // Find suffix and print them to screen
        checkOwnership(lostWord);
        checkObjectCondition(lostWord);
        checkStaticPosition(lostWord);
        checkMoveTowards(lostWord);
        checkMoveAway(lostWord);
        checkPlurals(lostWord);
        checkLacking(lostWord);
        
        strReverse(lostWord);
        
        cout << lostWord << " : Possible Root\n";
    } 
    
    return EXIT_SUCCESS;
    
}

// ** DEFINITIONS OF SUFFIX CHECKER FUNCTIONS **

bool checkObjectCondition(char word[])
{
    char *dict[4]={"iy", "uy", "i", "u"}; //Suffix Dictionary
    bool found=false;
    int i=0, j=0, len=strlen(word), suffLen=0;
    char foundSuffix[5];
    
    if(len < 1)
        return false;
    
    for(i=0; i<4 ; ++i){
        if(strcmp(word, dict[i]) == 0){
            found = true;
            suffLen = strlen(dict[i]);
            strcpy(dict[i], foundSuffix);
            for(j=0; j<len; ++j){
                word[j] = word[j+suffLen];
            }
            word[len-suffLen] = NULL_CHAR;
        }
    }
    
    if(found){
        strReverse(foundSuffix);
        cout << "-" << foundSuffix << " : Object Condition Suffix\n";
    }
    
    return found;
}


bool checkStaticPosition(char word[])
{
    char *dict[4]={"ad", "ed", "at", "et"}; //Suffix Dictionary
    bool found=false;
    int i=0, j=0, len=strlen(word), suffLen=0;
    char foundSuffix[5];
    
    if(len < 1)
        return false;
    
    for(i=0; i<4 ; ++i){
        if(strcmp(word, dict[i]) == 0){
            found = true;
            suffLen = strlen(dict[i]);
            strcpy(dict[i], foundSuffix);
            for(j=0; j<len; ++j){
                word[j] = word[j+suffLen];
            }
            word[len-suffLen] = NULL_CHAR;
        }
    }
    
    if(found){
        strReverse(foundSuffix);
        cout << "-" << foundSuffix << " : Static Position Suffix\n";
    }
    
    return found;
}

bool checkOwnership(char word[])
{
    char *dict[4]={"nin", "nun", "ni", "nu"}; //Suffix Dictionary
    bool found=false;
    int i=0, j=0, len=strlen(word), suffLen=0;
    char foundSuffix[5];
    
    if(len < 1)
        return false;
    
    for(i=0; i<4 ; ++i){
        if(strcmp(word, dict[i]) == 0){
            found = true;
            suffLen = strlen(dict[i]);
            strcpy(dict[i], foundSuffix);
            for(j=0; j<len; ++j){
                word[j] = word[j+suffLen];
            }
            word[len-suffLen] = NULL_CHAR;
        }
    }
    
    if(found){
        strReverse(foundSuffix);
        cout << "-" << foundSuffix << " : Ownership Suffix\n";
    }
        
    
    return found;
}

bool checkMoveTowards(char word[])
{
    char *dict[4]={"ay", "ey", "a", "e"}; //Suffix Dictionary
    bool found=false;
    int i=0, j=0, len=strlen(word), suffLen=0;
    char foundSuffix[5];
    
    if(len < 1)
        return false;
    
    for(i=0; i<4 ; ++i){
        if(strcmp(word, dict[i]) == 0){
            found = true;
            suffLen = strlen(dict[i]);
            strcpy(dict[i], foundSuffix);
            for(j=0; j<len; ++j){
                word[j] = word[j+suffLen];
            }
            word[len-suffLen] = NULL_CHAR;
        }
    }
    
    if(found){
        strReverse(foundSuffix);
        cout << "-" << foundSuffix << " : Move Towards Suffix\n";
    }
    
    return found;
}

bool checkMoveAway(char word[])
{
    char *dict[4]={"nad", "ned", "nat", "net"}; //Suffix Dictionary
    bool found=false;
    int i=0, j=0, len=strlen(word), suffLen=0;
    char foundSuffix[5];
    
    if(len < 1)
        return false;
    
    for(i=0; i<4 ; ++i){
        if(strcmp(word, dict[i]) == 0){
            found = true;
            suffLen = strlen(dict[i]);
            strcpy(dict[i], foundSuffix);
            for(j=0; j<len; ++j){
                word[j] = word[j+suffLen];
            }
            word[len-suffLen] = NULL_CHAR;
        }
    }
    
    if(found){
        strReverse(foundSuffix);
        cout << "-" << foundSuffix << " : Move Away Suffix\n";
    }
    
    return found;
}

bool checkLacking(char word[])
{
    char *dict[2]={"zis", "zus"}; //Suffix Dictionary
    bool found=false;
    int i=0, j=0, len=strlen(word), suffLen=0;
    char foundSuffix[5];
    
    if(len < 1)
        return false;
    
    for(i=0; i<2 ; ++i){
        if(strcmp(word, dict[i]) == 0){
            strcpy(dict[i], foundSuffix);
            found = true;
            suffLen = strlen(dict[i]);
            for(j=0; j<len; ++j){
                word[j] = word[j+suffLen];
            }
        }
    }
    
    if(found){
        strReverse(foundSuffix);
        cout << "-" << foundSuffix << " : Lacking Suffix\n";
    }
    
    return found;
}

bool checkPlurals(char word[])
{
    char *dict[2]={"ral", "rel"}; //Suffix Dictionary
    bool found=false;
    int i=0, j=0, len=strlen(word), suffLen=0;
    char foundSuffix[5];
    
    if(len < 1)
        return false;
    
    for(i=0; i<2 ; ++i){
        if(strcmp(word, dict[i]) == 0){
            strcpy(dict[i], foundSuffix);
            found = true;
            suffLen = strlen(dict[i]);
            for(j=0; j<len; ++j){
                word[j] = word[j+suffLen];
            }
        }
    }
    
    if(found){
        strReverse(foundSuffix);
        cout << "-" << foundSuffix << " : Plural Suffix\n";
    }
    
    return found;
}
// ** END OF SUFFIX CHECKER FUNCTIONS **



void strReverse(char str[])
{
    int i=0, len;
    char tmp;
    
    len = strlen(str);
    
    for (i=0; i < (len / 2); ++i){
        tmp = str[i];
        str[i] = str[(len - (i + 1))];
        str[(len - (i + 1))] = tmp;
    }
}

int strlen(const char str[])
{
    int length=0;
    
    for(length=0; str[length] != NULL_CHAR; ++length)
        continue;
    
    return length;
            
}

int replaceAll(char toFind, char toRepalce, 
               char str1[], char str2[], char str3[])
{
    int i=0, count=0, len1=0, len2=0, len3=0;
    
    len1 = strlen(str1);
    len2 = strlen(str2);
    len3 = strlen(str3);
    
    for(i=0; i<len1; ++i){
        if(str1[i] == toFind){
            str1[i] = toRepalce;
            count++;
        }
    }
    
    for(i=0; i<len2; ++i){
        if(str2[i] == toFind){
            str2[i] = toRepalce;
            count++;
        }
    }
    
    for(i=0; i<len3; ++i){
        if(str3[i] == toFind){
            str3[i] = toRepalce;
            count++;
        };
    }
    
    return count;
}

int replace(char toFind, char toRepalce, char str[])
{
    int i=0, count=0, len=0;
    
    len = strlen(str);
    
    for(i=0; (i<len) ; ++i){
        if(str[i] == toFind){
            str[i] = toRepalce;
            count++;
        }
    }
    
    return count;
}

void append (char str[], char toAppend)
{
    int len=strlen(str);
    
    if(str[len] == NULL_CHAR){
        str[len] = toAppend;
        str[len+1] = NULL_CHAR;
    }
    
}

void strcpy (const char source[], char dest[])
{
    int i=0, len=strlen(source);
    
    for(i=0;i<len;++i){
        dest[i] = source[i];
    }
    dest[len] = NULL_CHAR;
}

int getIndex(const char str[], char toFind)
{
    int i=0, index=FAILED_TO_FIND;
    
    for(i=0; str[i]!=NULL_CHAR; ++i){
        if(str[i] == toFind){
            index = i;
            break;
        }
    }
    
    return index;
}

void fillWithNull(char str[], int size)
{
    int i=0;
    for (i=0; i< size; ++i)
        str[i] = NULL_CHAR;
}

char numberToChar(const int number)
{
    char value;
    if ((number < 10) || (number > -1))
        value = static_cast<char>(number+48);
    else
        value = FAIL;
    
    return value;
}

int strcmp(const char str1[], const char str2[])
{
    int i=0, differ=0, maxlen=0;
    int len1=strlen(str1), len2=strlen(str2);
    
    if(len1 > len2)
        maxlen = len2;
    else
        maxlen = len1;
    
    for(i=0; i<maxlen; ++i){
        differ += ((static_cast<int>(str1[i])) - static_cast<int>(str2[i]));
    }
    
    return differ;
}