package tr.edu.gyte.bilmuh.cse241.tasova.hw10;

import java.awt.Color;

/**
 * A class for Hexagons.
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class Hexagon extends Polygon {

    public Hexagon(Integer[] xCoords, Integer[] yCoords) {
        super(6, xCoords, yCoords);
    }

    /**
     * Sets Hexagon's line color to given one
     *
     * @param newColor Desired color of line
     */
    @Override
    public Hexagon setLineColor(Color newColor) {
        super.setLineColor(newColor);
        return this;
    }

    /**
     * Sets Hexagon's line thickness
     *
     * @param newStroke Desired thickness of line
     */
    @Override
    public Hexagon setLineThickness(double thickness) {
        super.setLineThickness(thickness);
        return this;
    }

    /**
     * Set Points of Hexagon
     *
     * @param x1 x-axis of first point
     * @param x2 x-axis of second point
     * @param x3 x-axis of third point
     * @param x4 x-axis of fourth point
     * @param x5 x-axis of fifth point
     * @param x6 x-axis of sixth point
     *
     * @param y1 y-axis of first point
     * @param y2 y-axis of second point
     * @param y3 y-axis of third point
     * @param y4 y-axis of fourth point
     * @param y5 y-axis of fifth point
     * @param y6 y-axis of sixth point
     */
    public void setCoordinates(int x1, int x2, int x3, int x4, int x5, int x6,
            int y1, int y2, int y3, int y4, int y5, int y6) {

        Integer[] xCoords = new Integer[6];
        xCoords[0] = x1;
        xCoords[1] = x2;
        xCoords[2] = x3;
        xCoords[3] = x4;
        xCoords[4] = x5;
        xCoords[5] = x6;
        this.setXCoordinates(xCoords);

        Integer[] yCoords = new Integer[6];
        yCoords[0] = y1;
        yCoords[1] = y2;
        yCoords[2] = y3;
        yCoords[3] = y4;
        yCoords[4] = y5;
        yCoords[5] = y6;
        this.setYCoordinates(yCoords);
    }

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    @Override
    public String getClassName() {
        return "Hexagon";
    }

    /**
     * toString Method of Hexagon
     *
     * @return A string which contains coordinates, line color, line thickness,
     * area and perimeter
     */
    @Override
    public String toString() {
        Integer[] tmpX = getXCoordinates();
        Integer[] tmpY = getYCoordinates();
        int numOfEdges = getNumberOfEdges();

        String tmp = "Hexagon: ";
        for (int i = 0; i < numOfEdges; ++i) {
            tmp += ("P" + i + "(" + tmpX[i] + ", " + tmpY[i] + ")");
            if (i != (numOfEdges - 1)) {
                tmp += ", ";
            } else {
                tmp += ".";
            }
        }

        tmp += (" Area: " + getArea() + ",");
        tmp += (" Perimeter: " + getPerimeter() + ".");
        tmp += (" LineThickness: " + getLineThickness() + ", ");
        tmp += (" Line Color: " + ColorUtls.getColorName(getLineColor()) + ".");

        return tmp;
    }
}
