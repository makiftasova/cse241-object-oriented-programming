package tr.edu.gyte.bilmuh.cse241.tasova.hw10;

import java.awt.Color;

/**
 * A class for Triangles.
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class Triangle extends Polygon {

    public Triangle(Integer[] xCoords, Integer[] yCoords) {
        super(3, xCoords, yCoords);
    }

    /**
     * Sets Triangle's line color to given one
     *
     * @param newColor Desired color of line
     */
    @Override
    public Triangle setLineColor(Color newColor) {
        super.setLineColor(newColor);
        return this;
    }

    /**
     * Sets Triangle's line thickness
     *
     * @param newStroke Desired thickness of line
     */
    @Override
    public Triangle setLineThickness(double thickness) {
        super.setLineThickness(thickness);
        return this;
    }

    /**
     * Set Points of Triangle
     *
     * @param x1 x-axis of first point
     * @param x2 x-axis of second point
     * @param x3 x-axis of third point
     *
     * @param y1 y-axis of first point
     * @param y2 y-axis of second point
     * @param y3 y-axis of third point
     */
    public void setCoordinates(int x1, int x2, int x3, int y1, int y2, int y3) {
        Integer[] xCoords = new Integer[3];
        xCoords[0] = x1;
        xCoords[1] = x2;
        xCoords[2] = x3;
        this.setXCoordinates(xCoords);

        Integer[] yCoords = new Integer[3];
        yCoords[0] = y1;
        yCoords[1] = y2;
        yCoords[2] = y3;
        this.setYCoordinates(yCoords);
    }

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    @Override
    public String getClassName() {
        return "Triangle";
    }

    /**
     * toString Method of Triangle
     *
     * @return A string which contains coordinates, line color and line
     * thickness
     */
    @Override
    public String toString() {
        Integer[] tmpX = getXCoordinates();
        Integer[] tmpY = getYCoordinates();
        int numOfEdges = getNumberOfEdges();

        String tmp = "Triangle: ";
        for (int i = 0; i < numOfEdges; ++i) {
            tmp += ("P" + i + "(" + tmpX[i] + ", " + tmpY[i] + ")");
            if (i != (numOfEdges - 1)) {
                tmp += ", ";
            } else {
                tmp += ".";
            }
        }

        tmp += (" LineThickness: " + getLineThickness() + ", ");
        tmp += (" Line Color: " + ColorUtls.getColorName(getLineColor()) + ".");

        return tmp;
    }
}
