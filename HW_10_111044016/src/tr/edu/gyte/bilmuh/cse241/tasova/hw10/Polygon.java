package tr.edu.gyte.bilmuh.cse241.tasova.hw10;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * Polygon class of HW10, extends ShapePanel class and implements Shape
 * interface
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class Polygon extends Shape {

    private int _numOfEdges;
    private Integer[] _xCoords;
    private Integer[] _yCoords;

    public Polygon(int numberOfEdges, Integer[] xCoords, Integer[] yCoords) {
        this.setNumberOfEdges(numberOfEdges);
        this.setXCoordinates(xCoords);
        this.setYCoordinates(yCoords);
        this.setLineColor(Color.black);
        this.setLineThickness(1.0);
    }

    /**
     * Sets Polygon's line color to given one
     *
     * @param newColor Desired color of line
     */
    @Override
    public Polygon setLineColor(Color newColor) {
        super.setLineColor(newColor);
        return this;
    }

    /**
     * Sets Polygon's line thickness
     *
     * @param newStroke Desired thickness of line
     */
    @Override
    public Polygon setLineThickness(double thickness) {
        super.setLineThickness(thickness);
        return this;
    }

    /**
     * Draws Polygon to given Graphics object
     *
     * @param graph the Graphics object
     *
     * @param xChange Resize ratio at x-axis
     *
     * @param yChange Resize ratio at y-axis
     *
     * @throws ArrayIndexOutOfBoundsException if any point of shape is out of
     * initial JPanel, throws ArrayIndexOutOfBoundsException
     */
    @Override
    public void drawShape(Graphics graph,
            double xChange, double yChange, int panelWidth, int panelheight)
            throws ArrayIndexOutOfBoundsException {
        Integer[] tmpX = getXCoordinates();
        int numOfXCoords = getXCoordinates().length;
        int[] xCoords = new int[numOfXCoords];

        for (Integer i : tmpX) {
            if (i.intValue() >= panelWidth) {
                throw new ArrayIndexOutOfBoundsException(
                        "ERROR: Shape is out of bounds" + this.toString());
            }
        }

        for (int i = 0; i < numOfXCoords; ++i) {
            xCoords[i] = (int) (tmpX[i] * xChange);
        }

        Integer[] tmpY = getYCoordinates();

        for (Integer i : tmpY) {
            if (i.intValue() >= panelheight) {
                throw new ArrayIndexOutOfBoundsException(
                        "ERROR: Shape is out of bounds" + this.toString());
            }
        }

        int numOfYCoords = getYCoordinates().length;
        int[] yCoords = new int[numOfYCoords];
        for (int i = 0; i < numOfYCoords; ++i) {
            yCoords[i] = (int) (tmpY[i] * yChange);
        }

        float stroke = (float) (getLineThickness() * (xChange * yChange));
        BasicStroke tmpStroke = new BasicStroke(stroke);

        Graphics2D graph2d = (Graphics2D) graph;
        graph2d.setPaintMode();
        graph2d.setColor(getLineColor());
        graph2d.setStroke(tmpStroke);
        graph2d.drawPolygon(xCoords, yCoords, getNumberOfEdges());

    }

    /**
     * Calculates area of polygon by using Gauss' area formula
     *
     * @return Area of Polygon
     */
    @Override
    public double getArea() {
        Integer[] xCoords = getXCoordinates();
        Integer[] yCoords = getYCoordinates();
        int numOfEdges = getNumberOfEdges();

        double area = 0.0;
        double part1;
        double part2;

        for (int i = 0; i < getNumberOfEdges(); ++i) {
            part1 = (xCoords[i % numOfEdges] * yCoords[(i + 1) % numOfEdges]);
            part2 = (xCoords[(i + 1) % numOfEdges] * yCoords[i % numOfEdges]);

            area += part1 - part2;
        }

        area = Math.abs(area / 2.0);

        return area;
    }

    /**
     * Calculates Perimeter length of Polygon
     *
     * @return Perimeter length of Polygon
     */
    @Override
    public double getPerimeter() {
        Integer[] xCoods = getXCoordinates();
        Integer[] yCoords = getYCoordinates();

        int numOfEdges = getNumberOfEdges();
        double xDiff;
        double yDiff;
        double perimeter = 0.0;

        for (int i = 0; i < numOfEdges; ++i) {
            xDiff = xCoods[(i + 1) % numOfEdges] - xCoods[i % numOfEdges];
            yDiff = yCoords[(i + 1) % numOfEdges] - yCoords[i % numOfEdges];

            perimeter += Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
        }
        return perimeter;
    }

    /**
     * Returns x-axis coordinates of Polygon in an Integer [ ]
     *
     * @return x-axis coordinates of Polygon in an Integer [ ]
     */
    @Override
    public Integer[] getXCoordinates() {
        return this._xCoords;
    }

    /**
     * Returns y-axis coordinates of Polygon in an Integer [ ]
     *
     * @return y-axis coordinates of Polygon in an Integer [ ]
     */
    @Override
    public Integer[] getYCoordinates() {
        return this._yCoords;
    }

    /**
     * Set x-axis coordinates of Polygon with given ones
     *
     * @param xCoords an Integer [ ] which contains x-axis coordinates
     */
    @Override
    public void setXCoordinates(Integer[] xCoords) {
        this._xCoords = xCoords;
    }

    /**
     * Set y-axis coordinates of Polygon with given ones
     *
     * @param yCoords an Integer [ ] which contains y-axis coordinates
     */
    @Override
    public void setYCoordinates(Integer[] yCoords) {
        this._yCoords = yCoords;
    }

    /**
     * Sets total number of edges of Polygon
     *
     * @param numberOfEdges Total number of Edges
     */
    public void setNumberOfEdges(int numberOfEdges) {
        this._numOfEdges = numberOfEdges;
    }

    /**
     * Returns total number of Edges of Polygon
     *
     * @return Total number of Edges of Polygon
     */
    public int getNumberOfEdges() {
        return this._numOfEdges;
    }

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    @Override
    public String getClassName() {
        return "Polygon";
    }

    /**
     * toString Method of Polygon
     *
     * @return A string which contains coordinates, line color, line thickness,
     * area and perimeter
     */
    @Override
    public String toString() {
        Integer[] tmpX = getXCoordinates();
        Integer[] tmpY = getYCoordinates();
        int numOfEdges = getNumberOfEdges();

        String tmp = "Polygon: ";
        for (int i = 0; i < numOfEdges; ++i) {
            tmp += ("P" + i + "(" + tmpX[i] + ", " + tmpY[i] + ")");
            if (i != (numOfEdges - 1)) {
                tmp += ", ";
            } else {
                tmp += ".";
            }
        }

        tmp += (" Area: " + getArea() + ",");
        tmp += (" Perimeter: " + getPerimeter() + ".");
        tmp += (" LineThickness: " + getLineThickness() + ", ");
        tmp += (" Line Color: " + ColorUtls.getColorName(getLineColor()) + ".");

        return tmp;
    }
}
