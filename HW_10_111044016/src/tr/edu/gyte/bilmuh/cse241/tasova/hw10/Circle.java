package tr.edu.gyte.bilmuh.cse241.tasova.hw10;

import java.awt.Color;

/**
 * Circle class of HW10. extends Ellipse class since Circles are a specialized
 * form of Ellipses
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class Circle extends Ellipse {

    /**
     * Default constructor of Circle
     *
     * Directly calls super class' default constructor
     */
    public Circle() {
        super();
    }

    /**
     * A four parameter constructor for Circle class
     *
     * Directly calls super class' four parameter constructor as super(x, y, r,
     * r)
     *
     * Calls super class' four parameter constructor
     *
     * @param x x-axis point of top left corner of square which the Circle can
     * fit into it
     *
     * @param y y-axis point of top left corner of square which the Circle can
     * fit into it
     *
     * @param r radius of Circle
     */
    public Circle(int x, int y, int r) {
        super(x, y, r, r);
    }

    /**
     * Sets Circle's line color to given one
     *
     * @param newColor Desired color of line
     */
    @Override
    public Circle setLineColor(Color newColor) {
        super.setLineColor(newColor);
        return this;
    }

    /**
     * Sets Circle's line thickness
     *
     * @param newStroke Desired thickness of line
     */
    @Override
    public Circle setLineThickness(double thickness) {
        super.setLineThickness(thickness);
        return this;
    }

    /**
     * A function for setting radius of Circle
     *
     * @param radius new radius value of Circle
     */
    public void setRadius(int radius) {
        this.setHorizontalRadius(radius);
        this.setVerticalRadius(radius);
    }

    /**
     * A function for setting coordinates of Circle
     *
     * @param x x-axis point of top left corner of square which the Circle can
     * fit into it
     *
     * @param y y-axis point of top left corner of square which the Circle can
     * fit into it
     *
     */
    public void setCoordinates(int x, int y) {
        this.setXCoordinate(x);
        this.setYCoordinate(y);
    }

    /**
     * A function for getting Radius of Circle
     *
     * @return Radius of Circle
     */
    public int getRadius() {
        return getVerticalRadius();
    }

    /**
     * Returns area of Circle
     *
     * @return Area of Circle
     */
    @Override
    public double getArea() {
        return (Math.PI * getRadius() * getRadius());
    }

    /**
     * Returns perimeter of Circle
     *
     * @return Perimeter of Circle
     */
    @Override
    public double getPerimeter() {
        return (2 * Math.PI * getRadius());
    }

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    @Override
    public String getClassName() {
        return "Circle";
    }

    /**
     * toString Method of Circle class
     *
     * @return A string which contains coordinates, radius, line color, line
     * thickness, area and perimeter
     */
    @Override
    public String toString() {
        String tmp = "Circle: ";
        tmp += ("M(" + getXCoordinate() + ", " + getYCoordinate() + ")");
        tmp += (", R = " + getRadius() + ".");
        tmp += (" Area: " + getArea() + ",");
        tmp += (" Perimeter: " + getPerimeter() + ".");
        tmp += (" LineThickness: " + getLineThickness() + ", ");
        tmp += (" Line Color: " + ColorUtls.getColorName(getLineColor()) + ".");

        return tmp;
    }
}
