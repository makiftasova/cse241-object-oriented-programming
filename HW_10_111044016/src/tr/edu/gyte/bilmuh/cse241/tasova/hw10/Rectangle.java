package tr.edu.gyte.bilmuh.cse241.tasova.hw10;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * A class for Rectangles.
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class Rectangle extends Shape {

    private int _width;
    private int _height;
    private int _x;
    private int _y;

    public Rectangle(int x, int y, int width, int height) {
        _x = x;
        _y = y;
        _width = width;
        _height = height;

    }

    /**
     * Sets Rectangle's line color to given one
     *
     * @param newColor Desired color of line
     */
    @Override
    public Rectangle setLineColor(Color newColor) {
        super.setLineColor(newColor);
        return this;
    }

    /**
     * Sets Rectangle's line thickness
     *
     * @param newStroke Desired thickness of line
     */
    @Override
    public Rectangle setLineThickness(double thickness) {
        super.setLineThickness(thickness);
        return this;
    }

    /**
     * Draws Rectangle to given Graphics object
     *
     * @param graph the Graphics object
     *
     * @param xChange Resize ratio at x-axis
     *
     * @param yChange Resize ratio at y-axis
     *
     * @throws ArrayIndexOutOfBoundsException if any point of shape is out of
     * initial JPanel, throws ArrayIndexOutOfBoundsException
     */
    @Override
    public void drawShape(Graphics graph,
            double xChange, double yChange, int panelWidth, int panelHeight)
            throws ArrayIndexOutOfBoundsException {

        int x = getX();
        int y = getY();
        int height = getHeigth();
        int width = getWidth();

        if ((x + width) >= panelWidth) {
            throw new ArrayIndexOutOfBoundsException(
                    "ERROR: Shape is out of bounds" + this.toString());
        }

        if ((y + height) >= panelHeight) {
            throw new ArrayIndexOutOfBoundsException(
                    "ERROR: Shape is out of bounds" + this.toString());
        }

        x *= xChange;
        y *= yChange;
        width *= xChange;
        height *= yChange;

        float stroke = (float) (getLineThickness() * (xChange * yChange));
        BasicStroke tmpStroke = new BasicStroke(stroke);

        Graphics2D graph2d = (Graphics2D) graph;
        graph2d.setPaintMode();
        graph2d.setColor(getLineColor());
        graph2d.setStroke(tmpStroke);
        graph2d.drawRect(x, y, width, height);

    }

    /**
     * Calculates Distance between two point
     *
     * @param x1 First point's x-axis value
     * @param y1 First point's y-axis value
     * @param x2 Second point's x-axis value
     * @param y2 Second point's y-axis value
     *
     * @return Distance between given points
     */
    private int distanceBetweenPoints(int x1, int y1, int x2, int y2) {

        int xPart = (int) Math.pow((x2 - x1), 2);
        int yPart = (int) Math.pow((y2 - y1), 2);
        int result = (int) Math.sqrt(xPart + yPart);

        return result;
    }

    /**
     * Calculates area of Rectangle
     *
     * @return Area of Rectangle
     */
    @Override
    public double getArea() {
        return (_height * _width);
    }

    /**
     * Calculates perimeter of Rectangle
     *
     * @return Perimeter of Rectangle
     */
    @Override
    public double getPerimeter() {
        return ((2 * _width) + (2 * _height));
    }

    @Override
    public Integer[] getXCoordinates() {
        Integer[] tmp = new Integer[4];
        tmp[0] = _x;
        tmp[1] = (_x + _width);
        tmp[2] = (_x + _width);
        tmp[3] = _x;

        return tmp;
    }

    @Override
    public Integer[] getYCoordinates() {
        Integer[] tmp = new Integer[4];
        tmp[0] = _y;
        tmp[1] = _y;
        tmp[2] = (_y + _height);
        tmp[3] = (_y + _height);

        return tmp;
    }

    @Override
    public void setXCoordinates(Integer[] xCoords)
            throws ArrayIndexOutOfBoundsException {
        _x = xCoords[0];
    }

    @Override
    public void setYCoordinates(Integer[] yCoords)
            throws ArrayIndexOutOfBoundsException {
        _y = yCoords[0];
    }

    /**
     * Changes width of Rectangle
     *
     * @param width New width value of rectangle
     *
     * @return its own
     */
    public Rectangle setWidth(int width) {
        _width = width;
        return this;
    }

    /**
     * Changes height of Rectangle
     *
     * @param height New height value of rectangle
     *
     * @return its own
     */
    public Rectangle setHeight(int height) {
        _height = height;
        return this;
    }

    /**
     * Changes x-axis coordinate of Rectangle
     *
     * @param x x-axis coordinate of Rectangle
     *
     * @return its own
     */
    public Rectangle setX(int x) {
        _x = x;
        return this;
    }

    /**
     * Changes y-axis coordinate of Rectangle
     *
     * @param y y-axis coordinate of Rectangle
     *
     * @return its own
     */
    public Rectangle setY(int y) {
        _y = y;
        return this;
    }

    /**
     * Returns Height of Rectangle
     *
     * @return Height of Rectangle
     */
    public int getHeigth() {
        return _height;
    }

    /**
     * Returns Width of Rectangle
     *
     * @return Width of Rectangle
     */
    public int getWidth() {
        return _width;
    }

    /**
     * Return x-axis coordinate of Rectangle
     *
     * @return x-axis coordinate of Rectangle
     */
    public int getX() {
        return _x;
    }

    /**
     * Return y-axis coordinate of Rectangle
     *
     * @return y-axis coordinate of Rectangle
     */
    public int getY() {
        return _y;
    }

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    @Override
    public String getClassName() {
        return "Rectangle";
    }

    /**
     * Created and Returns a Quadrangle object from field members of Rectangle
     *
     * @return The brand new Quadrangle object
     */
    public Quadrangle toQuadrangle() {
        Integer[] xCoords = new Integer[4];

        xCoords[0] = this.getX();
        xCoords[1] = (this.getX() + this.getWidth());
        xCoords[2] = (this.getX() + this.getWidth());
        xCoords[3] = this.getX();


        Integer[] yCoords = new Integer[4];

        yCoords[0] = (this.getY());
        yCoords[1] = (this.getY());
        yCoords[2] = (this.getY() + this.getHeigth());
        yCoords[3] = (this.getY() + this.getHeigth());


        return new Quadrangle(xCoords, yCoords);
    }

    /**
     * toString method of Rectangle class
     *
     * @return A String which contains coordinates, width, height, line color,
     * line thickness, area and perimeter
     */
    @Override
    public String toString() {
        String tmp = "Rectangle: ";
        tmp += ("(" + getX() + ", " + getY() + ")");
        tmp += (" Area: " + getArea() + ",");
        tmp += (" Perimeter: " + getPerimeter() + ".");
        tmp += (" LineThickness: " + getLineThickness() + ", ");
        tmp += (" Line Color: " + ColorUtls.getColorName(getLineColor()) + ".");

        return tmp;
    }
}
