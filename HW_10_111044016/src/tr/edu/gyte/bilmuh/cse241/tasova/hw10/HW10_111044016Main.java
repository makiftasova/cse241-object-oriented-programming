package tr.edu.gyte.bilmuh.cse241.tasova.hw10;

import java.awt.BasicStroke;
import java.awt.Color;
import java.util.Scanner;
import javax.swing.JFrame;

/**
 * Main class of CSE241 Homework10
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class HW10_111044016Main {

    // Frame related finals
    private static final int FRM_WIDTH = 640;
    private static final int FRM_HEIGHT = 480;
    // A Scanner for scanning things
    private static Scanner input = new Scanner(System.in);

    /**
     * One Function to Rule Them All
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        JFrame mainFrame = createFrame("111044016HW10", FRM_WIDTH, FRM_HEIGHT);
        // Now activating frame
        mainFrame.setVisible(true);



// A message for developer
//               System.out.println("mainFrame successfuly created.");

        // Some testing coordinates for some polygons
        Integer[] x1 = new Integer[]{450, 620, 14};
        Integer[] y1 = new Integer[]{60, 120, 36};

        Integer[] x2 = new Integer[]{20, 20, 120, 120};
        Integer[] y2 = new Integer[]{20, 80, 130, 20};

        Integer[] x3 = new Integer[]{100, 185, 210, 260, 310, 600};
        Integer[] y3 = new Integer[]{85, 110, 125, 68, 98, 350};

        Integer[] x4 = new Integer[]{150, 150, 250, 250};
        Integer[] y4 = new Integer[]{120, 180, 120, 180};

        // Let the real UI begin...
        ShapePanel shapes = new ShapePanel(FRM_WIDTH, FRM_HEIGHT);
        int choice = 0;
        int subchoice;

        Shape a = new Circle(123, 123, 123);
        shapes.addShape(a);
        mainFrame.add(shapes);

        do {

            try {
                int choosen; // choosen shape for remove or modify
                System.out.println("---------- MAIN MENU ----------");
                System.out.println(mainMenu());
                choice = readInt("Enter your choice => ");
                input.nextLine(); // clear input buffer
                switch (choice) {
                    case 1:
                        System.out.println("-----LIST SHAPES-----");
                        System.out.println(shapes.toString());
                        System.out.println("-----END OF LIST-----");
                        break;
                    case 2:
                        System.out.println("----- ADD SHAPE -----");
                        System.out.println(addMenu());

                        subchoice = readInt("Enter your choice => ");
                        input.nextLine(); // clear input buffer

                        while ((subchoice < 1) || (subchoice > 7)) {
                            System.out.println(
                                    "There is no such choice " + subchoice);

                            subchoice = readInt("Enter your choice => ");
                            input.nextLine(); // clear input buffer
                        }

                        Shape tmp = createShape(subchoice);
                        shapes.addShape(tmp);

                        System.out.println("Shape Added!\n");

                        break;
                    case 3:
                        System.out.println("-----REMOVE SHAPE-----");
                        System.out.println(shapes.toString());
                        choosen = readInt("Select A Shape to Remove => ");
                        input.nextLine(); // clear input buffer
                        shapes.removeShape(choosen - 1);
                        System.out.println("Shape Removed!\n");
                        break;
                    case 4:
                        System.out.println("-----MODIFY SHAPE-----");
                        System.out.println(shapes.toString());
                        choosen = readInt("Select A Shape to Modify => ");
                        input.nextLine(); // clear input buffer


                        Shape tmpS = shapes.get(choosen - 1);
                        shapes.removeShape(choosen - 1);

                        shapes.addShape(modifyShape(tmpS));
                        System.out.println("Modifying Shape...");
                        break;

                    case 5:
                        System.out.println("Now Cleaning Panel...");
                        shapes.clear();
                        System.out.println("Cleaning Done!");
                        break;
                    default:
                        System.out.print("There is no such option like ");
                        System.out.println(choice + "\n");
                        continue;
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("Returning Main Menu...");
            } finally {
                mainFrame.add(shapes);
                mainFrame.setVisible(true);
                mainFrame.repaint();

            }

        } while (choice != 5);

        System.out.println("Starting shutdown sequence...");
        System.out.println("Closing input stream...");
        input.close();
        System.out.println("Closing Frame...");
        mainFrame.dispose();
        System.out.println("Cleaning Previously Allocated System Memory...");
        System.gc();
        System.out.println("Done!");
        System.out.println("Now Shutting Down. Good Bye...");
    }

    /**
     * Creates a frame with given values then returns it
     *
     * @param name title of Frame
     * @param width width of frame
     * @param height height of frame
     * @return created frame with given values
     */
    private static JFrame createFrame(String name, int width, int height) {
        JFrame frame = new JFrame(name);
        frame.setSize(width, height);

// Some mesaages to developer...
//        System.out.println("Creating Frame...");
//        System.out.println("width: " + width + ", heigth: " + height);

        // Choose what will be done when clicked close button of frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set initial location of frame
        frame.setLocation(width / 2, height / 2);

        return frame;
    }

    /**
     * Only reads primitive type integers from Read Buffer
     *
     * @return First legal input
     */
    private static int readInt(String msg) {
        System.out.print(msg);
        while (!(input.hasNextInt())) {
            System.out.println("Illegal Input!!!\n");
            input.nextLine();
            System.out.print(msg);
        }
        return input.nextInt();
    }

    /**
     * Only reads primitive type doubles from Read Buffer
     *
     * @return First legal input
     */
    private static double readDouble(String msg) {
        System.out.print(msg);
        while (!(input.hasNextDouble())) {
            System.out.println("Illegal Input!!!\n");
            input.nextLine();
            System.out.print(msg);
        }
        return input.nextDouble();
    }

    /**
     * Only Creates Chosen Shape with Black line color and 1 pt Line Thickness
     *
     * @param choice Decides which Shape will be created
     *
     * @return Created Shape Object
     *
     * @throws IndexOutOfBoundsException if given choice value lower than 1 or
     * greater than 7 throws IndexOutOfBoundsException
     */
    private static Shape BasicShapeCreator(int choice)
            throws IndexOutOfBoundsException {

        if ((choice < 1) || (choice > 7)) {
            throw new IndexOutOfBoundsException(
                    "There is no such choice like " + choice);
        }

        int x, y, a, b, r;
        Shape tmp;

        switch (choice) {
            case 1: // Add Ellipse
                System.out.println("-- ADDING ELLIPSE --");
                x = readInt("x => ");
                input.nextLine(); // clear buffer

                y = readInt("y => ");
                input.nextLine(); // clear buffer

                a = readInt("A => ");
                input.nextLine(); // clear buffer

                b = readInt("B => ");
                input.nextLine(); // clear buffer

                tmp = new Ellipse(x, y, b, a);
                break;

            case 2: // Add Circle
                System.out.println("-- ADDING CIRCLE --");
                x = readInt("x => ");
                input.nextLine(); // clear buffer

                y = readInt("y => ");
                input.nextLine(); // clear buffer

                r = readInt("r => ");
                input.nextLine(); // clear buffer

                tmp = new Circle(x, y, r);
                break;

            case 3: // Add Polygon
                System.out.println("-- ADDING POLYGON --");
                int times;
                System.out.print("How many point you will add => ");
                times = input.nextInt();
                input.nextLine(); // clear buffer
                Integer[] polyX = new Integer[times];
                Integer[] polyY = new Integer[times];
                for (int i = 0; i < times; ++i) {
                    polyX[i] = readInt("Enter x" + i + " => ");
                    input.nextLine(); // clear buffer

                    polyY[i] = readInt("Enter y" + i + " => ");
                    input.nextLine(); // clear buffer
                }

                tmp = new Polygon(times, polyX, polyY);
                break;

            case 4: // Add Triangle
                System.out.println("-- ADDING TRIANGLE --");
                Integer[] triX = new Integer[3];
                Integer[] triY = new Integer[3];
                for (int i = 0; i < 3; ++i) {
                    triX[i] = readInt("Enter x" + i + " => ");
                    input.nextLine(); // clear buffer

                    triY[i] = readInt("Enter y" + i + " => ");
                    input.nextLine(); // clear buffer
                }

                tmp = new Triangle(triX, triY);
                break;

            case 5: // Add Quadrangle
                System.out.println("-- ADDING QUADRANGLE --");
                Integer[] quadX = new Integer[4];
                Integer[] quadY = new Integer[4];
                for (int i = 0; i < 4; ++i) {
                    quadX[i] = readInt("Enter x" + i + " => ");
                    input.nextLine(); // clear buffer

                    quadY[i] = readInt("Enter y" + i + " => ");
                    input.nextLine(); // clear buffer
                }

                tmp = new Quadrangle(quadX, quadY);
                break;

            case 6: // Add Rectangle
                System.out.println("-- ADDING RECTANGLE --");
                int rectX;
                int rectY;
                int rectW; // Rectangle's width
                int rectH; // Rectangle's height

                System.out.print("x => ");
                rectX = input.nextInt();
                input.nextLine(); // clear buffer

                System.out.print("y => ");
                rectY = input.nextInt();
                input.nextLine(); // clear buffer

                System.out.print("width => ");
                rectW = input.nextInt();
                input.nextLine(); // clear buffer

                System.out.print("height => ");
                rectH = input.nextInt();
                input.nextLine(); // clear buffer

                tmp = new Rectangle(rectX, rectY, rectW, rectH);
                break;

            case 7: // Add Hexagon
                System.out.println("-- ADDING HEXAGON --");

                Integer[] hexaX = new Integer[6];
                Integer[] hexaY = new Integer[6];
                for (int i = 0; i < 6; ++i) {
                    hexaX[i] = readInt("Enter x" + i + " => ");
                    input.nextLine(); // clear buffer

                    hexaY[i] = readInt("Enter y" + i + " => ");
                    input.nextLine(); // clear buffer
                }

                tmp = new Hexagon(hexaX, hexaY);
                break;

            default:
                System.out.println("There is no such choice " + choice);
                return null;

        }

        return tmp;

    }

    /**
     * A little Shape creator function
     *
     * @param choice Choice of user about Shape to create (between 0 - 7)
     *
     * @return Created Shape object
     *
     * @throws IndexOutOfBoundsException if given choice value lower than 1 or
     * greater than 7 throws IndexOutOfBoundsException
     */
    private static Shape createShape(int choice)
            throws IndexOutOfBoundsException {

        Shape tmp;
        int color;
        double thickness;

        tmp = BasicShapeCreator(choice);

        // Color and Thickness part
        System.out.println(colors());
        color = readInt("Choose a color => ");
        input.nextLine();

        String msg = "Enter number value of Thickness (1 or above) => ";
        thickness = readDouble(msg);
        input.nextLine();

        Color tmpColor = ColorUtls.colorSelector(color - 1);

        return tmp.setLineThickness(thickness).setLineColor(tmpColor);
    }

    /**
     * A little Shape modifier function
     *
     * @param toModify The Shape object to modify
     *
     * @return Modified Shape Object
     */
    private static Shape modifyShape(Shape toModify)
            throws UnknownError, IndexOutOfBoundsException {

        System.out.print("Enter 1(number one) for Yes, ");
        System.out.println("0(number zero) for No\n");

        if (1 == readInt("Modify Shape Coordinates/Lines? [1/0] => ")) {
            Color tmpColor = toModify.getLineColor();
            double tmpThick = toModify.getLineThickness();

            switch (toModify.getClassName()) {
                case "Ellipse":
                    toModify = BasicShapeCreator(1);
                    break;
                case "Circle":
                    toModify = BasicShapeCreator(2);
                    break;
                case "Polygon":
                    toModify = BasicShapeCreator(3);
                    break;
                case "Triangle":
                    toModify = BasicShapeCreator(4);
                    break;
                case "Quadrangle":
                    toModify = BasicShapeCreator(5);
                    break;
                case "Rectangle":
                    toModify = BasicShapeCreator(6);
                    break;
                case "Hexagon":
                    toModify = BasicShapeCreator(7);
                    break;
                default:
                    throw new UnknownError(
                            "There is no known match for given Shape object");
            }

            toModify.setLineThickness(tmpThick);
            toModify.setLineColor(tmpColor);

        }


        if (1 == readInt("Modify Line Color? [1/0] => ")) {
            input.nextLine(); // clear input buffer
            String msg = "Current Color: ";
            msg += ColorUtls.getColorName(toModify.getLineColor());
            System.out.println(msg);
            int temp;
            System.out.println(colors());
            temp = readInt("Choose Desired Color => ");
            input.nextLine();

            // Legality check
            while ((temp < 1) || (temp > 13)) {
                System.out.println("Illegal Input!\n");
                temp = readInt("Choose Desired Color => ");
                input.nextLine();
            }

            toModify.setLineColor(ColorUtls.colorSelector(temp));
            System.out.println();
        }

        if (1 == readInt("Modify Line Thickness? [1/0] => ")) {
            input.nextLine(); // clear input buffer
            System.out.println(
                    "Current Thickness: " + toModify.getLineThickness());
            double temp;
            temp = readDouble("Enter Desired Thicness (1 or above) => ");
            input.nextLine();

            while (temp < 1.0) {
                System.out.println("Illegal Input!\n");
                temp = readDouble("Enter Desired Thicness (1 or above) => ");
                input.nextLine();
            }

            toModify.setLineThickness(temp);
            System.out.println();
        }

        return toModify;
    }

    /**
     * Returns All default java.awt.Color color's name in an String
     *
     * @return java.awt.Color color's name in an String
     */
    private static String colors() {
        String colorList = "1-) Black\n2-) Blue\n3-) Cyan\n4-) Dark Gray\n";
        colorList += "5-) Gray\n6-) Green\n7-) Light Gray\n8-) Magenta\n";
        colorList += "9-) Orange\n10-) Pink\n11-) Red\n12-) White\n13-) Yellow";

        return colorList;
    }

    /**
     * Returns Main Menu text in a String object
     *
     * @return Main Menu text in a String object
     */
    private static String mainMenu() {
        String menuText = "1-) List Shapes\n2-) Add Shape\n3-) RemoveShape\n";
        menuText += "4-) Modify Shape\n5-) Exit";
        return menuText;
    }

    /**
     * Returns Add Shape Menu text in a String Object
     *
     * @return Add Shape Menu text in a String Object
     */
    private static String addMenu() {
        String menuText = "1-) Add Ellipse\n2-) Add Circle\n3-) Add Polygon\n";
        menuText += "4-) Add Triangle\n5-) Add Quadrangle\n6-) Add Rectangle\n";
        menuText += "7-) Add Hexagon";

        return menuText;
    }
}