/*
 * File:   111044016HW03_main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on October 22, 2012, 1:31 PM
 */

#include <iostream> // I/O library
#include "111044016HW03Temp.h"

using namespace std;

int main(void)
{
	Tempature temp;
	int selection;
	double tempature;

	cout << "[1] Begin Hardcoded test\n"
		<< "[2] Manually enter tempature value for testing\n"
		<< "Make your choice: ";

	cin >> selection;

	switch (selection) {
	case 1:

		cout << "**##** Hardcoded Test Begun **##**\n"
			<< " ##Variable temp which created by default "
			<< "constructor##\n";
		temp.printKelvin();
		temp.printCelcius();
		temp.printFahrenheit();

		temp.SetTempCelcius(10);
		cout << "\n ##Tempature setted to 10 Celcius Degree##\n\n";

		temp.printKelvin();
		temp.printCelcius();
		temp.printFahrenheit();

		temp.SetTempKelvin(560);
		cout << "\n ##Tempature setted to 560 Kelvin Degree##\n\n";

		temp.printKelvin();
		temp.printCelcius();
		temp.printFahrenheit();

		temp = Tempature(120);
		cout << "\n ##Tempature setted to 120 Kelvin Degree##\n\n";

		temp.printKelvin();
		temp.printCelcius();
		temp.printFahrenheit();


		temp.SetTempFahrenheit();
		cout << " ##Tempature setted to default value "
			<< "of SetTempFahrenheit()##\n";

		temp.printKelvin();
		temp.printCelcius();
		temp.printFahrenheit();

		cout << "**##** End of Hardcoded Test **##**\n";

		break;
	case 2:
		cout << "What kind of tempature will you enter?\n"
			<< "[1] Kelvin\n"
			<< "[2] Celcius\n"
			<< "[3] Fahrenheit\n"
			<< "Make your Subchoice: ";

		cin >> selection;

		switch (selection) {
		case 1:
			cout << "Enter tempature as Kelvin Degrees => ";
			cin >> tempature;

			temp.SetTempKelvin(tempature);

			cout << "Results:\n";
			temp.printKelvin();
			temp.printCelcius();
			temp.printFahrenheit();

			break;
		case 2:
			cout << "Enter tempature as "
				<< "Celcius Degrees => ";
			cin >> tempature;

			temp.SetTempCelcius(tempature);

			cout << "Results:\n";
			temp.printKelvin();
			temp.printCelcius();
			temp.printFahrenheit();

			break;

		case 3:
			cout << "Enter tempature as "
				<< "Fahrenheit Degrees => ";
			cin >> tempature;

			temp.SetTempFahrenheit(tempature);

			cout << "Results:\n";
			temp.printKelvin();
			temp.printCelcius();
			temp.printFahrenheit();

			break;
		default:
			cout << "You made a wrong desicion\n"
				<< "Sorry mate...\n";
			break;
		}
		break;

	default:
		cout << "You made a wrong desicion\n"
			<< "Sorry mate...\n";
		break;
	}


	return 0;
}