/* 
 * File:   111044016HW03_tempature.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on October 22, 2012, 1:40 PM
 */

#include "111044016HW03Temp.h" // Headers and Definitions

// For I/O functions
#include <iostream>
using namespace std;

// For exit function
#include <cstdlib>

Tempature::Tempature() : kelvin(0)
{
	// default constructor
	// intentionally left blank
}

Tempature::Tempature(const double userKelvin) : kelvin(userKelvin)
{
	if (userKelvin < 0) {
		cout << "Kelvin value can not be lower than 0\n";
		exit(1);
	}
}

void Tempature::SetTempKelvin(const double userKelvin)
{
	if (userKelvin > 0)
		kelvin = userKelvin;
	else
		kelvin = 0;
}

void Tempature::SetTempCelcius(const double userCelcius)
{
	int tempKelvin = (userCelcius + CEL_PER_KEL);

	if (tempKelvin >= 0)
		kelvin = tempKelvin;
	else
		kelvin = 0;
}

void Tempature::SetTempFahrenheit(const double userFahrenheit)
{
	int tempKelvin = FahrenheitToCelcius(userFahrenheit) + CEL_PER_KEL;

	if (tempKelvin >= 0)
		kelvin = tempKelvin;
	else
		kelvin = 0;
}

double Tempature::GetKelvin() const
{
	return kelvin;
}

double Tempature::GetCelcius() const
{
	return(kelvin - CEL_PER_KEL);
}

double Tempature::GetFahrenheit() const
{
	return(CelciusToFahrenheit(GetCelcius()));
}

// Output Functions

void Tempature::printKelvin() const
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	cout << "Tempature is " << GetKelvin() << " Kelvin Degree\n";
}

void Tempature::printCelcius() const
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	cout << "Tempature is " << GetCelcius() << " Celcius Degree\n";
}

void Tempature::printFahrenheit() const
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	cout << "Tempature is " << GetFahrenheit() << " Fahrenheit Degree\n";
}

// Private Function definations
//  Outer world has no idea about them...

double Tempature::CelciusToKelvin(const double celcius) const
{
	return(celcius + CEL_PER_KEL);
}

double Tempature::FahrenheitToCelcius(const double fahrenheit) const
{
	return(FAH_TO_CEL_C * (fahrenheit - FAH_TO_CEL_D));
}

double Tempature::CelciusToFahrenheit(const double celcius) const
{
	return((celcius * CEL_TO_FAH_C) + FAH_TO_CEL_D);
}