/* 
 * File:   111044016HW03_tempature.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on October 22, 2012, 1:40 PM
 */

#ifndef TEMPATURE_H
#define	TEMPATURE_H

class Tempature {
public:
	// Constructors
	Tempature();
	Tempature(const double userKelvin);

	// Setter Functions
	void SetTempKelvin(const double userKelvin = 0);
	void SetTempCelcius(const double userCelcius = 0);
	void SetTempFahrenheit(const double userFahrenheit = 0);

	// Getter Functions
	double GetKelvin() const;
	double GetCelcius() const;
	double GetFahrenheit() const;

	// Output Functions
	void printKelvin() const;
	void printCelcius() const;
	void printFahrenheit() const;

private:
	double kelvin;

	// static constants for conversion
	static const double CEL_PER_KEL = 273.15; // C to K difference
	static const double FAH_TO_CEL_C = (5.0 / 9.0); // F to C counter
	static const double CEL_TO_FAH_C = (9.0 / 5.0); // C to F counter
	static const double FAH_TO_CEL_D = 32.0; // F to C difference

	// Private Functions
	double CelciusToKelvin(const double celcius) const;
	double FahrenheitToCelcius(const double fahrenheit) const;
	double CelciusToFahrenheit(const double celcius) const;
};

#endif	/* TEMPATURE_H */