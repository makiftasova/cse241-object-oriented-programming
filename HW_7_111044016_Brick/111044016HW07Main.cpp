/* 
 * File:   111044016HW07Main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 20, 2012, 4:11 PM
 */

#include <iostream>
#include <fstream>
#include "111044016HW07Family.h"
#include <cstring>
#include <stdlib.h> // for strcmp function

using namespace std;
using namespace Tasova;


bool checkRelativity(const Family fam, const Person& per1, const Person& per2);

// One Function to rule them all
int main(int argc, char** argv)
{
	// ****Family of Nicholas I
	// **Parents
	Person nicholas_1("Nicholas I", "The Emperor",male, 1796);
	Person alexandra_quenn("Alexandra", "Feodorovna", female, 1798);
	// **Children
	Person elizabeth("Elizabeth", "Feodorovna", female, 1826);
	Person nicholas("Nicholas", "Emperor's Son", male, 1831);
	Person olga("Olga", "Feodorovna", female, 1822);
	Person michael("Michael", "Emperor's Son", male, 1832);
	Person alexander_2("Alexander", "The Emperor", male, 1818);
	Person constantine("Constantine", "Emperor's Son", male, 1827);
	Person alexandra("Alexandra", "Feodorovna", female, 1825);
	Person maria("Maria", "Feodorovna", female, 1819);
	
	Family royal_court_of_nicholas_1(nicholas_1, alexandra);
	
	royal_court_of_nicholas_1 += elizabeth;
	royal_court_of_nicholas_1 += nicholas;
	royal_court_of_nicholas_1 += olga;
	royal_court_of_nicholas_1 += michael;
	royal_court_of_nicholas_1 += alexander_2;
	royal_court_of_nicholas_1 += constantine;
	royal_court_of_nicholas_1 += alexandra;
	royal_court_of_nicholas_1 += maria;
	
	fstream family_1("data.bin", ios::out | ios::binary);
	
	family_1 << royal_court_of_nicholas_1;
	
	family_1.close();
	
	Family asd;
	fstream input("data.bin", ios::in | ios::binary);
	input >> asd;
	cout << asd;
	
	input.close();
	
	
	return (EXIT_SUCCESS);
}

bool checkRelativity(const Family fam, const Person& per1, const Person& per2)
{
	bool result=false;
	
	if(per1.getFamilyID() == per2.getFamilyID())
		result = true;
	else if (strcmp(per1.getSurname(), per2.getSurname()))
		result == true;
	else if (strcmp(per1.getSurname(), fam.getFather().getSurname()) &&
		 strcmp(per2.getSurname(),fam.getFather().getSurname()))
		result == true;
	
	
	return result;
}