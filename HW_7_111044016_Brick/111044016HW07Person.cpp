/* 
 * File:   111044016HW07Person.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on November 20, 2012, 4:13 PM
 */

#include "111044016HW07Person.h"
#include "111044016HW07Family.h"
#include <string>


namespace Tasova {

	Person::Person() : name(NULL), surname(NULL),
	birth_year(0), sex(male), cap_name(0), cap_surname(0), family_id(0),
	marital_status(single)
	{
		// Intentionally left blank
	}

	Person::Person(const char* u_name, const char* u_surname,
		sex_t u_sex, const year_t u_birth_year)
	: birth_year(u_birth_year), sex(u_sex), family_id(0),
	marital_status(single)
	{
		size_t tmp1 = strlen(u_name);
		cap_name = (tmp1 + 1);
		name = new char[cap_name];
		strcpy(u_name, name);

		size_t tmp2 = strlen(u_surname);
		cap_surname = (tmp2 + 1);
		surname = new char[cap_surname];
		strcpy(u_surname, surname);

	}

	Person::Person(const char* u_name, const char* u_surname,
		sex_t u_sex, const year_t u_birth_year,
		unsigned int u_family_id)
	: birth_year(u_birth_year), sex(u_sex), family_id(u_family_id),
	marital_status(single)
	{
		size_t tmp1 = strlen(u_name);
		cap_name = (tmp1 + 1);
		name = new char[cap_name];
		strcpy(u_name, name);

		size_t tmp2 = strlen(u_surname);
		cap_surname = (tmp2 + 1);
		surname = new char[cap_surname];
		strcpy(u_surname, surname);

	}

	Person::Person(const Person& orig)
	: birth_year(orig.getBirthYear()), sex(orig.getSex()),
	cap_name(orig.cap_name), cap_surname(orig.cap_surname),
	family_id(orig.getFamilyID()), marital_status(orig.getMaritalStatus())
	{
		name = new char[cap_name];
		strcpy(orig.name, name);

		surname = new char[cap_surname];
		strcpy(orig.surname, surname);
	}

	Person::~Person()
	{
		delete [] name;
		delete [] surname;
	}

	void Person::setName(const char* u_name)
	{
		size_t tmp = strlen(u_name);

		cap_name = (tmp + 1);
		delete [] name;
		name = new char[cap_name];
		strcpy(u_name, name);
	}

	void Person::setSurname(const char* u_surname)
	{
		size_t tmp = strlen(u_surname);
		cap_surname = (tmp + 1);
		delete [] surname;
		surname = new char[cap_surname];
		strcpy(u_surname, surname);
	}

	void Person::setBirthYear(const year_t u_birth_year)
	{
		birth_year = u_birth_year;
	}

	void Person::setSex(const sex_t u_sex)
	{
		sex = u_sex;
	}

	void Person::setFamilyID(const unsigned int u_family_id)
	{
		family_id = u_family_id;
	}

	void Person::setMaritalStatus(const marital_t u_marital_status)
	{
		marital_status = u_marital_status;
	}

	const char* Person::getName() const
	{
		return name;
	}

	const char* Person::getSurname() const
	{
		return surname;
	}

	const year_t Person::getBirthYear() const
	{
		return birth_year;
	}

	const sex_t Person::getSex() const
	{
		return sex;
	}

	const unsigned int Person::getFamilyID(void) const
	{
		return family_id;
	}

	const marital_t Person::getMaritalStatus(void) const
	{
		return marital_status;
	}

	const Person Person::operator =(const Person& source)
	{

		if (this != &source) {
			setName(source.getName());
			setSurname(source.getSurname());
			setSex(source.getSex());
			setBirthYear(source.getBirthYear());
			setFamilyID(source.getFamilyID());
		}

		return *this;
	}

	// Unavoidable friend operators

	std::ostream& operator <<(std::ostream& output,
					const Person& person)
	{
		output << person.getName() << " "
			<< person.getSurname() << " ";

		switch (person.getSex()) {
		case male:
			output << STR_MALE << " ";
			break;
		case female:
			output << STR_FEMALE << " ";
			break;
		default:
			output << " ";
		}

		output << person.getBirthYear() << " ";

		output << person.getFamilyID();

		return output;
	}

	std::fstream& operator <<(std::fstream& output, const Person& person)
	{
		output.write(person.getName(), PERSON_NAME_BUFF_LEN);
		output.write(person.getSurname(), PERSON_SNAME_BUFF_LEN);

		output << person.getSex();
		output << person.getBirthYear();
		output << person.getFamilyID();

		return output;
	}



	//	std::istream& operator >>(std::istream& input, Person& person)
	//	{
	//		std::cout << "Enter Name: ";
	//		char *u_name;
	//		u_name = new char[PERSON_NAME_BUFF_LEN + 1];
	//		input.getline(u_name, PERSON_NAME_BUFF_LEN,
	//			CHAR_NEWLINE);
	//		person.setName(u_name);
	//		delete [] u_name;
	//
	//		std::cout << "Enter Surname: ";
	//		char *u_surname;
	//		u_surname = new char[PERSON_SNAME_BUFF_LEN + 1];
	//		input.getline(u_surname, PERSON_SNAME_BUFF_LEN,
	//			CHAR_NEWLINE);
	//		person.setSurname(u_surname);
	//		delete [] u_surname;
	//
	//		std::cout << "Enter Sex (m/f): ";
	//		char u_sex;
	//		input >> u_sex;
	//		person.setSex(((u_sex == CHAR_MALE) ? male : female));
	//
	//		std::cout << "Enter Birth Year: ";
	//		year_t u_birth_year;
	//		input >> u_birth_year;
	//		person.setBirthYear(u_birth_year);
	//
	//// *** This part is out of use because conflicts with Family class ***
	////		std::cout << "Enter Family ID: ";
	////		code_t u_family_id;
	////		input >> u_family_id;
	////		person.setFamilyID(u_family_id);
	//
	//		return input;
	//	}
	//	

	std::istream& operator >>(std::istream& input, Person& person)
	{
		char *u_name;
		u_name = new char[PERSON_NAME_BUFF_LEN + 1];
		input.getline(u_name, PERSON_NAME_BUFF_LEN,
			CHAR_NEWLINE);
		person.setName(u_name);
		delete [] u_name;

		char *u_surname;
		u_surname = new char[PERSON_SNAME_BUFF_LEN + 1];
		input.getline(u_surname, PERSON_SNAME_BUFF_LEN,
			CHAR_NEWLINE);
		person.setSurname(u_surname);
		delete [] u_surname;

		char u_sex;
		input >> u_sex;
		person.setSex(((u_sex == CHAR_MALE) ? male : female));

		year_t u_birth_year;
		input >> u_birth_year;
		person.setBirthYear(u_birth_year);

		unsigned int u_family_id;
		input >> u_family_id;
		person.setFamilyID(u_family_id);

		return input;
	}

	//	
	//	std::fstream& operator <<(std::fstream& output,
	//		const Person& person)
	//	{
	//
	//		output << person.getName() << " "
	//			<< person.getSurname() << " ";
	//
	//		switch (person.getSex()) {
	//		case male:
	//			output << STR_MALE << " ";
	//			break;
	//		case female:
	//			output << STR_FEMALE << " ";
	//			break;
	//		default:
	//			output << " ";
	//		}
	//
	//		output << person.getBirthYear() << " ";
	//
	//		output << person.getFamilyID();
	//
	//		return output;
	//	}
	//
	//	std::ifstream& operator >>(std::ifstream& input, Person& person)
	//	{
	//		char *u_name;
	//		u_name = new char[PERSON_NAME_BUFF_LEN + 1];
	//		input.read(u_name, PERSON_NAME_BUFF_LEN);
	//		u_name[PERSON_NAME_BUFF_LEN] = CHAR_NULL;
	//		person.setName(u_name);
	//		delete [] u_name;
	//
	//		char *u_surname;
	//		u_surname = new char[PERSON_SNAME_BUFF_LEN + 1];
	//		input.read(u_surname, PERSON_SNAME_BUFF_LEN);
	//		u_surname[PERSON_NAME_BUFF_LEN] = CHAR_NULL;
	//		person.setSurname(u_surname);
	//		delete [] u_surname;
	//
	//		char u_sex;
	//		input.read(u_sex,sizeof(sex_t));
	//		person.setSex(u_sex);
	//
	//		year_t u_birth_year;
	//		input.read(u_birth_year, sizeof(year_t));
	//		person.setBirthYear(u_birth_year);
	//
	//		unsigned int u_family_id;
	//		input.read(u_family_id, sizeof(unsigned int));
	//		person.setFamilyID(u_family_id);
	//
	//		return input;
	//	}

	// *** End Of Person Class ***


	namespace { // Utilities namespace

		unsigned int strlen(const char* string)
		{
			unsigned int i = 0;
			for (i = 0; string[i] != CHAR_NULL; ++i)
				continue;
			return i;
		}

		const char* strcpy(const char* src, char* dest)
		{
			int len = strlen(src);
			int i = 0;

			for (i = 0; i < len; ++i)
				dest[i] = src[i];

			dest[i] = CHAR_NULL;

			return dest;
		}

	} // End of utilities namespace

} //End of Tasova Namespace
