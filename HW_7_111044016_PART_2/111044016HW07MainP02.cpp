/* 
 * File:   111044016HW07Main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 20, 2012, 4:11 PM
 */

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "111044016HW07FamilyP02.h"


using namespace std;
using namespace Tasova;

// ****Some useful constants

// **Some exit codes
const unsigned int INPUT_FILE_CANNOT_FOUND = 10;

// **A unique person object to point end of family
const Person EMPTY_PERSON_TEST("JOHN", "DOE", male, 1000);

// **Name of input file
const char *FILE_NAME = "111044016HW07Families.bin";

// ** Size of families array
const unsigned int FAMILIES_ARR_SIZE = 4;

// **constant strings for yes/no
const char *YES = "Yes";
const char *NO = "No";
const char *RIGHT_ARROW = "==>";

// ** Some homework Specific constants
const char *NICHOLAS_I_POSTFIX = " I";
const char *ALEXANDER_II_POSTFIX = " II";
const char *ALEXANDER_III_POSTFIX = " III";
const char *NICHOLAS_II_POSTFIX = " II";

// Some other utilities
bool isRelative(const Person& p1, const Person& p2,
		Family** fams, int numOfFams);

/*
 * 
 */
int main(int argc, char** argv)
{
	// our families...
	Family fam1, fam2, fam3, fam4;

	// Lets read families
	fstream input(FILE_NAME, ios::in | ios::binary);
	if (input.is_open()) {
		cout << "Reading data from \"" << FILE_NAME << "\"" << endl;
		cout << endl;

		fam1.readFromBinaryFile(input);
		fam2.readFromBinaryFile(input);
		fam3.readFromBinaryFile(input);
		fam4.readFromBinaryFile(input);
		input.close();
		cout << "Reading done...\n\n\n";
	} else {
		cout << "Cannot find file \"" << FILE_NAME << "\"" << endl;
		cout << endl;
		exit(INPUT_FILE_CANNOT_FOUND);
	}

	// Print our families to screen
	cout << "The Royal Families are...\n\n";
	cout << fam1.getFather().getName() << NICHOLAS_I_POSTFIX;
	cout << "'s Family:\n" << fam1 << endl;

	cout << fam2.getFather().getName() << ALEXANDER_II_POSTFIX;
	cout << "'s Family:\n" << fam2 << endl;

	cout << fam3.getFather().getName() << ALEXANDER_III_POSTFIX;
	cout << "'s Family:\n" << fam3 << endl;

	cout << fam4.getFather().getName() << NICHOLAS_II_POSTFIX;
	cout << "'s Family:\n" << fam4 << endl;

	// lets collect our families in an array of families
	Family * families[FAMILIES_ARR_SIZE];
	families[0] = &fam1;
	families[1] = &fam2;
	families[2] = &fam3;
	families[3] = &fam4;

	bool related = isRelative(fam2.getFather(), fam4.getFather(),
		families, FAMILIES_ARR_SIZE);

	cout << "Is " << families[1]->getFather().getName();
	cout << ALEXANDER_II_POSTFIX << " related with ";
	cout << families[3]->getFather().getName();
	cout << NICHOLAS_II_POSTFIX << "?\n";

	if (related)
		cout << RIGHT_ARROW << YES << endl;
	else
		cout << RIGHT_ARROW << NO << endl;

	return(EXIT_SUCCESS);
}

// Some other utilities

bool isRelative(const Person& p1, const Person& p2,
	Family** fams, int numOfFams)
{
	bool foundP1 = false, foundP2 = false;
	if (&p1 == &p2)
		return true; // if both person are the same then return true 

	size_t i = 0, k = 0, m = 0;

	for (i = 0; i < numOfFams; ++i) {

		if (p1 == fams[i]->getFather())
			foundP1 = true;
		else if (p1 == fams[i]->getMother())
			foundP1 = true;
		else if (!foundP1)
			for (k = 0; k < fams[i]->getChildrenNumber(); ++k)
				if (p1 == fams[i]->getChild(k)) {
					foundP1 = true;
					break;
				}

		if (foundP1) {
			for (k = 0; k < numOfFams; ++k) {
				if (p2 == fams[k]->getFather())
					foundP2 = true;
				else if (p2 == fams[k]->getMother())
					foundP2 = true;
				else if (!foundP2)
					for (m = 0
					     ;(m < fams[k]->getChildrenNumber())
					     ;++m)
						if (p2 == fams[k]->getChild(m)){
							foundP2 = true;
							break;
						}
			}
		} else {
			return foundP1;
		}


	}

	return(foundP1 && foundP2);

}