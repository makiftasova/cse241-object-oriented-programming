/* 
 * File:   111044016HW07Family.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 20, 2012, 4:15 PM
 */

#ifndef FAMILY_H
#define	FAMILY_H

#include "111044016HW07PersonP02.h"
#include <iostream>
#include <fstream>

namespace Tasova {

	class Family {
	public:
		Family();
		Family(Person u_fahter, Person u_mother);
		Family(const Family& orig);
		~Family();
	
		// Mutator Functions
		void setFather(Person u_father);
		void setMother(Person u_mother);
                void setChildren(Person* u_children);
			
		// Accessor Functions
		Person getMother(void) const;
		Person getFather(void) const;
		Person getChild(size_t index) const;
                Person* getChildren(void) const;
		size_t getChildrenNumber(void) const;
		unsigned int getFamilyID(void) const;
		
		// Binary File Input/Output functions
		//	both function returns true if success
		const bool writeToBinaryFile(std::fstream& file) const;
		const bool readFromBinaryFile(std::fstream& file);
		
		// Statics
		static size_t getNumberOfFamiliesAlive(void);
		
		// Operators
		const Family operator =(const Family& source);
		const Family operator +(const Family& other);
		const Family& operator +=(const Person& new_born);
		const Person& operator [](const size_t index) const;
		const bool operator ==(const Family& other) const;
		const bool operator !=(const Family& other) const;
		
		// Unavoidable friend operators
		friend std::ostream& operator <<(std::ostream& output,
							const Family& family);
		
		friend std::istream& operator >>(std::istream& input,
							Family& family);
                               
		
	private:
		Person father;
		Person mother;
		size_t family_id;
		Person *children;
		size_t num_of_child;
		size_t cap_of_child;
		
		// this is useless for outer world
		size_t getChildrenCapacity(void) const;
		
		// only i am able to give an id to a family
		void setFamilyID(size_t u_family_id); 
		
		static size_t total_number_of_families;
		static void familyCreated(void);
		static void familyDied(void);
		

	};
	
	namespace { // Utilities namespace
		
		// John Doe -- constant male person object
		const Person JOHN("John", "Doe", male, 2012);
		// Jane Doe -- constant female person object
		const Person JANE("Jane", "Doe", female, 2012);
		// Empty Person -- sign of end of family
		const Person EMPTY_PERSON("JOHN", "DOE", male, 1000);
		
		// constant initialize integers for children array
		const size_t FAMILY_INIT_CAP = 2;
		const size_t FAMILY_INIT_NUM = 0;
		
	

	} // End of utilities namespace

} //End of Tasova Namespace



#endif	/* FAMILY_H */

