

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * A Class for reading HW11_111044016_Shape objects from binary file and writing
 * HW11_111044016_Shape objects to a binary file
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public final class HW11_111044016_ShapePanelBinaryIO implements Serializable {

    private String _fileName;
    private FileOutputStream _outputFile;
    private FileInputStream _inputFile;
    private ObjectOutputStream _binOut;
    private ObjectInputStream _binIn;

    /**
     * Default Constructor. Initializes input/output file name as
     * 111044016Shapes.bin
     */
    public HW11_111044016_ShapePanelBinaryIO() {
        setFileName("111044016Shapes.bin");
    }

    /**
     * Initializes input/output file name as desired name
     *
     * @param fileName Desired file name
     */
    public HW11_111044016_ShapePanelBinaryIO(String fileName) {
        setFileName(fileName);
    }

    /**
     * Writes ingredients of given HW11_111044016_ShapePanel object to binary
     * file
     *
     * @param panel HW11_111044016_ShapePanel object which contains Shapes
     * @return How many object written into file
     */
    public int writeToFile(HW11_111044016_ShapePanel panel) {
        int objCount = 0;
        try {
            _outputFile = new FileOutputStream(_fileName);
            _binOut = new ObjectOutputStream(_outputFile);

            ArrayList<HW11_111044016_Shape> tmp = panel.toArrayList();
            for (HW11_111044016_Shape s : tmp) {
                ++objCount;
                _binOut.writeObject(s);
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                if (_outputFile != null) {
                    _outputFile.close();
                }

                if (_binOut != null) {
                    _binOut.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }
        }

        System.err.println(objCount + " Shapes Successfuly Wrote!");
        return objCount;
    }

    /**
     * Reads all shapes from a Binary file which only contains
     * HW11_111044016_Shape Objects to given HW11_111044016_ShapePanel Object
     *
     * @param panel HW11_111044016_ShapePanel object to write Shapes
     * @return How many object read from file
     */
    public int readFromFileTo(HW11_111044016_ShapePanel panel) {

        int objCount = 0;
        try {
            _inputFile = new FileInputStream(_fileName);
            _binIn = new ObjectInputStream(_inputFile);

            try {
                HW11_111044016_Shape tmp;
                while (true) {
                    tmp = (HW11_111044016_Shape) _binIn.readObject();
                    ++objCount;
                    if (tmp == null) {
                        --objCount;
                        break;
                    }
                    panel.addShape(tmp);
                }

            } catch (EOFException ex) {
            }

        } catch (ClassNotFoundException | FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        } finally {
            try {
                if (_inputFile != null) {
                    _inputFile.close();
                }

                if (_binIn != null) {
                    _binIn.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }
        }

        System.err.println(objCount + " Shapes Successfuly Read!");
        return objCount;
    }

    /**
     * Changes current file name of HW11_111044016_ShapePanelBinaryIO object
     * with desired name
     *
     * @param fileName Desired file name of HW11_111044016_ShapePanelBinaryIO
     * object
     */
    public void setFileName(String fileName) {
        _fileName = fileName;
    }

    /**
     * Returns current file name of HW11_111044016_ShapePanelBinaryIO object
     *
     * @return Current file name of HW11_111044016_ShapePanelBinaryIO object
     */
    public String name() {
        return _fileName;
    }
}
