/* 
 * File:   111044016HW06Family.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on November 20, 2012, 4:15 PM
 */

#include "111044016HW06Family.h"


namespace Tasova {

	// let's initialize static variables immediately
	unsigned int Family::total_number_of_families(0);

	Family::Family() : father(JOHN), mother(JANE),
	num_of_child(FAMILY_INIT_NUM), cap_of_child(FAMILY_INIT_CAP),
	family_id(total_number_of_families)
	{
		father.setFamilyID(family_id);
		mother.setFamilyID(family_id);
		father.setMaritalStatus(married);
		mother.setMaritalStatus(married);
		children = new Person[cap_of_child];
		familyCreated();
	}

	Family::Family(Person u_fahter, Person u_mother)
	: father(u_fahter), mother(u_mother),
	num_of_child(FAMILY_INIT_NUM), cap_of_child(FAMILY_INIT_CAP),
	family_id(total_number_of_families)
	{
		father.setFamilyID(family_id);
		mother.setFamilyID(family_id);
		father.setMaritalStatus(married);
		mother.setMaritalStatus(married);
		children = new Person[cap_of_child];
		familyCreated();
	}

	Family::Family(const Family& orig)
	: father(orig.getFather()), mother(orig.getMother()),
	num_of_child(orig.getChildrenNumber()),
	cap_of_child(orig.getChildrenCapacity()), family_id(orig.getFamilyID())
	{
		father.setFamilyID(family_id);
		mother.setFamilyID(family_id);
		father.setMaritalStatus(married);
		mother.setMaritalStatus(married);
		children = new Person[cap_of_child];
		size_t i(0);
		for (i = 0; i < num_of_child; ++i)
			children[i] = orig.getChild(i);
		
		familyCreated();
	}

	Family::~Family()
	{
		delete [] children;
		familyDied();
	}

	void Family::setFather(Person u_father)
	{
		father = u_father;
	}

	void Family::setMother(Person u_mother)
	{
		mother = u_mother;
	}

	void Family::setChildren(Person* u_children)
	{
		children = u_children;
	}

	void Family::setFamilyID(size_t u_family_id)
	{
		family_id = u_family_id;
	}

	Person Family::getFather(void) const
	{
		return father;
	}

	Person Family::getMother(void) const
	{
		return mother;
	}

	Person Family::getChild(size_t index) const
	{
		if (index >= getChildrenNumber())
			return children[(getChildrenNumber() - 1)];
		else
			return children[index];
	}

	Person* Family::getChildren(void) const
	{
		return children;
	}

	size_t Family::getChildrenNumber(void) const
	{
		return num_of_child;
	}

	size_t Family::getFamilyID(void) const
	{
		return family_id;
	}

	size_t Family::getChildrenCapacity(void) const
	{
		return cap_of_child;
	}

	const Family Family::operator =(const Family& source)
	{
		if (this != &source) {
			father = source.getFather();
			mother = source.getMother();
			father.setFamilyID(family_id);
			mother.setFamilyID(family_id);
			num_of_child = source.getChildrenNumber();
			cap_of_child = source.getChildrenCapacity();
			family_id = source.getFamilyID();
			delete [] children;
			children = new Person[cap_of_child];
			size_t i(0);
			for (i = 0; i < num_of_child; ++i)
				children[i] = source.getChild(i);
		}

		return *this;

	}

	const Family Family::operator +(const Family& other)
	{
		size_t groom_index(0);
		size_t i(0);
		for (i = 0; i < this->getChildrenNumber(); ++i) {
			if ((this->getChild(i).getSex() == male) &&
			    (this->getChild(i).getMaritalStatus() == single)) {
				groom_index = i;
				break;

			}
		}

		if (i == this->getChildrenNumber()) {
			std::cout << "No Suitable Groom Found!\n";
			std::cout << "Returning Doe Family\n";
			return Family();
		}

		size_t bride_index(0);
		for (i = 0; i < other.getChildrenNumber(); ++i) {
			if ((other.getChild(i).getSex() == female) &&
			    (other.getChild(i).getMaritalStatus() == single)) {
				bride_index = i;
				break;
			}

		}

		if (i == other.getChildrenNumber()) {
			std::cout << "No Suitable Bride Found!\n";
			std::cout << "Returning Doe Family\n";
			return Family();
		}

		this->getChild(groom_index).setMaritalStatus(married);
		other.getChild(bride_index).setMaritalStatus(married);

		return Family(this->getChild(groom_index),
			other.getChild(bride_index));

	}

	 void Family::operator +=(const Person& new_born)
	{
		 Person my_baby(new_born);
		 my_baby.setFamilyID(getFamilyID());
		 
		 if(getChildrenNumber() >= cap_of_child){
			Person* tmp(new Person[cap_of_child]);
			size_t i(0);
			for(i=0; i<getChildrenNumber(); ++i)
				tmp[i] = children[i];
			delete [] children;
			
			while(getChildrenNumber() >= cap_of_child)
				cap_of_child *= 2;
			
			children = new Person[cap_of_child];
			for(i=0; i<getChildrenNumber(); ++i)
				children[i] = tmp[i];
			delete [] tmp;
		 }
		 
		 children[getChildrenNumber()] = my_baby;
		 ++num_of_child;
		 
//		 return *this;
	}

	const Person& Family::operator [](const size_t index) const
	{
		if (index >= getChildrenNumber())
			return children[(getChildrenNumber() - 1)];
		else
			return children[index];
	}
	
	const bool Family::operator ==(const Family& other) const
	{
		return (getFamilyID() == other.getFamilyID());
	}
	
	const bool Family::operator !=(const Family& other) const
	{
		return (getFamilyID() != other.getFamilyID());
	}

	// Unavoidable friend operators

	std::ostream& operator <<(std::ostream& output, const Family & family)
	{
		output << "Father: " << family.getFather() << std::endl;
		output << "Mother: " << family.getMother() << std::endl;
		output << "Childs: \n";

		size_t i(0);
		for (i = 0; i < family.getChildrenNumber(); ++i)
			output << family.getChild(i) << std::endl;

		return output;
	}

	std::istream& operator >>(std::istream& input, Family & family)
	{
		Person father, mother;
		std::cout << "Enter Father's Data:\n";
		input >> father;

		std::cout << std::endl;
		char garb;
		input.get(garb);

		std::cout << "Enter Mother's Data:\n";
		input >> mother;
		input.get(garb);

		family = Family(father, mother);
		
		size_t i(0), count(0);
		std::cout << "How many kids this family have (0 or more) ==>";
		input >> count;
		input.get(garb);
		Person kid;
		for(i=0; i<count; ++i ){
			std::cout << "Kid #" << (i+1) << std::endl;
			input >> kid;
			input.get(garb);
			family += kid;			
		}

		return input;
	}

	// Static part of class

	size_t Family::getNumberOfFamiliesAlive(void)
	{
		return Family::total_number_of_families;
	}

	void Family::familyCreated(void)
	{
		Family::total_number_of_families += static_cast<size_t>(1);
	}

	void Family::familyDied(void)
	{
		Family::total_number_of_families -= static_cast<size_t>(1);
	}


	// *** End Of Family Class ***

	namespace { // Utilities namespace

	} // End of utilities namespace

} //End of Tasova Namespace

