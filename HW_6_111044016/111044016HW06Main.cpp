/* 
 * File:   111044016HW06Main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 20, 2012, 4:11 PM
 */

#include <iostream>
#include "111044016HW06Family.h"
#include <cstring> // for strcmp function

using namespace std;
using namespace Tasova;


bool checkRelativity(const Family fam, const Person& per1, const Person& per2);

/*
 * 
 */
int main(int argc, char** argv)
{

	Person father1("Hilmi", "Ucak", male, 1961);
	Person mother1("Nurfidan", "Ucak", female, 1969);

	Family f1(father1, mother1);


	Person father2("Hasan", "Kerimoglu", male, 1961);
	Person mother2("Gülistan", "Kerimoglu", female, 1969);

	Family f2(father2, mother2);

	cout << "\tOur base families:\nFamily #1:\n" << f1 << endl;
	cout << "Family #2:\n" << f2 << endl << endl;

	Person f1k1("Mehmet", "Ucak", male, 1993);
	Person f1k2("Nergis", "Ucak", female, 1998);

	f1 += f1k1;
	f1 += f1k2;

	Person f2k1("Hakan", "Kerimoglu", male, 1993);
	Person f2k2("Neriman", "Kerimoglu", female, 1993);
	Person f2k3("Nagihan", "Kerimoglu", female, 1989);
	Person f2k4("Nalan", "Kerimoglu", female, 1989);
	Person f2k5("Naciye", "Kerimoglu", female, 1989);
	Person f2k6("Nuket", "Kerimoglu", female, 1989);


	f2 += f2k1;
	f2 += f2k2;
	f2 += f2k3;
	f2 += f2k4;
	f2 += f2k5;
	f2 += f2k6;


	cout << "\tOur base families with kids:\nFamily #1:\n" << f1 << endl;
	cout << "Family #2:\n" << f2 << endl << endl;


	cout << "A wedding ceremony...\n";

	Family f3;
	f3 = (f1 + f2);

	cout << "Our Fresh family a.k.a. Family #3:\n" << f3 << endl;
	
	Person f3k1("Hilmi", f3.getFather().getSurname(), male, 1986);
	
	f3 += f3k1;
	
	cout << "Last state of Famile #3:\n" << f3 << endl << endl;
	
	cout << "2nd child of Family # 1: \n" << f1.getChild(1) << endl << endl;
	cout << "same thing with operator []\n";
	cout << "2nd child of Family # 1: \n" << f1[1] << endl << endl;
	
	cout << "Are Family #3 and Family #2 same? ==> " << boolalpha;
	cout << (f2 == f3) << endl << endl;
	
	cout << "is (f1 != f2) ==> " << boolalpha << (f1 != f2) << endl << endl;
	
	cout << "is father of Family #2 related to mother of Family #1 ==> ";
	cout << boolalpha;
	cout << checkRelativity(f1, f2.getFather(),f1.getMother()) << endl;
	
	cout << endl << Family::getNumberOfFamiliesAlive();
	cout << " families used when testing\n";
	
	return 0;
}

bool checkRelativity(const Family fam, const Person& per1, const Person& per2)
{
	bool result=false;
	
	if(per1.getFamilyID() == per2.getFamilyID())
		result = true;
	else if (strcmp(per1.getSurname(), per2.getSurname()))
		result == true;
	else if (strcmp(per1.getSurname(), fam.getFather().getSurname()) &&
		 strcmp(per2.getSurname(),fam.getFather().getSurname()))
		result == true;
	
	
	return result;
}