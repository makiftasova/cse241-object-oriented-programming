/* 
 * File:   111044016HW03.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on October 3, 2012, 9:07 AM
 */

#include <iostream>
#include <string>

using namespace std;

// Constant Exit Codes
const int EXIT_SUCCESS=0;
const int EXIT_STR3_TOO_SHORT=1;
const int EXIT_STR3_TOO_LONG=2;
const int EXIT_TOO_MANY_UNIQUES=3;

// Constant function return codes 
const int SUCCESS=1;
const int FAIL=0;
const int FAILED_TO_FIND=-1; // failed to find code for getIndex function

// Miscellaneous Constants
const char CHAR_ONE='1'; // const char which represents 1 as char type
const char CHAR_ZERO='0'; // const char which represents 0 as char type
const char NULL_CHAR='\0'; // NULL character as string terminator
const int ARR_SIZE=11; // maximum kength of arrays
const int UNQ_ARR_SIZ=10; // int unique number array size
const int ONE=1; // int 1 for manipulating nums array
const int ZERO=0; // int 0 for manipulating nums array

// Function Prototypes

int pow (int base, int exponent);
//  Takes base and exponent as integer, then calculates base's exponenth power
//  Precondition: given arguments type must be integer

void strReverse(char str[]);
//  Takes a string reference then reverses the referenced string

int strlen(const char str[]);
// counts total number of elements of a string wihtout null character
// Precondition: given char array must contain a null characker as known as
//  string-terminator

int strToInt(const char str[]);
//   Takes a number filled string and returns its ingredients as integer
//   Precondition: string must contain only characters between 48 to 57 as 
//      ascii code (including 48 and 57, mean '0' to '9')
//   
//   Precondition2: math library must be included or pow function 
//      must be defined separately (function uses pow function when calculating
//      digit values )
//   
//   If any letters found in string, function cuts to convertion and returns
//          calculated value as int

int replaceAll(char toFind, char toRepalce, 
               char str1[], char str2[], char str3[]);
//  Finds char toFind in all strings, and replaces it with char toReplace
//  Returns total number of changes made

int replace(char toFind, char toRepalce, char str[]);
//  Finds char toFind in string, and replaces it with char toReplace
//  Returns total number of changes made

void append (char str[], char toAppend);
// Appends given char to given string
// Precondition: string must contain a null character at end

void strcpy (const char source[], char dest[]);
// copies source string to destination string
// Precondition1: source string must contain a null char
// Precondition2: destination string's allocated area must be equal or greater
//  than source string

int getIndex(const char str[], char toFind);
// Find given character toFind in string and return its index
//  returns index of first occurance
//  return -1 if cannot found character in string
// Precondition: string must contain a null character

void fillWithNull(char str[], int size);
// fills given char array with nulls

void fillWithZero(int arr[], int size);
// fills given int array with zeros

char numberToChar(const int number);
// converts given number to character

// one function to rule them all
int main(void) 
{
    cout << "NOTICE: This program cannot compute the solved form of "
         << "given cryptarithmetic problem yet\n"
         << "It colud never solve any of cryptarithmetic, but it still tries\n";
    
    // **GET DATA**
    // input container strings
    char str1[ARR_SIZE], str2[ARR_SIZE], str3[ARR_SIZE];
    
    // Let's make the arrays secure to use
    fillWithNull(str1, ARR_SIZE);
    fillWithNull(str2, ARR_SIZE);
    fillWithNull(str3, ARR_SIZE);
    
    // get strings
    cout << "Enter the first string: ";
    cin >> str1;
    cout << "Enter the second string: ";
    cin >> str2;
    cout << "Enter the third string: ";
    cin >> str3;
    
    char uniques[ARR_SIZE];
    int i=0, j=0; //loop counter
    bool positive; //flag for choosing unique letters
    
    fillWithNull(uniques, ARR_SIZE);
    
    // check uniques in 3rd string so must-to-be one digit can easily found
    for (i=0; i < strlen(str3); ++i) {
        for (j=0; j < strlen(uniques); ++j){
            if (str3[i] == uniques[j]){
                positive = false;
                break;
            } else {
                positive = true;
            }
        }
        if (positive)
            append(uniques, str3[i]);
    }
    
    // check uniques in 1st string
    for (i=0; i < strlen(str1); ++i) {
        for (j=0; j < strlen(uniques); ++j){
            if (str1[i] == uniques[j]){
                positive = false;
                break;
            } else {
                positive = true;
            }
        }
        if (positive)
            append(uniques, str1[i]);
    }
    
    // check uniques in 2nd string
    for (i=0; i < strlen(str2); ++i) {
        for (j=0; j < strlen(uniques); ++j){
            if (str2[i] == uniques[j]){
                positive = false;
                break;
            } else {
                positive = true;
            }
        }
        if (positive)
            append(uniques, str2[i]);
    }
    
    // get number of unique letters then print it to stdout
    int uniques_size=strlen(uniques); // length of unique letter string
    
    
    // print error if illegal inputs given
    if(uniques_size > 10) {
        cout << "Total number of unique letters should be"
             << " less or equal to 10.\n";
        return EXIT_TOO_MANY_UNIQUES; // too many unique letters

    } else if ((strlen(str1) > strlen(str3)) || (strlen(str2) > strlen(str3))){
	    cout << "There is no solution.\n"
             << "\t3rd string is TOO SHORT to solve\n";
        return EXIT_STR3_TOO_SHORT; // string is too short
        
    } else if ((strlen(str3) > (strlen(str1) + 1)) || 
               (strlen(str3) > (strlen(str2) + 1))) {
        cout << "There is no solution.\n" 
             << "\t3rd string is TOO LONG to solve\n";
        return EXIT_STR3_TOO_LONG; // strings is too long
        
    }
    
    // **BEGIN MANIPULATION**
    // manipulated strings
    char man1[ARR_SIZE], man2[ARR_SIZE], man3[ARR_SIZE];
    // other variables for manipulations
    
    int numsPerUniques[UNQ_ARR_SIZ]={0}; // int correspondings on unique letters
    fillWithZero(numsPerUniques, UNQ_ARR_SIZ);
    
    strcpy(str1, man1);
    strcpy(str2, man2);
    strcpy(str3, man3);
    
    char tmp=man3[0];
    int index=0;
    bool isStatic=false;
    
    if ((strlen(str3) == (strlen(str1) + 1)) && 
        (strlen(str3) == (strlen(str2) + 1))) {
        // if true first digit of result must be one 
        // so lets make it
        index = getIndex(uniques, tmp);
        numsPerUniques[index] = ONE;
        // and mark first element of array as cannot changable
        isStatic = true;         
    } 
    
    // reverse all the string, so it will be easier to manipulate
    strReverse(man1);
    strReverse(man2);
    strReverse(man3);
    
    // check for must-to-be-zero letters
    if(man1[0] == man3[0]){
        index = getIndex(uniques, man2[0]);
        numsPerUniques[index] = ZERO;
    }
    
    if(man2[0] == man3[0]){
        index = getIndex(uniques, man1[0]);
        numsPerUniques[index] = ZERO;
    }
    
    int nbrToAssgn=0; // the number which will be assigned to letters for test
    int numOfMaxTests=1; //maximum number of possible tests
    int num1, num2, num3; // integer forms of strings
    int temp=0; // do not assing this value to any variable
    int numOfFilled=0; // number of filled numbers in int array
    bool found=false, usable=false;
    
    i=0, j=0; //reset previously used loop counters to zero
    int k=0, l=0; // additional loop counters 
    
    cout << "The solutions are:\n";

    strcpy(str1, man1);
    strcpy(str2, man2);
    strcpy(str3, man3);
            
    for(i=(uniques_size-1); i>=0; --i){
        for(j=(uniques_size-1); j>=0; --j){
            if((j == 1) && (numsPerUniques[j] <= 1)){
                continue;      
            } else if((j == 2) && ((numsPerUniques[j]+1) > 9)){
                break;
            }else if(numsPerUniques[j] > 9){
                numsPerUniques[j-1] += 1;
                numsPerUniques[j] -= 10;
            } else {
                numsPerUniques[j]++;
            }
                    if((numsPerUniques[j] == numsPerUniques[j-1]) && (j != 1)){
                if(numsPerUniques[j] != 0)
                    numsPerUniques[j] --;
                else
                    numsPerUniques[j]++;
            } 
        }
        if((numsPerUniques[i] == numsPerUniques[i-1]) && (i != 1)){
                if(numsPerUniques[i] != 0)
                    numsPerUniques[i] --;
                else
                    numsPerUniques[i]++;
        }
    }
    
    for(i=0; i<uniques_size; ++i){
        if((numsPerUniques[i] == numsPerUniques[i+1]) 
          && (i != (uniques_size-1))){
            
            numsPerUniques[i+1]++;
        }            

    }
            
    for(i=0; i<uniques_size; ++i)
        replaceAll(uniques[i], numberToChar(numsPerUniques[i]),
                   man1, man2, man3);
            
    num1 = strToInt(man1);
    num2 = strToInt(man2);
    num2 = strToInt(man2);
            
            
    // if test success print the possibilities
    if((num1 + num2) == num3){
        found = true;
        cout << "( ";
        for(i=0; i<uniques_size; ++i){
            cout << uniques[i] << "=" << numsPerUniques[i];
            if(i != (uniques_size - 1))
                cout << ", ";
        }                
        cout << " )\n";
    }
    
    if(!found)
        cout << "No Solution Found\n";
    
    return EXIT_SUCCESS; //run success    
}

int pow (int base, int exponent)
{
    int power=1;
    
    for(int i = 0; i < exponent; ++i){
        power *= base;
    }
    
    return power;
}

void strReverse(char str[])
{
    int i=0, len;
    char tmp;
    
    len = strlen(str);
    
    for (i=0; i < (len / 2); ++i){
        tmp = str[i];
        str[i] = str[(len - (i + 1))];
        str[(len - (i + 1))] = tmp;
    }
}

int strlen(const char str[])
{
    int length=0;
    
    for(length=0; str[length] != NULL_CHAR; ++length)
        continue;
    
    return length;
            
}
    
int strToInt (const char str[])
{
    int number=0, len=0, tmp=0, i=0;
    
    len = strlen(str); // get the complete strings length
    
    // find the length of continuous number array in string
    while(i < len){
        if((static_cast<int>(str[i]) >= 58) || (static_cast<int>(str[i]) <= 47))
        {
            len = i;
            break;
        }        
        ++i;
    }
    
    // convert string to integer
    for (i = 0; (i < len); ++i){
        
        if((static_cast<int>(str[i]) < 58) && (static_cast<int>(str[i]) > 47)){
            
            // cast char type to int type
            tmp = (static_cast<int>(str[i]) - 48);
        
            number += (tmp * pow(10, (len - (i + 1))));
            
          //  break;
        } else {
            break;
        }
    } 
    
    return number;
}

int replaceAll(char toFind, char toRepalce, 
               char str1[], char str2[], char str3[])
{
    int i=0, count=0, len1=0, len2=0, len3=0;
    
    len1 = strlen(str1);
    len2 = strlen(str2);
    len3 = strlen(str3);
    
    for(i=0; i<len1; ++i){
        if(str1[i] == toFind){
            str1[i] = toRepalce;
            count++;
        }
    }
    
    for(i=0; i<len2; ++i){
        if(str2[i] == toFind){
            str2[i] = toRepalce;
            count++;
        }
    }
    
    for(i=0; i<len3; ++i){
        if(str3[i] == toFind){
            str3[i] = toRepalce;
            count++;
        };
    }
    
    return count;
}

int replace(char toFind, char toRepalce, char str[])
{
    int i=0, count=0, len=0;
    
    len = strlen(str);
    
    for(i=0; (i<len) ; ++i){
        if(str[i] == toFind){
            str[i] = toRepalce;
            count++;
        }
    }
    
    return count;
}

void append (char str[], char toAppend)
{
    int len=strlen(str);
    
    if(str[len] == NULL_CHAR){
        str[len] = toAppend;
        str[len+1] = NULL_CHAR;
    }
    
}

void strcpy (const char source[], char dest[])
{
    int i=0, len=strlen(source);
    
    for(i=0;i<len;++i){
        dest[i] = source[i];
    }
    dest[len] = NULL_CHAR;
}

int getIndex(const char str[], char toFind)
{
    int i=0, index=FAILED_TO_FIND;
    
    for(i=0; str[i]!=NULL_CHAR; ++i){
        if(str[i] == toFind){
            index = i;
            break;
        }
    }
    
    return index;
}

void fillWithNull(char str[], int size)
{
    int i=0;
    for (i=0; i< size; ++i)
        str[i] = NULL_CHAR;
}

void fillWithZero(int arr[], int size)
{
    int i=0;
    for(i=0; i< size; ++i)
        arr[i] = ZERO;
}

char numberToChar(const int number)
{
    char value;
    if ((number < 10) || (number > -1))
        value = static_cast<char>(number+48);
    else
        value = FAIL;
    
    return value;
}