/* 
 * File:   111044016HW09ArraySet.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 17, 2012, 11:18 PM
 */

#ifndef ARRAYSET_H
#define	ARRAYSET_H

#include "111044016HW09Set.h"



namespace Tasova {

	namespace {
		typedef unsigned int size_t;
	}

	template <typename T>
	class ArraySet : public Set<T> {
	public:

		typedef typename Set<T>::template BasicIterator<T> Iterator;


		typedef typename Set<T>::template BasicIterator<const T>
		ConstIterator;

		ArraySet(); // Default Constructor

		// Construct Set with reserved space
		ArraySet(size_t size);

		ArraySet(const ArraySet& orig); // Copy Constructor

		virtual ~ArraySet(); //Destructor

		// Adds element to set if not present
		virtual void addElement(const T& element);

		// Removes latest element from set
		virtual const T removeElement(void);

		// Removes given element from set
		virtual const T removeElement(const T& element);

		// Search given element in set
		const Iterator search(const T& element) const;

		// Return total number of elements in set
		const size_t numberOfElements(void) const;

		// Return how many elements can store in container
		//	without any capacity changes
		const size_t getCapacity(void) const;

		// Assignment Operator
		const ArraySet<T>& operator =(const ArraySet<T>& other);

		// Append another set
		const ArraySet<T>& operator +=(const ArraySet<T>& other);

		// Get union of sets
		const ArraySet<T> operator +(const ArraySet<T>& other);

		// Get intersection of sets
		const ArraySet<T> operator %(const ArraySet<T>& other);

		// Get the element at index
		const T operator [] (const size_t index) const;

		// Get the element at index (non-const)
		T operator [] (const size_t index);

		// Begin iterator
		virtual Iterator begin(void) const;

		// End iterator
		virtual Iterator end(void) const;

		// Constant Begin iterator
		virtual ConstIterator cbegin(void) const;

		// Constant End iterator
		virtual ConstIterator cend(void) const;


	private:
		T* set;
		size_t capacity;
		size_t used;

		// ** Background functions **
		// Changes maximum capacity of array without 
		//	changing its ingredients
		T* enlarge(T* set, size_t oldcap, size_t newcap, size_t inuse);

		typedef ArraySet<T>* OwnPointer;



	}; // End of class ArraySet

	// Stream insertion operator for ArraySet
	template <typename T>
	std::ostream& operator <<(std::ostream& output, const ArraySet<T>& set);

} // End Of namespace Tasova

#endif	/* ARRAYSET_H */
