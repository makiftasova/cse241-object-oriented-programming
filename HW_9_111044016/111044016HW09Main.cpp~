/* 
 * File:   main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 9, 2012, 9:56 PM
 */

#include <iostream>
#include <stdexcept>

#include "111044016HW09Set.h"
#include "111044016HW09ArraySet.h"
#include "111044016HW09VectorSet.h"

using namespace std;
using namespace Tasova;

// Tester function. if test successfully done, returns true;
const bool test(void);

int main(int argc, char** argv)
{
	bool result = false;
	// start the test...

	cout << "It's time to start testing...\n";
	cout << "--------------------\n";

	result = test();

	cout << "--------------------\n";
	if (result)
		cout << "Test success\n";
	else
		cout << "Test failed\n";

	return 0;
}

const bool test(void)
{
	try {
		// Generating test vectors
		VectorSet<char> vec1;

		vec1.addElement('A');
		vec1.addElement('B');
		vec1.addElement('C');
		vec1.addElement('D');
		vec1.addElement('E');
		vec1.addElement('F');
		vec1.addElement('G');

		VectorSet<char> vec2, vec3;

		vec2.addElement('F');
		vec2.addElement('G');
		vec2.addElement('H');
		vec2.addElement('I');
		vec2.addElement('J');



		cout << "vec1 = " << vec1 << endl;
		cout << "vec2 = " << vec2 << endl;

		cout << endl;
		cout << "vec3 = vec1 + vec2\n";
		vec3 = vec1 + vec2;
		cout << "vec3 = " << vec3 << endl;

		cout << endl;

		cout << "vec3 = vec1 % vec2\n";
		vec3 = vec1 % vec2;
		cout << "vec3 = " << vec3 << endl;
		cout << endl;
		
		vec3 += vec1;

		cout << "vec1 = " << vec1 << endl;
		cout << "vec2 = " << vec2 << endl;

		// Generating test arrays
		ArraySet<char> arr1, arr2;

		arr1.addElement('A');
		arr1.addElement('B');
		arr1.addElement('C');
		arr1.addElement('D');
		arr1.addElement('E');
		arr1.addElement('F');

		arr2.addElement('E');
		arr2.addElement('F');
		arr2.addElement('G');
		arr2.addElement('H');
		arr2.addElement('I');
		arr2.addElement('J');

		cout << "arr1 = " << arr1 << endl;
		cout << "arr2 = " << arr2 << endl;

		cout << "Now removing some elemnets...\n";
		arr1.removeElement();
		arr2.removeElement('G');
		vec1.removeElement();
		vec2.removeElement('I');

		cout << endl;
		cout << "Sets after removal...\n";
		cout << "vec1 = " << vec1 << endl;
		cout << "vec2 = " << vec2 << endl;
		cout << "arr1 = " << arr1 << endl;
		cout << "arr2 = " << arr2 << endl;
		cout << endl;

		ArraySet <char> arr3;

		arr3 = arr1 + arr2;

		cout << "arr3 = " << arr3 << endl;

		Set<char> &set1 = vec1;
		cout << endl << endl;
		cout << "set1 = " << set1 << endl;

		cout << endl;

		Set<char> *set2;

		set2 = &arr2;

		cout << "set2 = " << set2 << endl;
		
		cout << endl;
		cout << "Now appending an ArraySet to a VectorSet...\n";
		
		set1 += set2;
		cout << "set1 += set2 = " << set1 << endl;
		cout << endl;
		
		Set<char> &baseSet = arr1;
		Set<char> *setToAdd = &vec1;
		cout << "Calculating sum of arr1 and vec1...\n";
		cout << "arr1 + vec1 = " << (baseSet + setToAdd) << endl;
		cout << endl;
		
		// Iterator tests
		cout << "Iterator Testing...\n";
		ArraySet<char> tmparr;
		tmparr = arr3;
		ArraySet<char>::Iterator arritr;
		arritr = arr3.begin();
		cout << "Changing some elements of arr3...\n";
		*arritr = 'M';
		++arritr;
		*arritr = 'Z';
		++arritr;
		*arritr = 'H';
		cout << "Changes done!\n";
		cout << "before changes arr3 = " << tmparr << endl;
		cout << "after changes arr3 = " << arr3 << endl;
		cout << endl;
		
		cout << "- - - - - - - - - -\n";
		cout << "Doing same thing for vec3...\n";
		VectorSet<char> tmpvec;
		tmpvec = vec3;
		VectorSet<char>::Iterator vecitr;
		vecitr = vec3.begin();
		++vecitr;
		*vecitr = 'H';
		++vecitr;
		*vecitr = 'T';
		++vecitr;
		*vecitr = 'K';
		cout << "Changes done!\n";
		cout << "before changes vec3 = " << tmpvec << endl;
		cout << "after changes vec3 = " << vec3 << endl;
		cout << endl;
		
		cout << "- - - - - - - - - -\n";
		// Some other tests...
		cout << "vec3 % vec2 = " << (vec3 % vec2) << endl;
		cout << endl;
		
		cout << "- - - - - - - - - -\n";
		cout << "It looks enough for now...\nEnding test section...\n";
		
		
	} catch (out_of_range rng_err) {
		cout << rng_err.what() << endl;
		return false;
	} catch (bad_alloc out_of_memory) {
		cout << out_of_memory.what() << endl;
		return false;
	} catch (...) {
		cout << "Unhandled exception occurred.";
		cout << "Please contact with developer\n";
	}
	return true;
}

// Unavoidable unwanted includes

#include "111044016HW09Set.cpp"
#include "111044016HW09ArraySet.cpp"
#include "111044016HW09VectorSet.cpp"
