/* 
 * File:   111044016HW09VectorSet.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 16, 2012, 9:11 PM
 */

#ifndef VECTORSET_H
#define	VECTORSET_H

#include <vector>
#include <iostream>

#include "111044016HW09Set.h"


namespace Tasova {

	template <typename T>
	class VectorSet : public Set<T> {
	public:
		typedef typename Set<T>::template BasicIterator<T> Iterator;


		typedef typename Set<T>::template BasicIterator<const T>
		ConstIterator;


		VectorSet();
		VectorSet(const VectorSet<T>& orig);

		// ** VectorSet<T> members **

		// Adds element to set if not present
		virtual void addElement(const T& element);

		// Removes latest element from set
		virtual const T removeElement(void);

		// Removes given element from set
		virtual const T removeElement(const T& element);

		// Search given element in set
		virtual const Iterator search(const T& element) const;

		// Return total number of elements in set
		virtual const size_t numberOfElements(void) const;

		// ** Abstract operators **
		// Append another set
		virtual const VectorSet<T> operator+=(
			const VectorSet<T>& other);

		// Get union of sets
		virtual const VectorSet<T> operator+(const VectorSet<T>& other);

		// Get intersection of sets
		virtual const VectorSet<T> operator%(const VectorSet<T>& other);

		// Get the element at index
		virtual const T operator [] (const size_t index) const;
		
		// Get the element at index (non-const)
		virtual T operator [] (const size_t index);

		// Begin iterator
		virtual Iterator begin(void) const;

		// Begin iterator
		virtual Iterator end(void) const;

		// Constant Begin iterator
		virtual ConstIterator cbegin(void) const;

		// Constant End iterator
		virtual ConstIterator cend(void) const;




	private:
		std::vector<T> set;

	}; // End of class VectorSet

	// Stream insertion operator for VectorSet
	template <typename T>
	std::ostream& operator <<(std::ostream& output,
		const VectorSet<T>& set);

} // End Of namespace Tasova

#endif	/* VECTORSET_H */

