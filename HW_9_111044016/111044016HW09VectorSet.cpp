/* 
 * File:   111044016HW09VectorSet.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on December 16, 2012, 9:11 PM
 */

#include <stdexcept>
#include "111044016HW09VectorSet.h"


namespace Tasova {

	template<typename T>
	VectorSet<T>::VectorSet()
	{
		//Intentionally left blank
	}

	template<typename T>
	VectorSet<T>::VectorSet(const VectorSet<T>& orig)
	{
		this->set = orig.set;
	}

	// Adds element to set if not present

	template<typename T>
	void VectorSet<T>::addElement(const T& element)
	{
		if (this->end() == this->search(element))
			set.push_back(element);
	}

	// Removes latest element from set

	template<typename T>
	const T VectorSet<T>::removeElement(void)
	{
		T temp;
		temp = set.back();
		set.pop_back();
		set.resize(set.size());
		return temp;
	}

	// Removes given element from set

	template<typename T>
	const T VectorSet<T>::removeElement(const T& element)
	{
		typename std::vector<T>::const_iterator itr = set.begin();

		size_t i = 0;
		size_t size = set.size();
		for (i = 0; i < size; ++i) {
			if (element == set[i])
				break;
		}

		T temp;

		temp = set[i];
		set.erase(set.begin() + i);

		return temp;
	}

	// Search given element in set

	template<typename T>
	const typename VectorSet<T>::Iterator
	VectorSet<T>::search(const T& element) const
	{
		Iterator itr(this->end());

		size_t i = 0;
		size_t size = set.size();
		for (i = 0; i < size; ++i)
			if (element == set[i]) {
				itr = const_cast<T*> (&set[i]);
				break;
			}

		return itr;
	}

	// Return total number of elements in set

	template<typename T>
	const size_t VectorSet<T>::numberOfElements(void) const
	{
		return set.size();
	}

	// ** Operators **
	// Append another set

	template<typename T>
		const VectorSet<T>
		VectorSet<T>::operator +=(const VectorSet<T>& other)
	{
		size_t otherSize = other.numberOfElements();
		size_t mySize = numberOfElements();
		size_t i = 0;

		for (i = 0; i < otherSize; ++i)
			this->addElement(other[i]);

		return(*this);
	}

	// Get union of sets

	template<typename T>
	const VectorSet<T> VectorSet<T>::operator +(const VectorSet<T>& other)
	{
		VectorSet<T> temp;

		temp = *this;
		temp += other;

		return temp;
	}

	// Get intersection of sets

	template<typename T>
	const VectorSet<T> VectorSet<T>::operator %(const VectorSet<T>& other)
	{
		VectorSet<T> temp;
		size_t i = 0, j = 0;
		size_t mySize = numberOfElements();
		size_t otherSize = other.numberOfElements();

		for (i = 0; i < mySize; ++i) {
			for (j = 0; j < otherSize; ++j) {
				if ((*this)[i] == other[j])
					temp.addElement(other[j]);
			}
		}

		return temp;
	}

	// Get the element at index

	template<typename T>
	const T VectorSet<T>::operator [] (const size_t index) const
	{
		if (index >= set.size())
			throw std::out_of_range("Index out of range\n");
		return set[index];
	}

	// Get the element at index (non-const)

	template<typename T>
	T VectorSet<T>::operator [] (const size_t index)
	{
		if (index >= set.size())
			throw std::out_of_range("Index out of range\n");
		return set[index];
	}

	// Begin iterator

	template<typename T>
	typename VectorSet<T>::Iterator VectorSet<T>::begin(void) const
	{
		return Iterator(const_cast<T*> (&set[0]));
	}

	// Begin iterator

	template<typename T>
	typename VectorSet<T>::Iterator VectorSet<T>::end(void) const
	{
		size_t size = set.size();
		return Iterator(const_cast<T*> (&set[size]));
	}

	// Constant Begin iterator

	template<typename T>
	typename VectorSet<T>::ConstIterator VectorSet<T>::cbegin(void) const
	{
		return ConstIterator(const_cast<T*> (&set[0]));
	}

	// Constant End iterator

	template<typename T>
	typename VectorSet<T>::ConstIterator VectorSet<T>::cend(void) const
	{
		size_t size = set.size();
		return ConstIterator(const_cast<T*> (&set[size]));
	}

	// End of class VectorSet

	// Stream insertion operator for VectorSet

	template <typename T>
	std::ostream& operator <<(std::ostream& output, const VectorSet<T>& set)
	{
		size_t size = set.numberOfElements();
		size_t i = 0;

		output << "{";
		for (i = 0; i < size; ++i) {
			output << set[i];
			if (i != (size - 1))
				output << ", ";
		}

		output << " }";

		return output;
	}

} // End Of namespace Tasova

