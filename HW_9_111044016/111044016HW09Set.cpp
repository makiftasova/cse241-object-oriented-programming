/* 
 * File:   111044016HW09Set.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 12, 2012, 4:27 PM
 */

#include <algorithm>

#include "111044016HW09Set.h"

namespace Tasova {

	template <typename T>
	const typename Set<T>::Iterator Set<T>::search(const T& element) const
	{
		Iterator pos;
		pos = find(this->begin(), this->end(), element);
		return pos;
	}

	// Append an element to set

	template <typename T>
		const Set<T>& Set<T>::operator +=(const Set<T>& other)
	{
		size_t otrSize = other.numberOfElements();
		size_t i = 0;
		for (i = 0; i < otrSize; ++i)
			this->addElement(other[i]);

		return *this;
	}

	// Get union of sets

	template <typename T>
	const Set<T>& Set<T>::operator +(const Set<T>& other)
	{
		Set<T> *tmp;
		tmp = this;
		*tmp += other;

		return *tmp;
	}

	// Get intersection of sets

	template <typename T>
	const Set<T>& Set<T>::operator %(const Set<T>& other)
	{

		size_t i = 0, j = 0;
		size_t mySize = this->numberOfElements();
		size_t otrSize = other.numberOfElements();

		Set<T> *tmp;
		tmp = this;
		for (i = 0; i < mySize; ++i)
			tmp->removeElement();

		for (i = 0; i < mySize; ++i) {
			for (j = 0; j < otrSize; ++j)
				if ((*this)[i] == other[j])
					tmp->addElement((*this)[i]);
		}

		return *tmp;
	}

	// Append an element to set

	template <typename T>
		const Set<T>& Set<T>::operator +=(const Set<T>* other)
	{
		size_t otrSize = other->numberOfElements();
		size_t i = 0;
		for (i = 0; i < otrSize; ++i)
			this->addElement((*other)[i]);

		return *this;
	}

	// Get union of sets

	template <typename T>
	const Set<T>& Set<T>::operator +(const Set<T>* other)
	{
		Set<T> *tmp;
		tmp = this;
		*tmp += *other;

		return *tmp;
	}

	// Get intersection of sets

	template <typename T>
	const Set<T>& Set<T>::operator %(const Set<T>* other)
	{

		size_t i = 0, j = 0;
		size_t mySize = this->numberOfElements();
		size_t otrSize = other->numberOfElements();

		Set<T> *tmp;
		tmp = this;
		for (i = 0; i < mySize; ++i)
			tmp->removeElement();

		for (i = 0; i < mySize; ++i) {
			for (j = 0; j < otrSize; ++j)
				if ((*this)[i] == (*other)[j])
					tmp->addElement((*this)[i]);
		}

		return *tmp;
	}

	// Stream insertion operator for Set<T> references

	template <typename T>
	std::ostream& operator <<(std::ostream& output, const Set<T>& set)
	{
		size_t size = set.numberOfElements();
		size_t i = 0;

		output << "{";
		for (i = 0; i < size; ++i) {
			output << set[i];
			if (i != (size - 1))
				output << ", ";
		}

		output << " }";

		return output;
	}


	// Stream insertion operator for Set<T> pointers

	template <typename T>
	std::ostream& operator <<(std::ostream& output, const Set<T>* set)
	{
		size_t size = set->numberOfElements();
		size_t i = 0;

		output << "{";
		for (i = 0; i < size; ++i) {
			output << set->operator [](i);
			if (i != (size - 1))
				output << ", ";

		}

		output << "}";

		return output;
	}

	template <typename T>
	typename Set<T>::Iterator find(typename Set<T>::Iterator begin,
		typename Set<T>::Iterator end,
		const T& element)
	{
		typename Set<T>::Iterator itr;
		for (itr = begin; itr != end; ++itr)
			if (*itr == element)
				break;

		return itr;
	}

} // End of namespace Tasova
