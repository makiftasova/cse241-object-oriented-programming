/* 
 * File:   111044016HW09Set.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 12, 2012, 4:27 PM
 */

#ifndef SET_H
#define	SET_H

#include <iostream>


namespace Tasova {

	namespace { // Utilities namespace

		// Type definitions
		typedef unsigned int size_t;

	} // End of utilities namespace


	// Template class set begins

	template <typename T>
	class Set {
	public:

		// Basic class of Set iterator

		template <typename U = T>
			class BasicIterator {
		public:

			typedef T* Pointer;

			BasicIterator() : _data(NULL)
			{
				//Intentionally left blank
			}

			BasicIterator(Pointer ptr) : _data(ptr)
			{
				//Intentionally left blank
			}

			BasicIterator(const BasicIterator& orig)
			: _data(orig._data)
			{
				//Intentionally left blank
			}

			// Dereference operators

			T& operator *()
			{
				return *(this->_data);
			}

			const T& operator *() const
			{
				return *(this->_data);
			}

			// Increment operators

			T* operator ++()
			{
				return ++_data;
			}

			T* operator ++(int postfix)
			{
				return _data++;
			}


			// Constant increment operators

			const T* operator ++() const
			{
				return ++_data;
			}

			const T* operator ++(int postfix) const
			{
				return _data++;
			}


			// Decrement operators

			T* operator --()
			{
				return --_data;
			}

			T* operator --(int postfix)
			{
				return _data--;
			}

			// Constant decrement operators

			const T* operator --() const
			{
				return --_data;
			}

			const T* operator --(int postfix) const
			{
				return _data--;
			}

			// Assignment operator

			template <typename S>
				BasicIterator&
				operator =(const BasicIterator<S> orig)
			{
				_data = &(*orig);
				return *this;

			}

			// Equality check operators

			bool operator ==(const BasicIterator<T> other)const
			{
				return(this->_data == other._data);
			}

			bool operator !=(const BasicIterator<T> other)const
			{
				return(this->_data != other._data);
			}

			// prints memory address to given output stream

			std::ostream& printAddressTo(std::ostream& out) const
			{
				out << (int*) _data;
				return out;
			}

		private:
			Pointer _data;

		}; // End of class BasicIterator

		typedef BasicIterator<T> Iterator;
		typedef BasicIterator<const T> ConstIterator;

		// ** Abstract functions **
		// Adds element to set if not present
		virtual void addElement(const T& element) = 0;

		// Removes lastest element from set
		virtual const T removeElement(void) = 0;

		// Removes given element from set
		virtual const T removeElement(const T& element) = 0;

		// Search given element in set
		virtual const Iterator search(const T& element) const;

		// Return total number of elements in set
		virtual const size_t numberOfElements(void) const = 0;

		// Get the element at index
		virtual const T operator [] (const size_t index) const = 0;
		
		// Get the element at index (non-const)
		virtual T operator [] (const size_t index) = 0;

		// Append an element to set
		const Set<T>& operator +=(const Set<T>& other);

		// Get union of sets
		const Set<T>& operator +(const Set<T>& other);

		// Get intersection of sets
		const Set<T>& operator %(const Set<T>& other);
		
		// Append an element to set
		const Set<T>& operator +=(const Set<T>* other);

		// Get union of sets
		const Set<T>& operator +(const Set<T>* other);

		// Get intersection of sets
		const Set<T>& operator %(const Set<T>* other);

		// Begin iterator
		virtual Iterator begin(void) const = 0;
		
		// End iterator
		virtual Iterator end(void) const = 0;

		// Constant Begin iterator
		virtual ConstIterator cbegin(void) const = 0;
		
		// Constant end iterator
		virtual ConstIterator cend(void) const = 0;


	private:

	}; // End of abstract class Set

	// Stream insertion operator for Set<T> references
	template <typename T>
	std::ostream& operator <<(std::ostream& output, const Set<T>& set);

	// Stream insertion operator for Set<T> pointers
	template <typename T>
	std::ostream& operator <<(std::ostream& output, const Set<T>* set);

	// Find function for finding an element in a Set
	//	return element's iterator if element found
	//	else return end iterator
	template <typename T>
	typename Set<T>::Iterator find(typename Set<T>::Iterator begin,
		typename Set<T>::Iterator end,
		const T& element);

} // End of namespace Tasova

#endif	/* SET_H */

