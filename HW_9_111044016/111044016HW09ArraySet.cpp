/* 
 * File:   111044016HW09ArraySet.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on December 17, 2012, 11:18 PM
 */

#include <stdbool.h>
#include <stdexcept>

#include "111044016HW09ArraySet.h"




namespace Tasova {

	template <typename T>
	ArraySet<T>::ArraySet() : capacity(1), set(new T[1]), used(0)
	{
		//Intentionally left blank
	}

	template <typename T>
	ArraySet<T>::ArraySet(size_t size)
	: capacity(size), set(new T[size]), used(0)
	{
		//Intentionally left blank
	}

	template <typename T>
	ArraySet<T>::ArraySet(const ArraySet<T>& orig)
	: capacity(orig.capacity), used(orig.used), set(new T[capacity])
	{
		size_t i = 0;
		for (i = 0; i < used; ++i) {
			set[i] = orig[i];
		}

	}

	template <typename T>
	ArraySet<T>::~ArraySet()
	{
		delete [] set;
	}

	// Adds element to set if not present

	template <typename T>
	void ArraySet<T>::addElement(const T& element)
	{
		bool isGrown = false;
		size_t oldcap = capacity;


		if (0 == capacity) {
			capacity = 1;
			oldcap = capacity;
		}


		while (capacity <= used) {
			capacity *= 2;
			isGrown = true;
		}

		if (isGrown) {
			set = enlarge(set, oldcap, capacity, used);
		}

		size_t size = numberOfElements();
		size_t i = 0;
		bool add = false;
		for (i = 0; i <= size; ++i) {
			if (this->end() == this->search(element)) {
				add = true;
			}
		}

		if (add) {
			set[used] = element;
			++used;
		}
	}

	// Removes latest element from set

	template <typename T>
	const T ArraySet<T>::removeElement(void)
	{
		T removedOne;

		removedOne = set[used - 1];
		--used;

		// a little more process to free some unused memory
		if (used <= (capacity / 2)) {
			size_t oldcap = capacity;
			while (used <= (capacity / 2))
				capacity /= 2;

			set = enlarge(set, oldcap, capacity, used);
		}

		return removedOne;
	}

	// Removes given element from set, if given element can't found in set
	//	removes last element from set

	template <typename T>
	const T ArraySet<T>::removeElement(const T& element)
	{
		T removedOne;
		size_t size = numberOfElements();
		size_t i = 0;

		for (i = 0; i < size; ++i) {
			if (element == set[i])
				break;
		}

		if (i == size)
			removedOne = this->removeElement();
		else if (i < size) {
			removedOne = set[i];
			size_t j = 0;
			for (j = i; j < (size - i - 1); ++j)
				set[j] = set[j + 1];

			--used;
		}

		// a little more process to free some unused memory
		if (used <= (capacity / 2)) {
			size_t oldcap = capacity;
			while (used <= (capacity / 2))
				capacity /= 2;

			set = enlarge(set, oldcap, capacity, used);
		}

		return removedOne;
	}

	// Search given element in set

	template <typename T>
	const typename ArraySet<T>::Iterator
	ArraySet<T>::search(const T& element) const
	{
		size_t size = numberOfElements();
		size_t i = 0;
		bool found = false;

		Iterator itr = ArraySet<T>::end();

		for (i = 0; i < size; ++i)
			if ((*this)[i] == element) {
				itr = Iterator(set + i);
				break;
			}

		return itr;
	}

	// Return total number of elements in set

	template <typename T>
	const size_t ArraySet<T>::numberOfElements(void) const
	{
		return used;
	}

	// Return how many elements can store in container
	//	without any capacity changes

	template <typename T>
	const size_t ArraySet<T>::getCapacity(void) const
	{
		return capacity;
	}

	// Assignment Operator

	template <typename T>
		const ArraySet<T>&
		ArraySet<T>::operator =(const ArraySet<T>& other)
	{
		size_t i = 0;
		if (this != &other) {
			capacity = other.capacity;

			used = other.used;

			delete [] set;
			set = new T[capacity];


			for (i = 0; i < used; ++i) {
				set[i] = other[i];
			}
		}

		return *this;
	}

	// Append another set

	template <typename T>
		const ArraySet<T>&
		ArraySet<T>::operator+=(const ArraySet<T>& other)
	{
		size_t otherLen = other.numberOfElements();
		size_t i = 0;

		for (i = 0; i < otherLen; ++i)
			this->addElement(other[i]);

		return *this;

	}

	// Get union of sets

	template <typename T>
	const ArraySet<T> ArraySet<T>::operator+(const ArraySet<T>& other)
	{
		ArraySet<T> unionOfSets;
		unionOfSets = *this;
		unionOfSets += other;

		return unionOfSets;
	}

	// Get intersection of sets

	template <typename T>
	const ArraySet<T> ArraySet<T>::operator%(const ArraySet<T>& other)
	{
		ArraySet<T> intersection;

		size_t size = numberOfElements();
		size_t otherSize = other.numberOfElements();
		size_t i = 0, j = 0;
		for (i = 0; i < size; ++i) {
			for (j = 0; j < otherSize; ++j) {
				if ((*this)[i] == other[j])
					intersection.addElement((*this)[i]);
			}
		}

		return intersection;
	}

	// Get the element at index

	template <typename T>
	const T ArraySet<T>::operator [] (const size_t index) const
	{
		if (index < used)
			return set[index];
		// if index is out of range throw exception
		throw std::out_of_range(
			"ERROR: ArraySet<T>::operator [] index out of range\n");
	}

	// Get the element at index (non-const)

	template <typename T>
	T ArraySet<T>::operator [] (const size_t index)
	{
		if (index < used)
			return set[index];
		// if index is out of range throw exception
		throw std::out_of_range(
			"ERROR: ArraySet<T>::operator [] index out of range\n");
	}

	// Begin iterator

	template <typename T>
	typename ArraySet<T>::Iterator ArraySet<T>::begin(void) const
	{
		return Iterator(set);
	}

	// End iterator

	template <typename T>
	typename ArraySet<T>::Iterator ArraySet<T>::end(void) const
	{
		return Iterator(set + used);
	}

	template <typename T>
	typename ArraySet<T>::ConstIterator ArraySet<T>::cbegin(void) const
	{
		return ConstIterator(set);
	}

	// End iterator

	template <typename T>
	typename ArraySet<T>::ConstIterator ArraySet<T>::cend(void) const
	{
		return ConstIterator(set + used);
	}


	// ** Private Functions **
	// Changes maximum capacity of array without changing its ingredients

	template <typename T>
	T* ArraySet<T>::enlarge(T* set, size_t oldcap,
		size_t newcap, size_t inuse)
	{
		T* tmp;
		size_t i = 0;

		tmp = new T[oldcap];
		for (i = 0; i < inuse; ++i)
			tmp[i] = set[i];

		delete [] set;
		set = new T[capacity];

		for (i = 0; i < inuse; ++i)
			set[i] = tmp[i];

		delete [] tmp;

		return set;

	}

	// End of class ArraySet

	// Stream insertion operator for ArraySet

	template <typename T>
	std::ostream& operator <<(std::ostream& output, const ArraySet<T>& set)
	{
		size_t size = set.numberOfElements();
		size_t i = 0;

		output << "{";
		for (i = 0; i < size; ++i) {
			output << set[i];
			if (i != (size - 1))
				output << ", ";
		}

		output << " }";

		return output;
	}

} // End Of namespace Tasova
