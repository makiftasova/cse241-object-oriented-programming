/* 
 * File:   111044016HW05Series.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 2, 2012, 1:16 PM
 */

#include<iostream>
#include<vector>

#ifndef SERIES_H
#define	SERIES_H

class Series {
public:
	// Constructors
	Series(void);
	Series(const int user_k);
	Series(const int user_k, const double user_mult);
	Series(const int user_num, const int user_denom);
	Series(const int user_num, const int user_denom, const int user_k);

	// Manipulation Functions
	int add(void);
	int remove(void);
	const Series mult(const double user_multiplier);

	double evaluate(void) const;

	bool isLower(const Series& other) const;
	bool isEmpty(void) const;

	// Getter Functions
	int getK(void) const;
	double getAnElement(const int index) const;

	// Print Functions
	void print(void) const;
	void printAnElement(const int element_number) const;
	void printElements(void) const; // prints all elements of series

	// Static Functions
	static int setTotalObjectsNumber(const int number = 0);
	static int getTotalObjectNumber(void);
	static void printTotalObjectNumber(void);

	// Operators
	const Series operator *(const double multiplier) const;
	const Series operator +(const Series other) const;
	const Series operator -(const Series other) const;
	const double operator *(void) const; // evaluate operator (unary *)
	const Series operator ++(void); // postfix ++ oeprator
	const Series operator ++(int ignore_me); // prefix ++ oeprator
	const Series operator --(void); // postfix ++ oeprator
	const Series operator --(int ignore_me); // prefix ++ oeprator
	const double& operator [] (const unsigned int index) const;
	double& operator [] (const unsigned int index);
	const bool operator == (const Series& other) const;
	const bool operator != (const Series& other) const;
	const bool operator < (const Series& other) const;
	const bool operator > (const Series& other) const;
	const bool operator <= (const Series& other) const;
	const bool operator >= (const Series& other) const;

	// Friend Operator Functions
	friend std::ostream& operator <<(std::ostream& output_stream,
		const Series& series);
	friend std::istream& operator >>(std::istream& input_stream,
		Series& series);
	friend const Series operator *(const double user_multiplier,
		const Series& series);

private:
	int k; // Total number of elements
	int num; // Numerator of base element
	int denom; // Denominator of base element
	double multiplier;
	std::vector<double> elements; // vector array of elements

	// **Private Operators which will be useess for outer world
	const Series& operator -(void); //unary operator -

	// **Private Functions**
	bool constructorTest(void);

	// power functions
	double pow(const double base, const int power) const;
	int pow(const int base, const int power) const;

	// absolute value function
	int abs(const int number) const;

	int gcd(const int num1, const int num2) const;

	//Static Counters and Related Functions
	static int total_object_created;
	static int objectCreated(void);

	// Static Constant Defaults for Class
	static const int DEF_BASE_NUM = 1;
	static const int DEF_BASE_DENOM = 2;
	static const double DEF_MULTIPLIER = 1.0;

	// Static const foe exit codes
	static const int SERIES_EXIT_CONSTRUCTOR_ERR = 255;

};

#endif	/* SERIES_H */

