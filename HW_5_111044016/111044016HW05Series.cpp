/* 
 * File:   111044016HW05Series.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on November 2, 2012, 1:16 PM
 */

#include "111044016HW05Series.h"

#include<iostream>

// uses vectors for storing series elements
#include<vector>

// For exit() function
#include<cstdlib>

Series::Series(void) : num(DEF_BASE_NUM), denom(DEF_BASE_DENOM),
k(0), multiplier(0.0)
{
	elements.push_back((static_cast<double> (num) /
		static_cast<double> (denom)) * multiplier);
	++k;

	objectCreated();
}

Series::Series(const int user_k)
: num(DEF_BASE_NUM), denom(DEF_BASE_DENOM), k(user_k),
multiplier(DEF_MULTIPLIER)
{
	bool done = false;
	int i;

	done = constructorTest();

	if (k == 0) {
		elements.push_back(0.0);
	} else if (k > 0) {
		for (i = 0; i < k; ++i) {

			elements.push_back((static_cast<double> (num) /
				pow(denom, i)) * multiplier);
		}
	} else {
		elements.push_back((static_cast<double> (num) /
			pow(denom, k)) * multiplier);
	}

	objectCreated();

}

Series::Series(const int user_k, const double user_mult)
: num(DEF_BASE_NUM), denom(DEF_BASE_DENOM), k(user_k),
multiplier(user_mult)
{
	bool done = false;
	int i;

	done = constructorTest();


	if (k == 0) {
		elements.push_back(0.0);
	} else if (k > 0) {
		for (i = 0; i < k; ++i) {

			elements.push_back((static_cast<double> (num) /
				pow(denom, i)) * multiplier);
		}
	} else {
		elements.push_back((static_cast<double> (num) /
			pow(denom, k)) * multiplier);
	}

	objectCreated();
}

Series::Series(const int user_num, const int user_denom)
: num(user_num), denom(user_denom), k(0),
multiplier(DEF_MULTIPLIER)
{
	if (denom == 0) {
		std::cout << "FATAL ERROR: UNEXPECTED ERROR WHEN "
			<< "CONSTRUCTOR CALL\n"
			<< "Constructor Series::Series(const int, const int,"
			<< " const int) \nhas done something wrong.\n"
			<< "ERROR: Denominator of an element can't be 0\n"
			<< "Program Will Shut Down...\n";

		--total_object_created;

		exit(SERIES_EXIT_CONSTRUCTOR_ERR);
	}

	bool done = false;
	unsigned int i;

	done = constructorTest();

	elements.push_back((static_cast<double> (num) /
		pow(denom, k)) * multiplier);

	++k;

	objectCreated();
}

Series::Series(const int user_num, const int user_denom, const int user_k)
: num(user_num), denom(user_denom), k(user_k),
multiplier(DEF_MULTIPLIER)
{
	if (denom == 0) {
		std::cout << "FATAL ERROR: UNEXPECTED ERROR WHEN "
			<< "CONSTRUCTOR CALL\n"
			<< "Constructor Series::Series(const int, const int,"
			<< " const int) \nhas done something wrong.\n"
			<< "ERROR: Denominator of an element can't be 0\n"
			<< "Program Will Shut Down...\n";

		--total_object_created;

		exit(SERIES_EXIT_CONSTRUCTOR_ERR);
	}

	bool done = false;
	int i;

	done = constructorTest();

	if (k == 0) {
		elements.push_back(0.0);
	} else if (k > 0) {
		for (i = 0; i < k; ++i) {

			elements.push_back((static_cast<double> (num) /
				pow(denom, i)) * multiplier);
		}
	} else {
		elements.push_back((static_cast<double> (num) /
			pow(denom, k)) * multiplier);
	}
	objectCreated();
}

int Series::add(void)
{
	elements.push_back((static_cast<double> (num) /
		pow(denom, k)) * multiplier);

	++k;

	// returns lastest element's index
	return(elements.size() - 1);

}

int Series::remove(void)
{
	elements.pop_back();
	--k;

	// returns lastest element's index
	return( elements.size() - 1);
}

const Series Series::mult(const double user_multiplier)
{
	unsigned int len = elements.size();
	unsigned int i = 0;

	for (i = 0; i < len; ++i)
		elements[i] *= user_multiplier;

	return *this;

}

double Series::evaluate(void) const
{
	unsigned int length = elements.size();
	double sum = 0.0, sum_stack = 0.0;
	unsigned int i = 0;

	for (i = 0; i < length; ++i)
		sum += elements[i];

	return sum;
}

bool Series::isLower(const Series& other) const
{
	double my_sum = evaluate();
	double other_sum = other.evaluate();

	return(my_sum < other_sum);
}

bool Series::isEmpty(void) const
{
	return((k == 0) && (elements.size() == 0));
}

int Series::getK() const
{
	return k;
	//return (elements[0].size());
}

double Series::getAnElement(const int index) const
{
	return elements[index];
}

void Series::print(void) const
{
	unsigned int lenght = elements.size();
	unsigned int i = 0;

	for (i = 0; i < lenght; ++i) {
		std::cout << elements[i];
		if (i != (lenght - 1))
			std::cout << " + ";
	}


	std::cout << std::endl;


}

void Series::printAnElement(const int element_number) const
{
	if (element_number >= elements.size()) {
		std::cout << "ERROR:In Function printAnElement Given "
			<< "index value is out of range\n";
		return;
	}

	std::cout << elements[element_number] << std::endl;
}

void Series::printElements(void) const
{
	unsigned int lenght = elements.size();
	unsigned int i = 0;

	for (i = 0; i < lenght; ++i)
		std::cout << elements[i] << std::endl;

}


// Private Functions

bool Series::constructorTest(void)
{
	if (k < 0) {
		std::cout << "Warning: k must be >= 0\nvalue 0 automaticly "
			<< "assigned to k\n";
		k = 0;
	}
}

double Series::pow(const double base, const int power) const
{
	int i = 0;
	double result = 1.0;

	for (i = 0; i < power; ++i) {
		result *= base;
	}

	return result;
}

int Series::pow(const int base, const int power) const
{
	int result = 1, i = 0;

	for (i = 0; i < power; ++i) {
		result *= base;
	}

	return result;
}

int Series::abs(const int number) const
{
	int return_value = 0;
	if (number < 0)
		return_value = (-number);
	else
		return_value = number;

	return return_value;
}

int Series::gcd(const int num1, const int num2) const
{
	if (0 == num2) {

		return num1;
	}

	gcd(num2, num1 % num2);
}

// Static Functions and variables
int Series::total_object_created = 0; // initilize static member

int Series::objectCreated(void)
{
	total_object_created++;

	return total_object_created;
}

int Series::setTotalObjectsNumber(const int number)
{
	total_object_created = number;
	return total_object_created;
}

int Series::getTotalObjectNumber(void)
{
	return total_object_created;
}

void Series::printTotalObjectNumber(void)
{
	std::cout << "number of Series type objects = " << total_object_created
		<< std::endl;
}

// Member Operator Definitions

const Series Series::operator *(double user_multiplier) const
{
	Series temp;

	temp.k = k;

	unsigned int i = 0;

	for (i = 0; i < k; ++i)
		temp.elements.push_back(elements[i] * user_multiplier);

	return temp;

}

const Series Series::operator +(const Series other) const
{
	Series temp;
	unsigned int len1 = getK(), len2 = other.getK();
	unsigned int i = 0, j = 0;

	if (len1 > len2) {
		for (i = 0; i < len1; ++i)
			temp.elements.push_back(elements[i]);

		for (i = 0; i < len2; ++i)
			temp.elements[i] += other.elements[i];


	} else {
		for (i = 0; i < len2; ++i)
			temp.elements.push_back(other.elements[i]);

		for (i = 0; i < len1; ++i)
			temp.elements[i] += elements[i];
	}

	unsigned int new_k = 0;
	new_k = ((getK() < other.getK()) ? other.getK() : getK());

	temp.k = new_k;

	return temp;
}

const Series Series::operator -(const Series other) const
{
	Series temp;

	temp.k = other.getK();

	unsigned int i = 0, len = other.elements.size();

	for (i = 0; i < len; ++i)
		temp.elements.push_back(other.elements[i]);

	return(*this +(-temp));
}

const Series& Series::operator -(void) // Unary - operator ** Private **
{
	unsigned int i = 0;
	unsigned int len = getK();
	for (i = 0; i < len; ++i)
		elements[i] *= (-1);

	return *this;
}

const double Series::operator *(void) const // evaluate operator (unary *)
{
	return evaluate();
}

const Series Series::operator ++(void) // postfix
{
	unsigned int len = getK();
	unsigned int i = 0;

	for (i = 0; i < len; ++i)
		elements[i] += 1.0;

	return *this;
}

const Series Series::operator ++(int ignore_me) // prefix 
{
	Series temp;
	temp = *this;

	unsigned int len = getK();
	unsigned int i = 0;

	for (i = 0; i < len; ++i)
		elements[i] += 1.0;

	return temp;
}

const Series Series::operator --(void) // postfix
{
	unsigned int len = getK();
	unsigned int i = 0;

	for (i = 0; i < len; ++i)
		elements[i] -= 1.0;

	return *this;
}

const Series Series::operator --(int ignore_me) // prefix 
{
	Series temp;

	temp = *this;

	unsigned int len = getK();
	unsigned int i = 0;

	for (i = 0; i < len; ++i)
		elements[i] -= 1.0;

	return temp;
}

const double& Series::operator [] (const unsigned int index) const
{
	return elements[index];
}

double& Series::operator [] (const unsigned int index)
{
	return elements[index];
}

const bool Series::operator ==(const Series& other) const
{
	bool cmp_k = false, cmp_ele = true;

	if (this == &other)
		return true;

	if (cmp_k = (k == other.k)) {
		unsigned int i = 0;
		for (i = 0; i < k; ++i) {
			cmp_ele = (cmp_ele &&
				(elements[i] == other.elements[i]));
		}
	}

	return(cmp_k && cmp_ele);
}

const bool Series::operator !=(const Series& other) const
{
	return (!(*this == other));
}

const bool Series::operator<(const Series& other) const
{
	return(evaluate() < other.evaluate());
}

const bool Series::operator>(const Series& other) const
{
	return(evaluate() > other.evaluate());
}

const bool Series::operator <=(const Series& other) const
{
	return((*this < other) || (*this == other));
}

const bool Series::operator >=(const Series& other) const
{
	return((*this > other) || (*this == other));
}

// Friend Operator Definitions

std::ostream& operator <<(std::ostream& output_stream, const Series& series)
{
	unsigned int len = series.getK();
	unsigned int i = 0;

	if (len <= 1) {
		output_stream << " 0 ";
		return output_stream;
	}

	for (i = 0; i < len; ++i) {

		if (series.elements[i] == 0)
			continue;

		output_stream << series.elements[i];

		if (i != (len - 1))
			output_stream << " + ";

	}

	return output_stream;
}

std::istream& operator >>(std::istream& input_stream, Series& series)
{
	double temp_new_ele = 0.0;

	input_stream >> temp_new_ele;

	series.k += 1;
	series.elements.push_back(temp_new_ele);

	return input_stream;
}

const Series operator *(const double user_multiplier, const Series& series)
{
	Series temp;

	temp.k = series.k;

	unsigned int i = 0;
	unsigned int len = temp.k;

	for (i = 0; i < len; ++i)
		temp.elements.push_back(user_multiplier * series.elements[i]);

	return temp;
}