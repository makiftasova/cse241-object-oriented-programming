/* 
 * File:   111044016_main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on November 2, 2012, 1:15 PM
 */

#include <iostream>
#include "111044016HW05Series.h"


// for generating pseudorandom numbers
#include <ctime> 
#include <cstdlib>

using namespace std;


// *** Test Utilities *** //

// generates a pseudorandom integer in given range
// uses ctime for pseudorandom numbers
const int random(const int min, const int max); 

/*
 * 
 */
int main(int argc, char** argv)
{
	const int K1 = 8;
	const int K2 = 7; 
	const int K3 = 6; 
	const int K4 = 5; 
	const int K5 = 4;
	const int RAND_START = 10;
	const int RAND_END = 20;
	
	Series a(K1), b(K2), c(K3), d(K4), e(K5);
	
	cout << "Series initialized successfuly\n Series are:\n";
	cout << "a: " << a << endl;
	cout << "b: " << b << endl;
	cout << "c: " << c << endl;
	cout << "d: " << d << endl;
	cout << "e: " << e << endl;
	
	cout << "-------------\n";
	
	cout << "\nNow Starting Test\n\n";
	
	cout << "-------------\n";
	
	cout << "a + b == " << a+b << endl;
	cout << "c - e == " << c-e << endl;
	
	cout << "-------------\n";
	
	int tmp_mul = random(RAND_START, RAND_END);
	cout << "Pseudorandom test multiplier is " << tmp_mul << endl;
	cout << tmp_mul << " * d == " << (tmp_mul*d) << endl;
	cout << "d * " <<tmp_mul << " == " << (d*tmp_mul) << endl;
	
	cout << "-------------\n";
	
	cout << "Now Testing increment and decrement operators...\n";
	
	cout << "at start a == " << a << endl;
	
	cout << "\npostfixes:\n";
	cout << "a++ == " << a++ << endl;
	cout << "a-- == " << a-- << endl;
	
	cout << "afrter first part a == " << a << endl;

	cout << "\nprefixes:\n";
	cout << "++a == " << ++a << endl;
	cout << "--a == " << --a << endl;
	
	cout << "at end of part a == " << a << endl;
	
	cout << "-------------\n";
	
	cout << "evaluate(b) == " << b.evaluate() << endl;
	cout << "*b == " << *b << endl;
	
	cout << "-------------\n";
	
	double temp = e[2];
	cout << "e[2] == " << e[2] << endl;
	cout << "e[2] is changing to 5...\n";
	e[2] = 5;
	cout << "new e[2] is " << e[2] << endl;
	
	cout << "now reseting e[2]...\n";
	e[2] = temp;
	cout << "e: " << e << endl;
	
	cout << "-------------\n";
	
	cout << "logic operators test now beginnig...\n";
	cout << "a: " << a << endl << "b: " << b << endl;
	cout << "a == b --> " <<  boolalpha << (a == b) << endl<< endl;
	
	cout << "d: " << d << endl << "c: " << c << endl;
	cout << "d != c --> " << boolalpha << (d != c) << endl<< endl;
	
	cout << "e: " << e << endl << "a: " << a << endl;
	cout << "e >= a --> " << boolalpha << (e >= a) << endl<< endl;
	
	cout << "b: " << b << endl << "d: " << d << endl;
	cout << "b <= d --> " << boolalpha << (b <= d) << endl << endl;
	
	cout << "c: " << c << endl << "e: " << e << endl;
	cout << "c > e --> " << boolalpha << (c > e) << endl<< endl;
	
	cout << "b: " << b << endl << "c: " << c << endl;
	cout << "b < c --> " << boolalpha << (b < c) << endl<< endl;
	
	cout << "-------------\n";
	
	cout << Series::getTotalObjectNumber() 
	     << " Series object created when doing all thoose tests...\n";
	
	
	return 0;
}

const int random(const int min, const int max)
{
	int diff = (max - min);
	int random = (rand() % diff);
	
	return (random + min);
}