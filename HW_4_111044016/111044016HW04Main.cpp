/* 
 * File:   111044016_main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on November 2, 2012, 1:15 PM
 */

#include <iostream>
#include "111044016HW04Series.h"

using namespace std;



// Some Tester Function Prototypes

void callByValueTest(Series the_series);
void callByRefTest(Series & the_series_ref);
void callBySimRefTest(Series* the_series_ptr);

/*
 * 
 */
int main(int argc, char** argv)
{

	char choice = 0;

	cout << "[1] Do Class Test\n"
		<< "[2] Do Fuction Arguments Test\n"
		<< "[3] I've more important things to do, "
		<< "so let me go (a.k.a. Exit)\n"
		<< "Enter Your Choice =>";
	cin >> choice;

	switch (choice) {
	case '1':
	{
		// ** Const Series Test **
		cout << "\n\nGenerating a const Series object named "
			<< "a and printing it to screen...\n\n"
			<< "I need the value of  k for this test =>";
		int k = 0;
		cin >> k;
		if (k > 32) {
			cout << "Its going to be a \"Zero Division Error\"\n"
				<< "But i will try it...\n";
		} else if (k < 0) {
			cout << "You entered a non-positive integer!\n"
				<< "8 is now assigning to variable k...\n\n";
			k = 8;
		}

		const Series a(k);

		cout << "Elements of a\n";
		a.print();

		cout << "\na.evaluate() == " << a.evaluate() << endl;

		// ** Series Pointer Test **
		cout << "\n\nNow Testing Series poniter...\n";
		Series b(5);
		Series *b_ptr;

		b_ptr = &b;
		cout << "Printing with \"(*b_ptr).print();\"\n";
		(*b_ptr).print();

		cout << "Adding some elements to b series...\n";

		b_ptr->add();
		b_ptr->add();
		b_ptr->add();

		cout << "I've added 3 more elements to series b.\n";

		cout << "Printing with \"b_ptr->print();\"\n";
		b_ptr->print();

		cout << "\nb.evaluate() == " << b_ptr->evaluate() << endl;

		// ** Now its for the testing arrays... **
		cout << "\n\nWeee, its time for arrays...\n";

		Series array[3] = {(0), (3), (8)};

		cout << "Printing 1st element of array\n";
		array[0].print();
		cout << "array[0].evaluate == " << array[0].evaluate() << endl;
		cout << "Let's check is array[0] is empty...\n";
		if (array[0].isEmpty())
			cout << "Yes it is empty\n";
		else
			cout << "No it is not empty\n";


		cout << "\nMultiply array to with 3 and print it\n";
		array[1].mult(3);
		array[1].print();
		cout << "array[1].evaluate == " << array[0].evaluate() << endl;


		cout << "Now its time for last element of array...\n";
		cout << "This is array[2] ==> ";
		array[2].print();
		cout << "Now remove some of its elements...\n";
		array[2].remove();
		array[2].remove();
		array[2].remove();
		array[2].remove();
		cout << "and here is the our whole new array[2] ==> ";
		array[2].print();

		cout << "And here comes to total number of created objects...\n";
		Series::printTotalObjectNumber();

		cout << "By The way, a little test for isLower Method...\n";

		cout << "\nIs series a is lower than series b?\n"
			<< "The Answer is ";
		if (a.isLower(b))
			cout << "==>yes\n\n";
		else
			cout << "==>no\n\n";

		cout << "That's all folks...\n";
		break;



	}
	case '2':
	{
		cout << "\nGenerating a test series with 5 elements...\n";

		Series test_series(5);
		cout << endl;
		cout << "Now let's call \"callByValueTest(test_series);\"\n";
		callByValueTest(test_series);
		cout << endl << endl;

		cout << "Now let's call \"callByRefeTest(test_series);\"\n"
			<< "It looks going to hurt...\n";
		callByRefTest(test_series);
		cout << endl << endl;


		cout << "Now taking pointer of Series test_series...\n";
		Series *test_series_ptr;
		test_series_ptr = &test_series;

		cout << "Now let's call \"callBySimRefTest(test_series);\"\n";
		callBySimRefTest(test_series_ptr);

		cout << "Now I'll just write last version of series...\n";
		test_series.print();
		cout << "And also test_series.evaluate == "
			<< test_series.evaluate() << endl;


		cout << "\nUnfortunately That's all folks...\n";
		break;
	}
	case '3':
	{
		cout << "I hope you will find some time to look me after\n"
			<< "Goodbye, for now...\n";
		break;
	}
	default:
	{
		cout << "There is no such option like \"" << choice << "\"\n"
			<< "This mean you broke it and"
			<< " you need to start it again\n";
		break;
	}

	}

	cout << "\n\nAnd a little information annotation:\n"
		<< "Constructors called " << Series::getTotalObjectNumber()
		<< " times while we are doing this..." << endl;

	return 0;
}

// Some Tester Function Prototypes

void callByValueTest(Series the_series)
{
	cout << "\n**Function Name: callByValueTest**\n"
		<< "Vanilla version of series: ";
	the_series.print();

	cout << "It evaluates to value " << the_series.evaluate() << endl;

	cout << "Lets make some permanent changes and finish this part... \n";
	the_series.add();
	the_series.add();
	the_series.add();

	cout << "Last version of series befora exit\n";
	the_series.print();

	cout << "**Exiting Function callByValueTest**\n";
}

void callByRefTest(Series & the_series_ref)
{
	cout << "\n**Function Name: callByReferenceTest**\n";

	cout << "\nLets check our series at first...\n";
	the_series_ref.print();
	cout << "and it is also important...\n";
	cout << "evaluated value = " << the_series_ref.evaluate() << endl;

	cout << "\nNow remove some elements \n";
	the_series_ref.remove();
	the_series_ref.remove();

	cout << "Now it look like this ==> ";
	the_series_ref.print();

	cout << "\nNow add some... \n";
	the_series_ref.add();
	the_series_ref.add();
	the_series_ref.add();
	the_series_ref.add();
	the_series_ref.add();

	cout << "\nAnd the final cersion of our series is ";
	the_series_ref.print();

	cout << "and evaluated value = " << the_series_ref.evaluate() << endl;

	cout << "It look mot much pain here...\n";

	cout << "**Exiting Function callByReferenceTest**\n";
}

void callBySimRefTest(Series* the_series_ptr)
{
	cout << "\n**Function Name: callBySimRefTest**\n";

	cout << "Let's check our series at first\n";
	the_series_ptr->print();
	cout << "\nWait a minute, it is not my original series??\n"
		<< "Oh yes, same series called by reference before me\n"
		<< "Whatever, let's move on...\n";

	cout << "evaluated value = " << the_series_ptr->evaluate() << endl;

	cout << "Now making some operations...\n";
	the_series_ptr->mult(8);
	the_series_ptr->add();
	the_series_ptr->add();
	the_series_ptr->add();
	the_series_ptr->mult(0.2);

	cout << "Its time to show you, what I've done...";
	the_series_ptr->print();
	cout << "and evaluated value = " << the_series_ptr->evaluate() << endl;

	the_series_ptr->remove();
	the_series_ptr->remove();
	the_series_ptr->remove();
	the_series_ptr->remove();
	the_series_ptr->remove();

	cout << "**Exiting Function callBySimRefTest**\n";
}