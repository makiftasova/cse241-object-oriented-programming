/* 
 * File:   Series.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on November 2, 2012, 1:16 PM
 */

#include "111044016HW04Series.h"

#include<iostream>

// uses vectors for storing series elements
#include<vector>

// For exit() function
#include<cstdlib>

using namespace std;

Series::Series(void) : num(DEF_BASE_NUM), denom(DEF_BASE_DENOM),
k(0), multiplier(0.0)
{
	elements[0].push_back(num);
	elements[1].push_back(denom);
	multipliers.push_back(multiplier);
	objectCreated();
}

Series::Series(const int max_elements)
: num(DEF_BASE_NUM), denom(DEF_BASE_DENOM), k(max_elements),
multiplier(DEF_MULTIPLIER)
{
	bool done = false;
	int i;

	done = constructorTest();

	if (k == 0) {
		elements[0].push_back(num);
		elements[1].push_back(denom);
		multipliers.push_back(k);
	} else if (k > 0) {
		for (i = 0; i < k; ++i) {
			elements[0].push_back(num);
			elements[1].push_back(pow(denom, i));
			multipliers.push_back(multiplier);
		}
	} else {
		elements[0].push_back(num);
		elements[1].push_back(pow(denom, k));
		multipliers.push_back(multiplier);
	}

	objectCreated();

}

Series::Series(const int user_num, const int user_denom, const int max_elements)
: num(user_num), denom(user_denom), k(max_elements),
multiplier(DEF_MULTIPLIER)
{
	if (denom == 0) {
		cout << "FATAL ERROR: UNEXPECTED ERROR WHEN CONSTRUCTOR CALL\n"
			<< "Constructor Series::Series(const int, const int,"
			<< " const int) \nhas done something wrong.\n"
			<< "ERROR: Denominator of an element can't be 0\n"
			<< "Program Will Shut Down...\n";

		--total_object_created;

		exit(SERIES_EXIT_CONSTRUCTOR_ERR);
	}

	bool done = false;
	int i;

	done = constructorTest();


	if (k == 0) {
		elements[0].push_back(num);
		elements[1].push_back(denom);
		multipliers.push_back(k);
	} else if ((k > 0)) {
		for (i = 0; i < k; ++i) {
			elements[0].push_back(pow(num, i));
			elements[1].push_back(pow(denom, i));
			multipliers.push_back(multiplier);
		}
	} else {
		elements[0].push_back(pow(num, k));
		elements[1].push_back(pow(denom, k));
		multipliers.push_back(multiplier);
	}

	objectCreated();
}

int Series::add(void)
{
	elements[0].push_back(pow(num, k));
	elements[1].push_back(pow(denom, k));
	multipliers.push_back(multiplier);

	++k;

	// returns lastest element's index
	return(elements[0].size() - 1);

}

int Series::remove(void)
{
	elements[0].pop_back();
	elements[1].pop_back();
	multipliers.pop_back();
	--k;

	// returns lastest element's index
	return( elements[0].size() - 1);
}

void Series::mult(const double user_multiplier)
{
	multiplier = user_multiplier;

	int size = multipliers.size();
	int i = 0;

	for (i = 0; i < size; ++i)
		multipliers[i] *= user_multiplier;

}

double Series::evaluate(void) const
{
	int length = elements[0].size();
	double sum = 0.0, sum_stack = 0.0;
	int i = 0;

	for (i = 0; i < length; ++i) {
		sum_stack = (static_cast<double> (elements[0][i]) /
			static_cast<double> (elements[1][i]));

		sum += (multipliers[i] * sum_stack);
	}

	return sum;
}

bool Series::isLower(const Series& other) const
{
	double my_sum = evaluate();
	double other_sum = other.evaluate();

	return(my_sum < other_sum);
}

bool Series::isEmpty(void) const
{
	return(k == 0);
}

int Series::getK() const
{
	return k;
}

double Series::getAnElement(const int index) const
{
	double temp_eval = 0.0;

	temp_eval = (static_cast<double> (elements[0][index]) /
		static_cast<double> (elements[1][index]));

	return( temp_eval * multipliers[index]);
}

void Series::print(void) const
{
	int lenght = elements[0].size();
	int i = 0;

	for (i = 0; i < lenght; ++i) {
		printFraction(elements[0][i], elements[1][i], multipliers[i]);
		if (!((i + 1) == lenght))
			cout << " + ";
	}

	cout << endl;


}

void Series::printAnElement(const int element_number) const
{
	if (element_number >= elements[0].size()) {
		cout << "ERROR:In Function printAnElement Given "
			<< "index value is out of range\n";
		return;
	}

	printFraction(elements[0][element_number - 1],
		elements[1][element_number - 1],
		multipliers[element_number - 1]);
}

void Series::printElements(void) const
{
	int i = 0;

	for (i = 0; i < elements[0].size(); ++i) {
		printFraction(elements[0][i], elements[1][i], multipliers[i]);
		cout << endl;
	}
}


// Private Functions

bool Series::constructorTest(void)
{
	if (k < 0) {
		cout << "k must be >= 0\nvalue 0 automaticly assigned to k\n";
		k = 0;
	}
}

double Series::pow(const double base, const int power) const
{
	int i = 0;
	double result = 1.0;

	for (i = 0; i < power; ++i) {
		result *= base;
	}

	return result;
}

int Series::pow(const int base, const int power) const
{
	int result = 1, i = 0;

	for (i = 0; i < power; ++i) {
		result *= base;
	}

	return result;
}

int Series::abs(const int number) const
{
	int return_value = 0;
	if (number < 0)
		return_value = (-number);
	else
		return_value = number;

	return return_value;
}

int Series::gcd(const int num1, const int num2) const
{
	if (0 == num2) {

		return num1;
	}

	gcd(num2, num1 % num2);
}

void Series::printFraction(const int num, const int denom,
	const double multiplier_param) const
{
	if (denom == 0) {
		cout << "**Denominator can't be 0\n"
			<< "ERROR: Tried to print Zero Division Error in "
			<< "Function printFraction\n";
		return;
	}

	if (num == denom) {
		cout << multiplier_param;
		return;
	} else if ((num % denom) == 0) {
		cout << multiplier_param << "(" << num << ")";
		return;
	} else {
		cout << multiplier_param << "(" << num << " / " << denom << ")";
	}
}

// Static Funstions and variables
int Series::total_object_created = 0; // initilize static member

int Series::objectCreated(void)
{
	total_object_created++;

	return total_object_created;
}

int Series::setTotalObjectsNumber(const int number)
{
	total_object_created = number;
	return total_object_created;
}

int Series::getTotalObjectNumber(void)
{
	return total_object_created;
}

void Series::printTotalObjectNumber(void)
{
	cout << "number of Series type objects = " << total_object_created
		<< endl;
}