/* 
 * File:   Series.h
 * Author: makiftasova
 *
 * Created on November 2, 2012, 1:16 PM
 */

// uses vectors for storing sreies elements
#include<vector>
using namespace std;

#ifndef SERIES_H
#define	SERIES_H

class Series {
public:
	// Constructors
	Series(void);
	Series(const int max_elements);
	Series(const int user_num, const int user_denom, const int max_elements);

	// Manipulation Functions
	int add(void);
	int remove(void);
	void mult(const double user_multiplier);

	double evaluate(void) const;

	bool isLower(const Series& other) const;
	bool isEmpty(void) const;

	// Getter Functions
	int getK(void) const;
	double getAnElement(const int index) const;

	// Print Functions
	void print(void) const;
	void printAnElement(const int element_number) const;
	void printElements(void) const; // prints all elements of series

	// Static Functions
	static int setTotalObjectsNumber(const int number = 0);
	static int getTotalObjectNumber(void);
	static void printTotalObjectNumber(void);

private:
	int k; // Total number of elements
	int num; // Numerator of base element
	int denom; // Denominator of base element
	double multiplier;
	vector<int> elements[2]; // vector array of elements
	// element[0] stores numerators
	// element[1] stores denominators
	vector<double> multipliers; //vector array of multipliers

	// **Private Functions**
	bool constructorTest(void);

	// power functions
	double pow(const double base, const int power) const;
	int pow(const int base, const int power) const;

	// absolute value function
	int abs(const int number) const;

	int gcd(const int num1, const int num2) const;

	// Fraction print function
	void printFraction(const int num, const int denom,
		const double multiplier_param) const;

	//Static Counters and Related Functions
	static int total_object_created;
	static int objectCreated(void);

	// Static Constant Defaults for Class
	static const int DEF_BASE_NUM = 1;
	static const int DEF_BASE_DENOM = 2;
	static const double DEF_MULTIPLIER = 1.0;

	// Static const foe exit codes
	static const int SERIES_EXIT_CONSTRUCTOR_ERR = 255;

};

#endif	/* SERIES_H */

