/* 
 * File:   111044016HW07Main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 20, 2012, 4:11 PM
 */

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "111044016HW07Family.h"


using namespace std;
using namespace Tasova;

// ****Some useful constants
// **A unique person object to point end of family
const Person EMPTY_PERSON_TEST("JOHN", "DOE", male, 1000);
// **Name of input file
const char* FILE_NAME = "111044016HW07Families.bin";

// *** Thoose operators just defined because of practice ***
// fstream operators for binary files
//	Thoose are only for binary input/output
fstream& operator <<(fstream& output, const Person& person);
fstream& operator >>(fstream& input, Person& person);
fstream& operator <<(fstream& output, const Family& family);
fstream& operator >>(fstream& input, Family& family);

// Some other utilities
bool isRelative(const Person& p1, const Person& p2,
	Family** fams, int numOfFams);

/*
 * 
 */
int main(int argc, char** argv)
{


	// ****Family of Nicholas I
	// **Parents
	Person nicholas_1_emp("Nicholas I", "The Emperor", male, 1796);
	Person alexandra_quenn("Alexandra", "Feodorovna", female, 1798);
	// **Children
	Person elizabeth("Elizabeth", "Feodorovna", female, 1826);
	Person nicholas("Nicholas", "The Son", male, 1831);
	Person olga("Olga", "Feodorovna", female, 1822);
	Person michael("Michael", "The Son", male, 1832);
	Person alexander_2_emp("Alexander", "The Emperor", male, 1818);
	Person constantine("Constantine", "The Son", male, 1827);
	Person alexandra("Alexandra", "Feodorovna", female, 1825);
	Person maria("Maria", "Feodorovna", female, 1819);

	Family royal_court_of_nicholas_1(nicholas_1_emp, alexandra);

	royal_court_of_nicholas_1 += elizabeth;
	royal_court_of_nicholas_1 += nicholas;
	royal_court_of_nicholas_1 += olga;
	royal_court_of_nicholas_1 += michael;
	royal_court_of_nicholas_1 += alexander_2_emp;
	royal_court_of_nicholas_1 += constantine;
	royal_court_of_nicholas_1 += alexandra;
	royal_court_of_nicholas_1 += maria;

	// ****Family of Alexander II
	// ** Father of family is alexander_2_emp from  Nicholas I's family
	alexander_2_emp.setName("Alexander II");
	Person maria_2("Maria", "Alexandrovna", female, 1824);
	// **Children
	Person vladimir("Vladimir", "The Son", male, 1847);
	Person alexandra_2("Alexandra", "Alexandrovna", female, 1842);
	Person nicholas_2("Nicholas", "The Son", male, 1843);
	Person sergei("Sergei", "The Son", male, 1857);
	Person alexander_3_emp("Alexander", "The Emperor", male, 1845);
	Person alexei("Alexei", "The Son", male, 1850);
	Person paul("Paul", "The Son", male, 1860);
	Person maria_3("Maria", "Alexandrovna", female, 1853);

	Family royal_court_of_alexander_2(alexander_2_emp, maria_2);

	royal_court_of_alexander_2 += vladimir;
	royal_court_of_alexander_2 += alexandra_2;
	royal_court_of_alexander_2 += nicholas;
	royal_court_of_alexander_2 += sergei;
	royal_court_of_alexander_2 += alexander_3_emp;
	royal_court_of_alexander_2 += alexei;
	royal_court_of_alexander_2 += paul;
	royal_court_of_alexander_2 += maria_3;

	// ****Family of Alexander III
	// ** Father of family is alexander_3_emp from  Alexander II's family
	alexander_3_emp.setName("Alexander III");
	Person maria_4("Maria", "Feodorovna", female, 1847);
	// **Children
	Person alexander("Alexander", "The Son", male, 1869);
	Person george("George", "The Son", male, 1870);
	Person xenia("Xenia", "Feodorovna", female, 1875);
	Person nicholas_2_emp("Nicholas", "The Emperor", male, 1868);
	Person olga_2("Olga", "Feodorovna", female, 1882);
	Person michael_2("Michael", "The Son", male, 1878);

	Family royal_court_of_alexander_3(alexander_3_emp, maria_4);

	royal_court_of_alexander_3 += alexander;
	royal_court_of_alexander_3 += george;
	royal_court_of_alexander_3 += xenia;
	royal_court_of_alexander_3 += nicholas_2_emp;
	royal_court_of_alexander_3 += olga_2;
	royal_court_of_alexander_3 += michael_2;

	// ****Family of Nicholas II
	// ** Father of family is nicholas_2_emp from  Alexander III's family
	nicholas_2_emp.setName("Nicholas II");
	Person alexandra_3("Alexandra", "Fyodorovna", female, 1872);
	// **Children
	Person olga_3("Olga", "Fyodorovna", female, 1895);
	Person tatiana("Tatiana", "Fyodorovna", female, 1897);
	Person maria_5("Maria", "Fyodorovna", female, 1899);
	Person anastasia("Anastasia", "Fyodorovna", female, 1901);
	Person alexei_2("Alexei", "The Son", male, 1904);

	Family royal_court_of_nicholas_2(nicholas_2_emp, alexandra_3);

	royal_court_of_nicholas_2 += olga_3;
	royal_court_of_nicholas_2 += tatiana;
	royal_court_of_nicholas_2 += maria_5;
	royal_court_of_nicholas_2 += anastasia;
	royal_court_of_nicholas_2 += alexei_2;

	// lets write families to a binary file
	fstream output(FILE_NAME, ios::out | ios::binary);
	if (output.is_open()) {
		cout << "Writing data to \"" << FILE_NAME << "\"" << endl;

		royal_court_of_nicholas_1.writeToBinaryFile(output);
		EMPTY_PERSON.writeToBinaryFile(output);
		royal_court_of_alexander_2.writeToBinaryFile(output);
		EMPTY_PERSON.writeToBinaryFile(output);
		royal_court_of_alexander_3.writeToBinaryFile(output);
		EMPTY_PERSON.writeToBinaryFile(output);
		royal_court_of_nicholas_2.writeToBinaryFile(output);
		EMPTY_PERSON.writeToBinaryFile(output);
		output.close();
		cout << "Writing Done...\n";
	} else {
		cout << "Cannot create file \"" << FILE_NAME << "\"" << endl;
		cout << endl;
	}

	Family fam1, fam2, fam3, fam4;

	fstream input(FILE_NAME, ios::in | ios::binary);
	if (input.is_open()) {
		cout << "Reading data from \"" << FILE_NAME << "\"" << endl;
		cout << endl;

		fam1.readFromBinaryFile(input);
		fam2.readFromBinaryFile(input);
		fam3.readFromBinaryFile(input);
		fam4.readFromBinaryFile(input);
		input.close();
		cout << fam1 << endl;
		cout << fam2 << endl;
		cout << fam3 << endl;
		cout << fam4 << endl;
		cout << "Reading done...\n\n\n";
	} else {
		cout << "Cannot find file \"" << FILE_NAME << "\"" << endl;
		cout << endl;
	}

	return(EXIT_SUCCESS);
}

// *** Thoose operators just defined because of practice ***
// fstream operators for binary files
//	Thoose are only for binary input/output

// Let's teach writing a person to binary file to operator <<

fstream& operator <<(fstream& output, const Person& person)
{
	output.write(person.getName(), (PERSON_NAME_BUFF_LEN * sizeof(char)));
	output.write(person.getSurname(),
		(PERSON_SNAME_BUFF_LEN * sizeof(char)));

	// gender sign stores as a char in binary file
	char sex = ((male == person.getSex() ? CHAR_MALE : CHAR_FEMALE));
	output.write(&sex, sizeof(char));

	output << person.getBirthYear();

	// marital status sign stores as a char in binary file
	char mart_stat = ((married == person.getMaritalStatus())
		? CHAR_MARRIED : CHAR_SINGLE);
	output.write(&mart_stat, sizeof(char));


	output << person.getFamilyID();

	return output;
}

// Let's teach reading a person from binary file to operator >>

fstream& operator >>(fstream& input, Person& person)
{

	char *name(new char[PERSON_NAME_BUFF_LEN + 1]);
	input.read(name, PERSON_NAME_BUFF_LEN);
	person.setName(name);

	char *surname(new char[PERSON_SNAME_BUFF_LEN + 1]);
	input.read(surname, PERSON_SNAME_BUFF_LEN);
	person.setSurname(surname);

	// gender sign storen as a char in binary file
	char sex;
	input.read(&sex, sizeof(char));
	person.setSex(((sex == CHAR_MALE) ? male : female));

	year_t byear;
	input >> byear;
	person.setBirthYear(byear);

	// marital status sign storen as a char in binary file
	char mart_stat;
	input.read(&mart_stat, sizeof(char));
	marital_t m_status = ((mart_stat == CHAR_MARRIED) ? married : single);
	person.setMaritalStatus(m_status);

	unsigned int fam_id;
	input >> fam_id;
	person.setFamilyID(fam_id);


	return input;
}

fstream& operator <<(fstream& output, const Family& family)
{
	// operator << already knows how to write a person into binary file
	output << family.getFather();
	output << family.getMother();
	size_t i = 0, child_num = family.getChildrenNumber();
	for (i = 0; i < child_num; ++i)
		output << family.getChild(i);

	return output;
}

fstream& operator >>(fstream& input, Family& family)
{
	// operator >> already knows how to read a person from binary file

	Person father;
	input >> father;
	family.setFather(father);

	Person mother;
	input >> mother;
	family.setMother(mother);

	Person child;
	while (!(input.eof())) {
		input >> child;

		if (child == EMPTY_PERSON)
			break;

		family += child;
	}

	return input;
}


// Some other utilities

bool isRelative(const Person& p1, const Person& p2,
	Family** fams, int numOfFams)
{
	bool foundP1 = false, foundP2 = false;
	if (&p1 == &p2)
		return true; // if both person are the same then return true 

	size_t i = 0, k = 0, m = 0;

	for (i = 0; i < numOfFams; ++i) {

		if (p1 == fams[i]->getFather())
			foundP1 = true;
		else if (p1 == fams[i]->getMother())
			foundP1 = true;
		else if (!foundP1)
			for (k = 0; k < fams[i]->getChildrenNumber(); ++k)
				if (p1 == fams[i]->getChild(k)) {
					foundP1 = true;
					break;
				}

		if (foundP1) {
			for (k = 0; k < numOfFams; ++k) {
				if (p2 == fams[k]->getFather())
					foundP2 = true;
				else if (p2 == fams[k]->getMother())
					foundP2 = true;
				else if (!foundP2)
					for (m = 0; (m < fams[k]->getChildrenNumber()); ++m)
						if (p2 == fams[k]->getChild(m)) {
							foundP2 = true;
							break;
						}
			}
		} else {
			return foundP1;
		}


	}

	return(foundP1 && foundP2);

}