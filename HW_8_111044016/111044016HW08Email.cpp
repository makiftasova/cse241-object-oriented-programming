/* 
 * File:   111044016HW08Email.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on December 6, 2012, 10:41 PM
 */

#include "111044016HW08Email.h"

namespace Tasova {

	Email::Email()
	: Document(), sender(DEFAULT_SENDER),
	recipient(DEFAULT_RECIPIENT), title(DEFAULT_TITLE)
	{
		// Intentionally Left Blank
	}

	Email::Email(const std::string new_sender)
	: Document(DEFAULT_TEXT), sender(new_sender),
	recipient(DEFAULT_RECIPIENT), title(DEFAULT_TITLE)
	{
		// Intentionally Left Blank
	}

	Email::Email(const std::string new_sender, 
		const std::string new_recipient)
	: Document(DEFAULT_TEXT), sender(new_sender),
	recipient(new_recipient), title(DEFAULT_TITLE)
	{
		// Intentionally Left Blank
	}

	Email::Email(const std::string new_sender,
		const std::string new_recipient,
		const std::string new_title)
	: Document(DEFAULT_TEXT), sender(new_sender),
	recipient(new_recipient), title(new_title)
	{
		// Intentionally Left Blank
	}

	Email::Email(const std::string new_sender,
		const std::string new_recipient,
		const std::string new_title,
		const std::string new_text)
	: Document(new_text), sender(new_sender),
	recipient(new_recipient), title(new_title)
	{
		// Intentionally Left Blank
	}

	Email::Email(const Email& orig)
	: Document(orig.getText()), sender(orig.getSender()),
	recipient(orig.getRecipient()), title(orig.getTitle())
	{
		// Intentionally Left Blank
	}

	// Accessor Functions

	void Email::setSender(const std::string new_sender)
	{
		sender = std::string(new_sender);
	}

	void Email::setRecipient(const std::string new_recipient)
	{
		recipient = std::string(new_recipient);
	}

	void Email::setTitle(const std::string new_title)
	{
		title = std::string(new_title);
	}

	// Mutator Functions

	const std::string Email::getSender(void) const
	{
		return sender;
	}

	const std::string Email::getRecipient(void) const
	{
		return recipient;
	}

	const std::string Email::getTitle(void) const
	{
		return title;
	}

	// Operators

	const Email& Email::operator =(const Email& source)
	{
		Document::operator=(source);
		setSender(source.getSender());
		setRecipient(source.getRecipient());
		setTitle(source.getTitle());

		return *this;
	}


	// End of class Email

} // End of namespace Tasova