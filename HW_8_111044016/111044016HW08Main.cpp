/* 
 * File:   111044016HW08Main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 6, 2012, 10:38 PM
 */

#include <iostream>
#include "111044016HW08Document.h"
#include "111044016HW08Email.h"
#include "111044016HW08File.h"

using namespace std;
using namespace Tasova;

bool containsKeyword(const Document& docObject, const string keyword);

/*
 * 
 */
int main(int argc, char** argv)
{
	// Let's prepare Email objects to test
	Email mail1, mail2, mail3;

	mail1.setSender("person1@example.com");
	mail1.setRecipient("person2@example.com");
	mail1.setTitle("Hello");
	mail1.setText("Hello, how are you? I hope you're fine...");

	mail2.setSender("person2@example.com");
	mail2.setRecipient("person1@example.com");
	mail2.setTitle("RE: Hello");
	mail2.setText("You guessed right, I'm file :). So what abaout you?");

	mail3.setSender("person1@example.com");
	mail3.setRecipient("person2@example.com");
	mail3.setTitle("RE: RE: Hello");
	mail3.setText("I'm happy to heard that. I hope see you soon.");


	// Now, let's do same thing for File objects
	File file1, file2, file3;

	file1.setPathName("~/111044016HW08TestFiles/");
	file1.setText("Hello world from File #1");

	file2.setPathName("~/111044016HW08TestFiles/");
	file2.setText("Hello world from File #2");

	file3.setPathName("~/111044016HW08TestFiles/");
	file3.setText("Hello world from File #3");
	
	// Now it's time for testing
	string keyword;
	cout << "Personally I prefer \"Hello\" as keyword to search\n";
	cout << "Enter keyword to search ==> ";
	cin >> keyword;
	
	cout << "Now checking Email objects..." << endl;
	
	cout << "Is MAIL#1 has keyword \"" << keyword << "\" ==> ";
	cout << boolalpha << containsKeyword(mail1, keyword) << endl;
	
	cout << "Is MAIL#2 has keyword \"" << keyword << "\" ==> ";
	cout << boolalpha << containsKeyword(mail2, keyword) << endl;
	
	cout << "Is MAIL#3 has keyword \"" << keyword << "\" ==> ";
	cout << boolalpha << containsKeyword(mail3, keyword) << endl;
	
	cout << "Now checking File objects..." << endl;
	
	cout << "Is FILE#1 has keyword \"" << keyword << "\" ==> ";
	cout << boolalpha << containsKeyword(file1, keyword) << endl;
	
	cout << "Is FILE#2 has keyword \"" << keyword << "\" ==> ";
	cout << boolalpha << containsKeyword(file2, keyword) << endl;
	
	cout << "Is FILE#3 has keyword \"" << keyword << "\" ==> ";
	cout << boolalpha << containsKeyword(file3, keyword) << endl;

	return 0;
}

bool containsKeyword(const Document& docObject, const string keyword)
{
	if (docObject.getText().find(keyword) != string::npos)
		return true;
	return false;
}
