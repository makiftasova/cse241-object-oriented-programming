/* 
 * File:   111044016HW08Email.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 6, 2012, 10:41 PM
 */



#ifndef EMAIL_H
#define	EMAIL_H

#include "111044016HW08Document.h"

#include <string>
#include <iostream>

namespace Tasova {

	// ***Class Definition Begins***

	class Email : public Document {
	public:
		// Constructors
		Email();

		Email(const std::string new_sender);

		Email(const std::string new_sender,
			const std::string new_recipient);

		Email(const std::string new_sender,
			const std::string new_recipient,
			const std::string new_title);

		Email(const std::string new_sender,
			const std::string new_recipient,
			const std::string new_title,
			const std::string new_text);

		// Copy Constructor
		Email(const Email& orig);

		// No need for destructor because there is no dynamic
		//	memory allocation at all

		// Accessor Functions
		const std::string getSender(void) const;
		const std::string getRecipient(void) const;
		const std::string getTitle(void) const;

		// Mutator Functions
		void setSender(const std::string new_sender);
		void setRecipient(const std::string new_recipient);
		void setTitle(const std::string new_title);

		// Operators
		const Email& operator =(const Email& source);

	private:
		std::string sender;
		std::string recipient;
		std::string title;

	}; // End of class Email

	namespace { // Some useful constants

		const std::string DEFAULT_SENDER("johndoe@example.com");
		const std::string DEFAULT_RECIPIENT("janedoe@example.com");
		const std::string DEFAULT_TITLE("How Are You?");

	} // End of unnamed namespace


} // End of namespace Tasova

#endif	/* EMAIL_H */