/* 
 * File:   111044016HW08File.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on December 6, 2012, 10:42 PM
 */

#include "111044016HW08File.h"

namespace Tasova {

	File::File() : Document(), path_name(DEFAULT_PATH_NAME)
	{
		// Intentionally left blank
	}

	File::File(const std::string file_path)
	: Document(), path_name(file_path)
	{
		// Intentionally left blank
	}

	File::File(const std::string file_path, const std::string file_text)
	: Document(file_text), path_name(file_path)
	{
		// Intentionally left blank
	}

	File::File(const File& orig)
	: Document(orig.getText()), path_name(orig.getPathName())
	{
		// Intentionally left blank
	}

	// Returns file path

	const std::string File::getPathName(void) const
	{
		return path_name;
	}

	// Sets file path

	void File::setPathName(const std::string user_path)
	{
		path_name = std::string(user_path);
	}

	// Operators

	const File& File::operator =(const File& source)
	{
		
		Document::operator=(source);
		setPathName(source.getPathName());

		return *this;
	}

	// End of class File

} // End of namespace Tasova
