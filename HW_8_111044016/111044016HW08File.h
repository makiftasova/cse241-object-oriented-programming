/* 
 * File:   111044016HW08File.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 6, 2012, 10:42 PM
 */

#ifndef FILE_H
#define	FILE_H

#include "111044016HW08Document.h"

#include <string>
#include <iostream>


namespace Tasova {

	class File : public Document {
	public:
		// Constructors
		File();
		File(const std::string file_path);
		File(const std::string file_path, const std::string file_text);

		// Copy Constructor
		File(const File& orig);

		// No need for destructor because there is no dynamic
		//	memory allocation at all

		// Accessor Functions
		const std::string getPathName(void) const;

		// Mutator Functions
		void setPathName(const std::string user_path);

		// Operators
		const File& operator =(const File& source);

	private:
		std::string path_name;

	}; // End of class File

	namespace { // some useful constants

		const std::string DEFAULT_PATH_NAME("~/.cse241hw08/");
		
	} // End of unnamed namespace

} // End of namespace Tasova

#endif	/* FILE_H */

