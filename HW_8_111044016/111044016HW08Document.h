/* 
 * File:   111044016HW08Document.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on December 6, 2012, 10:41 PM
 */

#ifndef DOCUMENT_H
#define	DOCUMENT_H

#include <string>
#include <iostream>

namespace Tasova {

	class Document {
	public:

		// Constructors
		Document();
		Document(const std::string user_text);

		// Copy Constructor
		Document(const Document& orig);

		// No need for destructor because there is no dynamic
		//	memory allocation at all

		// Accessor Functions
		std::string getText(void) const;

		// Mutator Functions
		void setText(const std::string user_text);

		// Operators
		const Document& operator =(const Document& other);
		const Document& operator =(const std::string source);

	private:
		std::string text;

	}; //End of class Document

	namespace { // Some useful things
		
		const char *DEFAULT_TEXT = "This is default text";
		
	} // End of unnamed namespace

} // End of namespace Tasova

#endif	/* DOCUMENT_H */
