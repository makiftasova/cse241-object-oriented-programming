/* 
 * File:   111044016HW08Document.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on December 6, 2012, 10:41 PM
 */

#include "111044016HW08Document.h"

namespace Tasova {

	Document::Document() : text(DEFAULT_TEXT)
	{
		// Intentionally left blank
	}

	Document::Document(const std::string user_text) : text(user_text)
	{
		// Intentionally left blank
	}

	Document::Document(const Document & orig) :text(orig.getText())
	{
		// Intentionally left blank
	}

	std::string Document::getText(void) const
	{
		return text;
	}
	
	void Document::setText(const std::string user_text)
	{
		this->text = std::string(user_text);
	}

	const Document& Document::operator =(const Document& other)
	{
		setText(other.getText());
		
		return *this;
	}
	
	const Document& Document::operator =(const std::string source)
	{
		setText(source);
		
		return *this;
	}

	//End of class Document

} // End of namespace Tasova