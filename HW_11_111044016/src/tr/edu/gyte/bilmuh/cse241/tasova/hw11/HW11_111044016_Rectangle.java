package tr.edu.gyte.bilmuh.cse241.tasova.hw11;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * A class for Rectangles.
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class HW11_111044016_Rectangle extends HW11_111044016_Shape {

    private int _width;
    private int _height;
    private int _x;
    private int _y;

    public HW11_111044016_Rectangle(int x, int y, int width, int height) {
        _x = x;
        _y = y;
        _width = width;
        _height = height;

    }

    /**
     * Sets HW11_111044016_Rectangle's line color to given one
     *
     * @param newColor Desired color of line
     */
    @Override
    public HW11_111044016_Rectangle setLineColor(Color newColor) {
        super.setLineColor(newColor);
        return this;
    }

    /**
     * Sets HW11_111044016_Rectangle's line thickness
     *
     * @param newStroke Desired thickness of line
     */
    @Override
    public HW11_111044016_Rectangle setLineThickness(double thickness) {
        super.setLineThickness(thickness);
        return this;
    }

    /**
     * Draws HW11_111044016_Rectangle to given Graphics object
     *
     * @param graph the Graphics object
     *
     * @param xChange Resize ratio at x-axis
     *
     * @param yChange Resize ratio at y-axis
     *
     * @throws ArrayIndexOutOfBoundsException if any point of shape is out of
     * initial JPanel, throws ArrayIndexOutOfBoundsException
     */
    @Override
    public void drawShape(Graphics graph,
            double xChange, double yChange, int panelWidth, int panelHeight)
            throws ArrayIndexOutOfBoundsException {

        int x = getX();
        int y = getY();
        int height = getHeigth();
        int width = getWidth();

        if ((x + width) >= panelWidth) {
            throw new ArrayIndexOutOfBoundsException(
                    "ERROR: Shape is out of bounds" + this.toString());
        }

        if ((y + height) >= panelHeight) {
            throw new ArrayIndexOutOfBoundsException(
                    "ERROR: Shape is out of bounds" + this.toString());
        }

        x *= xChange;
        y *= yChange;
        width *= xChange;
        height *= yChange;

        float stroke = (float) (getLineThickness() * (xChange * yChange));
        BasicStroke tmpStroke = new BasicStroke(stroke);

        Graphics2D graph2d = (Graphics2D) graph;
        graph2d.setPaintMode();
        graph2d.setColor(getLineColor());
        graph2d.setStroke(tmpStroke);
        graph2d.drawRect(x, y, width, height);

    }

    /**
     * Calculates Distance between two point
     *
     * @param x1 First point's x-axis value
     * @param y1 First point's y-axis value
     * @param x2 Second point's x-axis value
     * @param y2 Second point's y-axis value
     *
     * @return Distance between given points
     */
    private int distanceBetweenPoints(int x1, int y1, int x2, int y2) {

        int xPart = (int) Math.pow((x2 - x1), 2);
        int yPart = (int) Math.pow((y2 - y1), 2);
        int result = (int) Math.sqrt(xPart + yPart);

        return result;
    }

    /**
     * Calculates area of HW11_111044016_Rectangle
     *
     * @return Area of HW11_111044016_Rectangle
     */
    @Override
    public double getArea() {
        return (_height * _width);
    }

    /**
     * Calculates perimeter of HW11_111044016_Rectangle
     *
     * @return Perimeter of HW11_111044016_Rectangle
     */
    @Override
    public double getPerimeter() {
        return ((2 * _width) + (2 * _height));
    }

    @Override
    public Integer[] getXCoordinates() {
        Integer[] tmp = new Integer[4];
        tmp[0] = _x;
        tmp[1] = (_x + _width);
        tmp[2] = (_x + _width);
        tmp[3] = _x;

        return tmp;
    }

    @Override
    public Integer[] getYCoordinates() {
        Integer[] tmp = new Integer[4];
        tmp[0] = _y;
        tmp[1] = _y;
        tmp[2] = (_y + _height);
        tmp[3] = (_y + _height);

        return tmp;
    }

    @Override
    public void setXCoordinates(Integer[] xCoords)
            throws ArrayIndexOutOfBoundsException {
        _x = xCoords[0];
    }

    @Override
    public void setYCoordinates(Integer[] yCoords)
            throws ArrayIndexOutOfBoundsException {
        _y = yCoords[0];
    }

    /**
     * Changes width of HW11_111044016_Rectangle
     *
     * @param width New width value of rectangle
     *
     * @return its own
     */
    public HW11_111044016_Rectangle setWidth(int width) {
        _width = width;
        return this;
    }

    /**
     * Changes height of HW11_111044016_Rectangle
     *
     * @param height New height value of rectangle
     *
     * @return its own
     */
    public HW11_111044016_Rectangle setHeight(int height) {
        _height = height;
        return this;
    }

    /**
     * Changes x-axis coordinate of HW11_111044016_Rectangle
     *
     * @param x x-axis coordinate of HW11_111044016_Rectangle
     *
     * @return its own
     */
    public HW11_111044016_Rectangle setX(int x) {
        _x = x;
        return this;
    }

    /**
     * Changes y-axis coordinate of HW11_111044016_Rectangle
     *
     * @param y y-axis coordinate of HW11_111044016_Rectangle
     *
     * @return its own
     */
    public HW11_111044016_Rectangle setY(int y) {
        _y = y;
        return this;
    }

    /**
     * Returns Height of HW11_111044016_Rectangle
     *
     * @return Height of HW11_111044016_Rectangle
     */
    public int getHeigth() {
        return _height;
    }

    /**
     * Returns Width of HW11_111044016_Rectangle
     *
     * @return Width of HW11_111044016_Rectangle
     */
    public int getWidth() {
        return _width;
    }

    /**
     * Return x-axis coordinate of HW11_111044016_Rectangle
     *
     * @return x-axis coordinate of HW11_111044016_Rectangle
     */
    public int getX() {
        return _x;
    }

    /**
     * Return y-axis coordinate of HW11_111044016_Rectangle
     *
     * @return y-axis coordinate of HW11_111044016_Rectangle
     */
    public int getY() {
        return _y;
    }

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    @Override
    public String getClassName() {
        return "Rectangle";
    }

    /**
     * toString method of HW11_111044016_Rectangle class
     *
     * @return A String which contains coordinates, width, height, line color,
     * line thickness, area and perimeter
     */
    @Override
    public String toString() {
        String tmp = "Rectangle: ";
        tmp += ("(" + getX() + ", " + getY() + ")");
        tmp += (" Area: " + getArea() + ",");
        tmp += (" Perimeter: " + getPerimeter() + ".");
        tmp += (" LineThickness: " + getLineThickness() + ", ");
        tmp += (" Line Color: " + HW11_111044016_ColorUtls.getColorName(getLineColor()) + ".");

        return tmp;
    }
}
