package tr.edu.gyte.bilmuh.cse241.tasova.hw11;

import java.awt.Color;

/**
 * HW11_111044016_Circle class of HW10. extends HW11_111044016_Ellipse class since Circles are a specialized
 * form of Ellipses
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class HW11_111044016_Circle extends HW11_111044016_Ellipse {

    /**
     * Default constructor of HW11_111044016_Circle
     *
     * Directly calls super class' default constructor
     */
    public HW11_111044016_Circle() {
        super();
    }

    /**
     * A four parameter constructor for HW11_111044016_Circle class
     *
     * Directly calls super class' four parameter constructor as super(x, y, r,
     * r)
     *
     * Calls super class' four parameter constructor
     *
     * @param x x-axis point of top left corner of square which the HW11_111044016_Circle can
     * fit into it
     *
     * @param y y-axis point of top left corner of square which the HW11_111044016_Circle can
     * fit into it
     *
     * @param r radius of HW11_111044016_Circle
     */
    public HW11_111044016_Circle(int x, int y, int r) {
        super(x, y, r, r);
    }

    /**
     * Sets HW11_111044016_Circle's line color to given one
     *
     * @param newColor Desired color of line
     */
    @Override
    public HW11_111044016_Circle setLineColor(Color newColor) {
        super.setLineColor(newColor);
        return this;
    }

    /**
     * Sets HW11_111044016_Circle's line thickness
     *
     * @param newStroke Desired thickness of line
     */
    @Override
    public HW11_111044016_Circle setLineThickness(double thickness) {
        super.setLineThickness(thickness);
        return this;
    }

    /**
     * A function for setting radius of HW11_111044016_Circle
     *
     * @param radius new radius value of HW11_111044016_Circle
     */
    public void setRadius(int radius) {
        this.setHorizontalRadius(radius);
        this.setVerticalRadius(radius);
    }

    /**
     * A function for setting coordinates of HW11_111044016_Circle
     *
     * @param x x-axis point of top left corner of square which the HW11_111044016_Circle can
     * fit into it
     *
     * @param y y-axis point of top left corner of square which the HW11_111044016_Circle can
     * fit into it
     *
     */
    public void setCoordinates(int x, int y) {
        this.setXCoordinate(x);
        this.setYCoordinate(y);
    }

    /**
     * A function for getting Radius of HW11_111044016_Circle
     *
     * @return Radius of HW11_111044016_Circle
     */
    public int getRadius() {
        return getVerticalRadius();
    }

    /**
     * Returns area of HW11_111044016_Circle
     *
     * @return Area of HW11_111044016_Circle
     */
    @Override
    public double getArea() {
        return (Math.PI * getRadius() * getRadius());
    }

    /**
     * Returns perimeter of HW11_111044016_Circle
     *
     * @return Perimeter of HW11_111044016_Circle
     */
    @Override
    public double getPerimeter() {
        return (2 * Math.PI * getRadius());
    }

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    @Override
    public String getClassName() {
        return "Circle";
    }

    /**
     * toString Method of HW11_111044016_Circle class
     *
     * @return A string which contains coordinates, radius, line color, line
     * thickness, area and perimeter
     */
    @Override
    public String toString() {
        String tmp = "Circle: ";
        tmp += ("M(" + getXCoordinate() + ", " + getYCoordinate() + ")");
        tmp += (", R = " + getRadius() + ".");
        tmp += (" Area: " + getArea() + ",");
        tmp += (" Perimeter: " + getPerimeter() + ".");
        tmp += (" LineThickness: " + getLineThickness() + ", ");
        tmp += (" Line Color: " + HW11_111044016_ColorUtls.getColorName(getLineColor()) + ".");

        return tmp;
    }
}
