package tr.edu.gyte.bilmuh.cse241.tasova.hw11;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

/**
 * HW11_111044016_Shape interface for 2D shapes.
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public abstract class HW11_111044016_Shape extends Object
        implements Serializable {

    private Color _lineColor;
    private float _lineStroke;

    /**
     * Sets Shapes line color to given one
     *
     * @param newColor Desired color of line
     */
    public HW11_111044016_Shape setLineColor(Color newColor) {
        _lineColor = newColor;
        return this;
    }

    /**
     * Return Shapes line color
     *
     * @return Shapes line color
     */
    public Color getLineColor() {
        return _lineColor;
    }

    /**
     * Sets Shapes line thickness
     *
     * @param thickness Desired thickness of line
     */
    public HW11_111044016_Shape setLineThickness(double thickness) {
        _lineStroke = (float) thickness;
        return this;

    }

    /**
     * Returns Shapes line thickness
     *
     * @return thickness of line as double
     */
    public double getLineThickness() {
        return (double) _lineStroke;
    }

    /**
     * Draws shape to panel
     *
     * @param graph a Graphic object to paint shapes
     */
    public abstract void drawShape(Graphics graph,
            double xChange, double yChange, int panelWidth, int panelheight)
            throws ArrayIndexOutOfBoundsException;

    /**
     * Calculates area of shape then returns it
     *
     */
    public abstract double getArea();

    /**
     * Calculates perimeter of shape then returns it
     *
     */
    public abstract double getPerimeter();

    /**
     * returns x-axis coordinates of points of shapes
     *
     */
    public abstract Integer[] getXCoordinates();

    /**
     * returns y-axis coordinates of points of shapes
     *
     */
    public abstract Integer[] getYCoordinates();

    /**
     * Set x coordinates of points of shape
     *
     * @param xCoords an Integer[ ] of x-axis coordinates of points
     */
    public abstract void setXCoordinates(Integer[] xCoords)
            throws ArrayIndexOutOfBoundsException;

    /**
     * Set y coordinates of points of shape
     *
     * @param yCoords an Integer[ ] of y-axis coordinates of points
     */
    public abstract void setYCoordinates(Integer[] yCoords)
            throws ArrayIndexOutOfBoundsException;

    /**
     * Returns name of class as String
     *
     * @return name of class as String
     */
    public String getClassName() {
        return "Shape";
    }

    /**
     * to String method of abstract class HW11_111044016_Shape
     *
     * @return Returns a string contains something like "this is an object of an
     * abstract class"
     */
    @Override
    public String toString() {
        String tmp = this.getClass().getName();
        tmp += (" is an object of Abstract Class Shape");
        return tmp;
    }
}