package tr.edu.gyte.bilmuh.cse241.tasova.hw11;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Color Utilities Class. Contains all defined colors in java.awt.Color . This
 * class is useful when need to color name as String
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public final class HW11_111044016_ColorUtls {

    // A final static ArrayList which contains 
    //  all defined colors from java.awt.Color
    private static final ArrayList<Color> _colors = new ArrayList<>(
            Arrays.asList(Color.black, Color.blue, Color.cyan, Color.darkGray,
            Color.gray, Color.green, Color.lightGray, Color.magenta,
            Color.orange, Color.pink, Color.red, Color.white, Color.yellow));

    /**
     * Returns index of given color in ArrayList
     *
     * @param color Color object to find
     *
     * @return index of Color object, if found
     *
     * @throws IndexOutOfBoundsException if given index is out of range throws
     * IndexOutOfBoundsException (index is lower than 0 or greater than 12)
     */
    public static int getColorIndex(Color color)
            throws IndexOutOfBoundsException {
        return _colors.indexOf(color);
    }

    /**
     * Gets an index between 0 to 12 then returns its corresponding color
     *
     * @param index the index value between 0-12
     *
     * @return chosen Color
     *
     * @throws IndexOutOfBoundsException if given index is out of range throws
     * IndexOutOfBoundsException (index is lower than 0 or greater than 12)
     */
    public static Color colorSelector(int index)
            throws IndexOutOfBoundsException {
        return _colors.get(index);
    }

    /**
     * Returns name of given color
     *
     * @param color Tho Color object to find its name
     *
     * @return name of given Color object if found
     *
     * @throws IndexOutOfBoundsException if given index is out of range throws
     * IndexOutOfBoundsException (index is lower than 0 or greater than 12)
     */
    public static String getColorName(Color color)
            throws IndexOutOfBoundsException {
        int colorCode = getColorIndex(color);
        String name = "Not Found";

        switch (colorCode) {
            case 0:
                name = "Black";
                break;
            case 1:
                name = "Blue";
                break;
            case 2:
                name = "Cyan";
                break;
            case 3:
                name = "Dark Gray";
                break;
            case 4:
                name = "Gray";
                break;
            case 5:
                name = "Green";
                break;
            case 6:
                name = "Light Gray";
                break;
            case 7:
                name = "Magenta";
                break;
            case 8:
                name = "Orange";
                break;
            case 9:
                name = "Pink"; //Floyd
                break;
            case 10:
                name = "Red";
                break;
            case 11:
                name = "White";
                break;
            case 12:
                name = "Yellow";
                break;
            default:
                throw new IndexOutOfBoundsException(
                        "ERROR: Given index is out of range\n");
        }

        return name;
    }
}
