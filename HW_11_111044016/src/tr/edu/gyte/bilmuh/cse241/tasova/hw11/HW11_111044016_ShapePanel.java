package tr.edu.gyte.bilmuh.cse241.tasova.hw11;

import java.awt.Graphics;
import java.awt.Toolkit;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.swing.JPanel;

/**
 * HW11_111044016_ShapePanel class of HW10. HW11_111044016_ShapePanel used for
 * drawing shapes to a JPanel
 *
 * @author Mehmet Akif TAŞOVA <makiftasova@gmail.com> 111044016
 */
public class HW11_111044016_ShapePanel extends JPanel {

    // Width of JPanel
    private int _panelWidth;
    // Height of Jpanel
    private int _panelheight;
    // same thing as private ArrayList<Shape> _shapes = new ArrayList<Shape>();
    private ArrayList<HW11_111044016_Shape> _shapes = new ArrayList<>();
    static private Toolkit toolkit = Toolkit.getDefaultToolkit();

    /**
     * Generates a HW11_111044016_ShapePanel at active JFrame size
     */
    public HW11_111044016_ShapePanel() {
        _panelWidth = (int) toolkit.getScreenSize().getWidth();
        _panelheight = (int) toolkit.getScreenSize().getHeight();
    }

    /**
     * Generates a HW11_111044016_ShapePanel at active JFrame size and given
     * Shapes in ArrayList<Shapes>
     *
     * @param shapes ArrayList<Shapes> object which contains desired Shapes in
     * HW11_111044016_ShapePanel
     */
    public HW11_111044016_ShapePanel(ArrayList<HW11_111044016_Shape> shapes) {
        _panelWidth = (int) toolkit.getScreenSize().getWidth();
        _panelheight = (int) toolkit.getScreenSize().getHeight();
        _shapes = shapes;
    }

    /**
     * Generates HW11_111044016_ShapePanel with desired size
     *
     * @param width With of Panel
     * @param height Height of panel
     */
    public HW11_111044016_ShapePanel(int width, int height) {
        _panelWidth = width;
        _panelheight = height;
    }

    /**
     * Generates HW11_111044016_ShapePanel with desired size and given Shapes in
     * ArrayList<Shapes>
     *
     * @param width width With of Panel
     * @param height height Height of panel
     * @param shapes ArrayList<Shapes> object which contains desired Shapes in
     * HW11_111044016_ShapePanel
     */
    public HW11_111044016_ShapePanel(int width, int height, ArrayList<HW11_111044016_Shape> shapes) {
        _panelWidth = width;
        _panelheight = height;
        _shapes = shapes;
    }

    /**
     * Adds a HW11_111044016_Shape object to Shapes list which contains Shapes
     * to draw
     *
     * @param shape A brand new shape to add
     */
    public void addShape(HW11_111044016_Shape shape) {
        _shapes.add(shape);
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    public int getNumberOfShapes() {
        return _shapes.size();
    }

    /**
     * Returns HW11_111044016_Shape object at the indexth order, will not remove
     * the HW11_111044016_Shape object from panel
     *
     * @param index Index of HW11_111044016_Shape object to get
     *
     * @return HW11_111044016_Shape object at given index
     */
    public HW11_111044016_Shape get(int index) {
        return _shapes.get(index);
    }

    /**
     *
     * Removes indexth shape from HW11_111044016_Shape list which contains
     * Shapes to draw
     *
     * if given index value greater than element number of list throws
     * IndexOutOfBoundsException
     *
     * @param index the precious index value
     *
     * @return deleted HW11_111044016_Shape from list
     */
    public HW11_111044016_Shape removeShape(int index) throws IndexOutOfBoundsException {
        try {
            return _shapes.remove(index);
        } catch (IndexOutOfBoundsException e) {
            String exceptionStr = "ERROR: ";
            exceptionStr += "Index is out of range ";
            exceptionStr += "at ShapePanel.removeShape(int)\n";
            exceptionStr += (e.getMessage() + "\n");
            exceptionStr += "Index value must be lower than Size value\n";
            throw new IndexOutOfBoundsException(exceptionStr);
        }
    }

    /**
     * Removes given HW11_111044016_Shape object from list,
     *
     * @param shape the HW11_111044016_Shape object to remove
     *
     * @return true if HW11_111044016_Shape object found and removed in list
     */
    public boolean removeShape(HW11_111044016_Shape shape) {
        return _shapes.remove(shape);
    }

    /**
     * Removes all of the elements from this panel. The panel will be empty
     * after this call returns
     */
    public void clear() {
        _shapes.clear();
    }

    /**
     * Returns Information of Shapes stored in HW11_111044016_ShapePanel in a
     * String
     *
     * @return a string which contains information of Shapes stored in
     */
    @Override
    public String toString() {
        String shapeList = "";
        int i = 0;
        for (HW11_111044016_Shape s : _shapes) {
            shapeList += ("#" + (i + 1) + " - " + s.toString() + "\n");
            ++i;
        }
        return shapeList;
    }

    public ArrayList<HW11_111044016_Shape> toArrayList() {
        ArrayList<HW11_111044016_Shape> tmp;
        tmp = new ArrayList<>(_shapes);
        return tmp;
    }

    /**
     *
     * Prints a listing Shapes stored in this component to the specified output
     * stream.
     *
     * @param out a print stream
     *
     * @throws NullPointerException if out is null
     */
    public void printShapesInfo(PrintStream out) throws NullPointerException {
        if (out == null) {
            throw new NullPointerException(
                    "ERROR: No Output Specified");
        }

        if (getNumberOfShapes() == 0) {
            out.println("No Shape To List");
            return;
        }

        String shapeList = "";
        for (HW11_111044016_Shape s : _shapes) {
            shapeList += (s.toString() + "\n");
        }

        out.println(shapeList);
    }

    /**
     *
     * Prints a listing Shapes stored in this component to the specified output
     * writer.
     *
     * @param out a print writer
     *
     * @throws NullPointerException if out is null
     */
    public void printShapesInfo(PrintWriter out) throws NullPointerException {
        if (out == null) {
            throw new NullPointerException(
                    "ERROR: No Output Specified");
        }

        if (getNumberOfShapes() == 0) {
            out.println("No Shape To List");
            return;
        }

        int i = 0;
        String shapeList;
        shapeList = "";
        for (HW11_111044016_Shape s : _shapes) {
            shapeList += ("#" + i + s.toString() + "\n");
            ++i;
        }

        out.println(shapeList);
    }

    /**
     * Takes a graphics object, then draws all shapes on it
     *
     * @param graph the Graphics object
     */
    @Override
    public void paintComponent(Graphics graph) {
        super.paintComponent(graph);

// Some messages to developer
//        System.out.print("Frame Resolution:" + getWidth());
//        System.out.println("x" + getHeight());

        double xChange = getWidth() / (double) _panelWidth;
        double yChange = getHeight() / (double) _panelheight;

        for (HW11_111044016_Shape shape : _shapes) {
            try {
                shape.drawShape(graph, xChange, yChange,
                        _panelWidth, _panelheight);
            } catch (Exception e) {
                System.out.println(e.getMessage());
//                e.printStackTrace(System.out);
//                JLabel errorLabel = new JLabel("Error: " + e.getMessage());
//                errorLabel.setLocation(_panelWidth / 2, _panelheight / 2);
//                errorLabel.setForeground(Color.red);
//                add(errorLabel);
                break;
            }
        }

    } // End of method paintComponent
}