/* 
 * File:   111044016HW07Person.h
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 20, 2012, 4:13 PM
 */

#ifndef PERSON_H
#define	PERSON_H

#include <iostream>

namespace Tasova {

	enum sex_t {
		female, male
	}; // type of a person's sex

	enum marital_t {
		married, single
	}; // type of a paerson's marital status as basically

	typedef unsigned short year_t; // year type
	typedef unsigned int size_t; // size type

	class Person {
	public:
		Person();
		Person(const char* u_name, const char* u_surname, sex_t u_sex,
			const year_t u_birth_year);

		Person(const char* u_name, const char* u_surname, sex_t u_sex,
			const year_t u_birth_year,
			unsigned int u_family_id);

		Person(const Person& orig);
		~Person();
		
		// Mutator Functions
		void setName(const char* u_name);
		void setSurname(const char* u_surname);
		void setBirthYear(const year_t u_birth_year);
		void setSex(const sex_t u_sex);
		void setFamilyID(const unsigned int u_family_code);
		void setMaritalStatus(const marital_t u_marital_status);

		// Accessor Functions
		const char* getName(void) const;
		const char* getSurname(void) const;
		const year_t getBirthYear(void) const;
		const sex_t getSex(void) const;
		const unsigned int getFamilyID(void) const;
		const marital_t getMaritalStatus(void) const;
		
		// Binary File Input/Output functions
		//	both function returns true if success
		const bool writeToBinaryFile(std::fstream& file) const;
		const bool readFromBinaryFile(std::fstream& file);

		// For the big three
		const Person operator =(const Person& source);

		// Some useful operators
		const bool operator ==(const Person& other) const;
		const bool operator !=(const Person& other) const;

		// Unavoidable friend operators
		friend std::ostream& operator <<(std::ostream& output,
			const Person& person);

		friend std::istream& operator >>(std::istream& input,
			Person& person);

	private:
		char *name;
		size_t cap_name; // capacity of name
		char *surname;
		size_t cap_surname; // capacity of surname
		year_t birth_year;
		sex_t sex;
		unsigned int family_id;
		marital_t marital_status;

	};


	namespace { // Utilities namespace

		// const chars
		const char CHAR_NULL = '\0'; // Null character
		const char CHAR_NEWLINE = '\n'; // Newline character
		const char CHAR_MALE = 'm'; // Male sign
		const char CHAR_FEMALE = 'f'; // Female sign
		const char CHAR_MARRIED = 'm'; // Married sign
		const char CHAR_SINGLE = 's'; // Single sign

		// const strings
		const char *STR_MALE = "male";
		const char *STR_FEMALE = "female";

		// Buffer Lengths
		const size_t PERSON_NAME_BUFF_LEN = 25;
		const size_t PERSON_SNAME_BUFF_LEN = 15;

		// functions
		unsigned int strlen(const char* string);
		const char* strcpy(const char* src, char* dest);
		bool strcmp(const char str1[], const char str2[]);

	} // End of utilities namespace

} //End of Tasova Namespace

#endif	/* PERSON_H */

