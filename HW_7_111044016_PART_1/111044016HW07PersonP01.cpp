/* 
 * File:   111044016HW07Person.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 * 
 * Created on November 20, 2012, 4:13 PM
 */

#include "111044016HW07PersonP01.h"
#include "111044016HW07FamilyP01.h"


namespace Tasova {

	Person::Person() : name(NULL), surname(NULL),
	birth_year(0), sex(male), cap_name(0), cap_surname(0), family_id(0),
	marital_status(single)
	{
		// Intentionally left blank
	}

	Person::Person(const char* u_name, const char* u_surname,
		sex_t u_sex, const year_t u_birth_year)
	: birth_year(u_birth_year), sex(u_sex), family_id(0),
	marital_status(single)
	{
		size_t tmp1 = strlen(u_name);
		cap_name = (tmp1 + 1);
		name = new char[cap_name];
		strcpy(u_name, name);

		size_t tmp2 = strlen(u_surname);
		cap_surname = (tmp2 + 1);
		surname = new char[cap_surname];
		strcpy(u_surname, surname);

	}

	Person::Person(const char* u_name, const char* u_surname,
		sex_t u_sex, const year_t u_birth_year,
		unsigned int u_family_id)
	: birth_year(u_birth_year), sex(u_sex), family_id(u_family_id),
	marital_status(single)
	{
		size_t tmp1 = strlen(u_name);
		cap_name = (tmp1 + 1);
		name = new char[cap_name];
		strcpy(u_name, name);

		size_t tmp2 = strlen(u_surname);
		cap_surname = (tmp2 + 1);
		surname = new char[cap_surname];
		strcpy(u_surname, surname);

	}

	Person::Person(const Person& orig)
	: birth_year(orig.getBirthYear()), sex(orig.getSex()),
	cap_name(orig.cap_name), cap_surname(orig.cap_surname),
	family_id(orig.getFamilyID()), marital_status(orig.getMaritalStatus())
	{
		name = new char[cap_name];
		strcpy(orig.name, name);

		surname = new char[cap_surname];
		strcpy(orig.surname, surname);
	}

	// Miss little destructor

	Person::~Person()
	{
		delete [] name;
		delete [] surname;
	}

	// Setter functions to set something

	void Person::setName(const char* u_name)
	{
		size_t tmp = strlen(u_name);

		cap_name = (tmp + 1);
		delete [] name;
		name = new char[cap_name];
		strcpy(u_name, name);
	}

	void Person::setSurname(const char* u_surname)
	{
		size_t tmp = strlen(u_surname);
		cap_surname = (tmp + 1);
		delete [] surname;
		surname = new char[cap_surname];
		strcpy(u_surname, surname);
	}

	void Person::setBirthYear(const year_t u_birth_year)
	{
		birth_year = u_birth_year;
	}

	void Person::setSex(const sex_t u_sex)
	{
		sex = u_sex;
	}

	void Person::setFamilyID(const unsigned int u_family_id)
	{
		family_id = u_family_id;
	}

	void Person::setMaritalStatus(const marital_t u_marital_status)
	{
		marital_status = u_marital_status;
	}

	// Getter functions to get something

	const char* Person::getName() const
	{
		return name;
	}

	const char* Person::getSurname() const
	{
		return surname;
	}

	const year_t Person::getBirthYear() const
	{
		return birth_year;
	}

	const sex_t Person::getSex() const
	{
		return sex;
	}

	const unsigned int Person::getFamilyID(void) const
	{
		return family_id;
	}

	const marital_t Person::getMaritalStatus(void) const
	{
		return marital_status;
	}

	// Binary File Input/Output functions
	//	both function returns true if success

	const bool Person::writeToBinaryFile(std::fstream& file) const
	{
		file.write(getName(), (PERSON_NAME_BUFF_LEN * sizeof(char)));
		file.write(getSurname(),
			(PERSON_SNAME_BUFF_LEN * sizeof(char)));

		// gender sign stores as a char in binary file
		char sex = ((male == getSex() ? CHAR_MALE : CHAR_FEMALE));
		file.write(&sex, sizeof(char));
		year_t byear = getBirthYear();
		file.write((char *)(&byear), sizeof(year_t));
//		file << getBirthYear();

		// marital status sign stores as a char in binary file
		char mart_stat = ((married == getMaritalStatus())
			? CHAR_MARRIED : CHAR_SINGLE);
		file.write(&mart_stat, sizeof(char));

		unsigned int fam_id = getFamilyID();
		file.write((char *)(&fam_id), sizeof(unsigned int));
//		file << getFamilyID();

		return true;
	}

	const bool Person::readFromBinaryFile(std::fstream& file)
	{
		char *name(new char[PERSON_NAME_BUFF_LEN + 1]);
		file.read(name, PERSON_NAME_BUFF_LEN);
		setName(name);

		char *surname(new char[PERSON_SNAME_BUFF_LEN + 1]);
		file.read(surname, PERSON_SNAME_BUFF_LEN);
		setSurname(surname);

		// gender sign storen as a char in binary file
		char sex;
		file.read(&sex, sizeof(char));
		setSex(((sex == CHAR_MALE) ? male : female));

		year_t byear;
		file.read((char *)(&byear), sizeof(year_t));
//		file >> byear;
		setBirthYear(byear);

		// marital status sign storen as a char in binary file
		char mart_stat;
		file.read(&mart_stat, sizeof(char));
		marital_t m_status = ((mart_stat == CHAR_MARRIED)
			? married : single);
		setMaritalStatus(m_status);

		unsigned int fam_id;
		file.read((char *)(&fam_id), sizeof(unsigned int));
//		file >> fam_id;
		setFamilyID(fam_id);

		return true;
	}

	const Person Person::operator =(const Person& source)
	{
		// Am I same with the other object?
		if (this != &source) {
			setName(source.getName());
			setSurname(source.getSurname());
			setSex(source.getSex());
			setBirthYear(source.getBirthYear());
			setFamilyID(source.getFamilyID());
		}

		return *this;
	}

	// All of people are equal, but some people are more equal than others

	const bool Person::operator ==(const Person& other) const
	{
		if (getSex() != other.getSex())
			return false;
		if (getBirthYear() != other.getBirthYear())
			return false;
		if (!(strcmp(getSurname(), other.getSurname())))
			return false;
		if (!(strcmp(getName(), other.getName())))
			return false;
		return true;
	}

	const bool Person::operator !=(const Person& other) const
	{
		if (getSex() == other.getSex())
			return false;
		if (getBirthYear() == other.getBirthYear())
			return false;
		if (strcmp(getSurname(), other.getSurname()))
			return false;
		if (strcmp(getName(), other.getName()))
			return false;
		return true;
	}

	// Unavoidable friend operators

	// I can insert data into stream

	std::ostream& operator <<(std::ostream& output,
		const Person& person)
	{

		output << person.getName() << " "
			<< person.getSurname() << " ";

		switch (person.getSex()) {
		case male:
			output << STR_MALE << " ";
			break;
		case female:
			output << STR_FEMALE << " ";
			break;
		default:
			output << " ";
		}

		output << person.getBirthYear() << " ";

		// out of use because of general functionality
		//output << person.getFamilyID();

		return output;
	}

	// I can extract data from stream

	std::istream& operator >>(std::istream& input, Person& person)
	{
		std::cout << "Enter Name: ";
		char *u_name;
		u_name = new char[PERSON_NAME_BUFF_LEN + 1];
		input.getline(u_name, PERSON_NAME_BUFF_LEN,
			CHAR_NEWLINE);
		person.setName(u_name);
		delete [] u_name;

		std::cout << "Enter Surname: ";
		char *u_surname;
		u_surname = new char[PERSON_SNAME_BUFF_LEN + 1];
		input.getline(u_surname, PERSON_SNAME_BUFF_LEN,
			CHAR_NEWLINE);
		person.setSurname(u_surname);
		delete [] u_surname;

		std::cout << "Enter Sex (m/f): ";
		char u_sex;
		input >> u_sex;
		person.setSex(((u_sex == CHAR_MALE) ? male : female));

		std::cout << "Enter Birth Year: ";
		year_t u_birth_year;
		input >> u_birth_year;
		person.setBirthYear(u_birth_year);

// *** This part is out of use because conflicts with Family class ***
//		std::cout << "Enter Family ID: ";
//		code_t u_family_id;
//		input >> u_family_id;
//		person.setFamilyID(u_family_id);

		return input;
	}

	// *** End Of Person Class ***


	namespace { // Utilities namespace

		unsigned int strlen(const char* string)
		{
			if (CHAR_NULL == string)
				return 0;

			unsigned int i = 0;
			for (i = 0; string[i] != CHAR_NULL; ++i)
				continue;
			return i;
		}

		const char* strcpy(const char* src, char* dest)
		{
			int len = strlen(src);
			int i = 0;

			for (i = 0; i < len; ++i)
				dest[i] = src[i];

			dest[i] = CHAR_NULL;

			return dest;
		}

		// simplified version of strcmp from cstring

		bool strcmp(const char str1[], const char str2[])
		{
			int i = 0;
			int len1 = strlen(str1), len2 = strlen(str2);
			bool result = false;
			if (len1 == len2) {
				for (i; (str1[i] == str2[i]) && (i < len1); ++i)
					continue;
				if (i == len1)
					result = true;
			}
			return result;
		}

	} // End of utilities namespace

} //End of Tasova Namespace
