/* 
 * File:   111044016HW07Main.cpp
 * Author: Mehmet Akif TAŞOVA <makiftasova@gmail.com>
 * Student Number: 111044016
 *
 * Created on November 20, 2012, 4:11 PM
 */

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "111044016HW07FamilyP01.h"


using namespace std;
using namespace Tasova;

// ****Some useful constants
// **Some exit codes
const unsigned int OUTPUT_FILE_UNABLE_TO_CREATE = 5;

// **A unique person object to point end of family
const Person EMPTY_PERSON_TEST("JOHN", "DOE", male, 1000);

// **Name of input file
const char* FILE_NAME = "111044016HW07Families.bin";

// Family creator functions
const Family createFamilyNicholasI(void);
const Family createFamilyAlexanderII(void);
const Family createFamilyAlexanderIII(void);
const Family createFamilyNicholasII(void);

/*
 * 
 */
int main(int argc, char** argv)
{


	Family familyOfNicholasI = createFamilyNicholasI();
	Family familyOfAlexanderII = createFamilyAlexanderII();
	Family familyOfAlexanderIII = createFamilyAlexanderIII();
	Family familyOfNicholasII = createFamilyNicholasII();

	// lets write families to a binary file
	fstream output(FILE_NAME, ios::out | ios::binary);
	if (output.is_open()) {
		cout << "Writing data to \"" << FILE_NAME << "\"" << endl;

		familyOfNicholasI.writeToBinaryFile(output);
		familyOfAlexanderII.writeToBinaryFile(output);
		familyOfAlexanderIII.writeToBinaryFile(output);
		familyOfNicholasII.writeToBinaryFile(output);
		output.close();
		cout << "Writing Done...\n";
	} else {
		cout << "Cannot create file \"" << FILE_NAME << "\"" << endl;
		cout << "Please check your permissions" << endl;
		exit(OUTPUT_FILE_UNABLE_TO_CREATE);
	}

	return(EXIT_SUCCESS);
}

// Family creator functions

const Family createFamilyNicholasI(void)
{
	// ****Family of Nicholas I
	// **Parents
	Person nicholas_1_emp("Nicholas", "Romanov", male, 1796);
	Person alexandra_quenn("Alexandra", "Feodorovna", female, 1798);
	// **Children
	Person elizabeth("Elizabeth", "Nicholova", female, 1826);
	Person nicholas("Nicholas", "Nicholov", male, 1831);
	Person olga("Olga", "Nicholova", female, 1822);
	Person michael("Michael", "Nicholov", male, 1832);
	Person alexander_2_emp("Alexander", "Nicholov", male, 1818);
	Person constantine("Constantine", "Nicholov", male, 1827);
	Person alexandra("Alexandra", "Nicholova", female, 1825);
	Person maria("Maria", "Nicholova", female, 1819);

	Family royal_court_of_nicholas_1(nicholas_1_emp, alexandra);

	royal_court_of_nicholas_1 += elizabeth;
	royal_court_of_nicholas_1 += nicholas;
	royal_court_of_nicholas_1 += olga;
	royal_court_of_nicholas_1 += michael;
	royal_court_of_nicholas_1 += alexander_2_emp;
	royal_court_of_nicholas_1 += constantine;
	royal_court_of_nicholas_1 += alexandra;
	royal_court_of_nicholas_1 += maria;

	return royal_court_of_nicholas_1;
}

const Family createFamilyAlexanderII(void)
{
	// ****Family of Alexander II
	// ** Father of family is alexander_2_emp from  Nicholas I's family
	Person alexander_2_emp("Alexander", "Nicholov", male, 1818);
	Person maria_2("Maria", "Alexandrovna", female, 1824);
	// **Children
	Person vladimir("Vladimir", "Alexandrov", male, 1847);
	Person alexandra_2("Alexandra", "Alexandrova", female, 1842);
	Person nicholas_2("Nicholas", "Alexandrov", male, 1843);
	Person sergei("Sergei", "Alexandrov", male, 1857);
	Person alexander_3_emp("Alexander", "Alexandrov", male, 1845);
	Person alexei("Alexei", "Alexandrov", male, 1850);
	Person paul("Paul", "Alexandrov", male, 1860);
	Person maria_3("Maria", "Alexandrova", female, 1853);

	Family royal_court_of_alexander_2(alexander_2_emp, maria_2);

	royal_court_of_alexander_2 += vladimir;
	royal_court_of_alexander_2 += alexandra_2;
	royal_court_of_alexander_2 += nicholas_2;
	royal_court_of_alexander_2 += sergei;
	royal_court_of_alexander_2 += alexander_3_emp;
	royal_court_of_alexander_2 += alexei;
	royal_court_of_alexander_2 += paul;
	royal_court_of_alexander_2 += maria_3;
	
	return royal_court_of_alexander_2;
}

const Family createFamilyAlexanderIII(void)
{
	// ****Family of Alexander III
	// ** Father of family is alexander_3_emp from  Alexander II's family
	Person alexander_3_emp("Alexander", "Alexandrov", male, 1845);
	Person maria_4("Maria", "Feodorovna", female, 1847);
	// **Children
	Person alexander("Alexander", "Alexandrov", male, 1869);
	Person george("George", "Alexandrov", male, 1870);
	Person xenia("Xenia", "Alexandrova", female, 1875);
	Person nicholas_2_emp("Nicholas", "Alexandrov", male, 1868);
	Person olga_2("Olga", "Alexandrova", female, 1882);
	Person michael_2("Michael", "Alexandrov", male, 1878);

	Family royal_court_of_alexander_3(alexander_3_emp, maria_4);

	royal_court_of_alexander_3 += alexander;
	royal_court_of_alexander_3 += george;
	royal_court_of_alexander_3 += xenia;
	royal_court_of_alexander_3 += nicholas_2_emp;
	royal_court_of_alexander_3 += olga_2;
	royal_court_of_alexander_3 += michael_2;
	
	return royal_court_of_alexander_3;
}

const Family createFamilyNicholasII(void)
{
		// ****Family of Nicholas II
	// ** Father of family is nicholas_2_emp from  Alexander III's family
	Person nicholas_2_emp("Nicholas", "Alexandrov", male, 1868);
	Person alexandra_3("Alexandra", "Fyodorovna", female, 1872);
	// **Children
	Person olga_3("Olga", "Nicholova", female, 1895);
	Person tatiana("Tatiana", "Nicholova", female, 1897);
	Person maria_5("Maria", "Nicholova", female, 1899);
	Person anastasia("Anastasia", "Nicholova", female, 1901);
	Person alexei_2("Alexei", "Nicholov", male, 1904);

	Family royal_court_of_nicholas_2(nicholas_2_emp, alexandra_3);

	royal_court_of_nicholas_2 += olga_3;
	royal_court_of_nicholas_2 += tatiana;
	royal_court_of_nicholas_2 += maria_5;
	royal_court_of_nicholas_2 += anastasia;
	royal_court_of_nicholas_2 += alexei_2;

	return royal_court_of_nicholas_2;
}