/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hw10_simple;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 *
 * @author Abdullah
 */
public class Canvas extends JPanel  {

    public Canvas() {
        super();
        
        _c = new Circle(100, 100, 100);
        
        
        lab1 = new JLabel("Ben bir kucuk cezveyim");  
        setLayout( null );
        add(lab1);

        
    }
    
    public void set_label_pos(int x, int y){
    
         lab1.setBounds(x+20, y+(int)_c.getR()-20, 200, 50);
    }
    
    
  public void paintComponent(Graphics g) {
      
      super.paintComponent(g);

      Graphics2D g2d = (Graphics2D) g;

      g2d.setColor(Color.blue);
    
      _c.draw(g2d);
      
     
      
  }

  
     private Circle _c;
     private JLabel lab1;

    /**
     * @return the _c
     */
    public Circle getC() {
        return _c;
    }

    /**
     * @param c the _c to set
     */
    public void setC(Circle c) {
        this._c = c;
    }
}
