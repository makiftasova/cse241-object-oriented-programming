/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hw10_simple;

import java.awt.Graphics2D;

/**
 *
 * @author Abdullah
 */
public class Circle {
    
    public Circle( double x, double y, double r ){
        
        _x = x;
        _y = y;
        _r = r;
        
    }
    
     public void draw( Graphics2D g ){
     
         g.drawOval( (int)getX(), (int)getY(), (int)getR()*2, (int)getR()*2);
         
         
     }
    
    private double _x;
    private double _y;
    private double _r;

    /**
     * @return the _x
     */
    public double getX() {
        return _x;
    }

    /**
     * @param x the _x to set
     */
    public void setX(double x) {
        this._x = x;
    }

    /**
     * @return the _y
     */
    public double getY() {
        return _y;
    }

    /**
     * @param y the _y to set
     */
    public void setY(double y) {
        this._y = y;
    }

    /**
     * @return the _r
     */
    public double getR() {
        return _r;
    }

    /**
     * @param r the _r to set
     */
    public void setR(double r) {
        this._r = r;
    }
}
