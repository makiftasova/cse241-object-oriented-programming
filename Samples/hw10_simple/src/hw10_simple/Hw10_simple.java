/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hw10_simple;

import java.io.IOException;
import javax.swing.JFrame;




/**
 *
 * @author Abdullah
 */
public class Hw10_simple {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        // TODO code application logic here
        
        
      Canvas simple_canvas = new Canvas();
        
      JFrame frame = new JFrame("Canvas");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.add( simple_canvas );
      frame.setSize(640, 480);
      frame.setVisible(true);
   
      
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////
      //animation
        for (float i = 0; i < 1000; i += .1) {

            System.out.println("gez gez..");
            Thread.sleep(100);

            float x = i * 16;
            float y = (float) (Math.sin(i) * 100) + frame.getWidth() / 5;


            simple_canvas.getC().setX(x);
            simple_canvas.getC().setY(y);

            simple_canvas.set_label_pos( (int)x, (int)y);
            
            
            frame.repaint();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
}


